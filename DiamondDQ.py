#!/usr/bin/env python
###############################
# Malta analysis at Diamond
#
# Kaloyan.Metodiev@cern.ch
# Maria.Mironova@cern.ch
# April 2019
###############################

import os
import sys
import ROOT
import time
import signal
import argparse
import PyMiniMaltaData
import numpy as np


def get_pos(filename):
	fr = open(filename,"r")
	counter=0
	xpos=[]
	ypos=[]
	ail=[]
	rc=[]
	timing_list=[]
	for line in fr.readlines():
	    counter+=1
	    if counter<11:continue
	    line.strip()
	    splitted=line.split("\t")
	    x=splitted[0]
	    y=splitted[1]
	    a=splitted[4]
	    r=splitted[5]
	    h=float(splitted[-3])
	    m=float(splitted[-2])
	    s=float(splitted[-1])
	    xpos.append(float(x))
	    ypos.append(float(y))
	    ail.append(a)
	    rc.append(r)
	    timing_list.append(h*3600+m*60+s)

	fr.close()
	return xpos,ypos,ail,rc,timing_list

def get_bins_above_thr(h1,threshold=5000):
	values=0
	for x in range(h1.GetNbinsX()):
		for y in range(h1.GetNbinsY()):
			val=h1.GetBinContent(x,y)
			if val>threshold:
				values+=val
	return values

cont = True
def signal_handler(signal, frame):
    print "You pressed ctrl+C to quit" 
    global cont
    cont = False
    return

class Tuple(dict):
    def __init__(self):
        self.treenumber=-1
        pass
    def LoadBranches(self,t):
        if self.treenumber == t.GetTreeNumber(): return
        self.treenumber = t.GetTreeNumber()
        for i in range(t.GetListOfLeaves().GetEntries()):
            print "Load", t.GetListOfLeaves().At(i).GetName()
            self.__setattr__(t.GetListOfLeaves().At(i).GetName(),
                             t.GetListOfLeaves().At(i))
            pass
        pass
    pass

parser=argparse.ArgumentParser()
parser.add_argument('-f','--file',help="input file",required=True)
parser.add_argument('-r','--diamond_run',help="file from diamond",required=False,default="312945")
parser.add_argument('-a','--diamond_file',help="file from diamond")
parser.add_argument('-t','--threshold',help="cutoff threshold",required=False,default=1500)
args=parser.parse_args()

run_file_dir="/mnt/b16/data/2019/mm22061-1/"
diamond_file=args.diamond_file
if args.diamond_run: diamond_file="%s/%s.dat" % (run_file_dir,args.diamond_run)
x,y,ail,rc,diamond_timing_list=get_pos(diamond_file)
stem=args.file.split('.')[-3]

tree=Tuple()
chain=ROOT.TChain("MALTA")
chain.AddFile(args.file)

md=PyMiniMaltaData.MiniMaltaData()

#c1=ROOT.TCanvas("c1","c1",800,600)
h1=ROOT.TH2I("h1",";Column;Row",16,0,16,64,0,64)
tt=ROOT.TLatex()

#f1=ROOT.TFile('./'+stem.split('/')[1]+'/analysed/'+stem.split('/')[-1]+'_analysed.root',"RECREATE")
f1=ROOT.TFile('./'+stem.split('/')[1]+'/analysed/'+stem.split('/')[-1]+'_analysed.root',"RECREATE")

entry=0
l1id=1
intensity=[]
index_offset=0

signal.signal(signal.SIGINT, signal_handler)
timing=[]
timing2=[]
while cont:
    chain.GetEntry(entry)
    if chain.LoadTree(entry)<0:
        break
    if entry%10000==0:
        print "Processed %i events" % entry
        pass
    entry += 1
    tree.LoadBranches(chain)

    if tree.l1idC.GetValue()!=l1id:
        #h1.Draw("COLZ")
        #tt.DrawLatexNDC(0.2,0.95,"L1ID: %i"%l1id)
        #c1.Update()
        #time.sleep(0.02)
        #raw_input("press any key to continue")

	intensity.append(get_bins_above_thr(h1,threshold=float(args.threshold)))
	#f1.WriteTObject(h1,str(x[index])+'_'+str(y[index])+'_'+str(ail[index])+'_'+str(rc[index])+'_'+str(tree.timer.GetValue()))
	f1.WriteTObject(h1,str(0)+'_'+str(0)+'_'+str(0)+'_'+str(0)+'_'+str(tree.timer.GetValue()))
        h1.Reset()
        l1id=tree.l1idC.GetValue()
	timing.append(tree.timer.GetValue())
	#timing2.append(diamond_timing_list[index])
        pass

    if tree.pixel.GetValue()==0: continue
    
    md.setWord1(int(tree.word1.GetValue()))
    md.setWord2(int(tree.word2.GetValue()))
    md.unpack()
    
    for i in xrange(md.getNhits()):
        h1.Fill(md.getHitColumn(i),md.getHitRow(i))
        pass
    pass


f1.Close()

timing=np.array(timing)
timing2=np.array(timing2)
#print np.diff(timing2)-np.diff(timing)

thGraph=ROOT.TGraph2D(len(x),np.array(x),np.array(y),np.array(intensity))
thGraph.Draw("COLZ")

