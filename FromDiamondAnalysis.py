#!/usr/bin/env python
import os
import sys
import ROOT
import time
import signal
import argparse
import numpy as np
from collections import Counter

import AtlasStyle

parser=argparse.ArgumentParser()
parser.add_argument('-f','--file',help="input file",required=True)
parser.add_argument('-r','--diamond_file',help="file from diamond",required=True)
parser.add_argument('-o','--output_dir',help="output directory",required=False)
parser.add_argument('-pX','--pixel_x',help="x pixel",required=False,default=-1)
parser.add_argument('-pY','--pixel_y',help="y pixel",required=False,default=-1)
args=parser.parse_args()

stem=args.file.split('.')[-2]
runnumber=stem.split('/')[-1]

AtlasStyle.SetAtlasStyle()

f1=ROOT.TFile(args.file)
keys_iterator=f1.GetListOfKeys()

def get_central_pixel():
	counter=0
	for key in keys_iterator:
		h1=key.ReadObj()
		max_bin=0
		for x in range(h1.GetNbinsX()+1):
			for y in range(h1.GetNbinsY()+1):
				if h1.GetBinContent(x,y)>max_bin:
					max_bin=h1.GetBinContent(x,y)
					max_pixel=(x,y)
		counter+=1
		if counter>1:
			break
	return max_pixel
print get_central_pixel()

if args.pixel_x>0:
	pixel=(int(args.pixel_x),int(args.pixel_y))
else:
	pixel=get_central_pixel()

pixels=[]

for pxi in range(-3,2):
	for pyi in range(-3,2):
		pixels.append((pixel[0]+pxi,pixel[1]+pyi))
		pass
	pass

#pixels=[(12,12),(11,12)]

c2=ROOT.TCanvas("c1","c1",800,800)

pad1=ROOT.TPad("pad1","pad1",0,0.3,1,1.0)
pad1.Draw()
pad1.SetBottomMargin(0.2)

pad2=ROOT.TPad("pad2","pad2",0,0.05,1,0.3)
pad2.Draw()
pad2.SetBottomMargin(0.25)


run_file_dir="/mnt/b16/data/2019/mm22061-1/"
def get_pos(filename):
	fr = open(run_file_dir+filename,"r")
	counter=0
	xpos=[]
	ypos=[]
	ail=[]
	rc=[]
	timing_list=[]
	for line in fr.readlines():
	    counter+=1
	    if counter<11:continue
	    line.strip()
	    splitted=line.split("\t")
	    x=splitted[0]
	    y=splitted[1]
	    a=float(splitted[4])
	    r=splitted[5]
	    h=float(splitted[-4])
	    m=float(splitted[-3])
	    s=float(splitted[-2])
	    xpos.append(float(x))
	    ypos.append(float(y))
	    ail.append(a)
	    rc.append(r)
	    timing_list.append(h*3600+m*60+s)

	fr.close()
	return xpos,ypos,ail,rc,timing_list

x_diamond_list,y_diamond_list,ail_diamond_list,rc_diamond_list,diamond_timing_list=get_pos(args.diamond_file)
timing2=np.array(diamond_timing_list)
timing2-=timing2[0]

def find_nearest(array,value):
	idx=(np.abs(array-value)).argmin()
	return idx

def add_missing_pixels(h1):
	for x in range(1,h1.GetNbinsX()+1):
		for y in range(1,h1.GetNbinsY()+1):
			if h1.GetBinContent(x,y)==0:
				vals=[]
				for dx in range(-1,2):
					for dy in range(-1,2):
						vals.append(h1.GetBinContent(x+dx,y+dy))
				vals=np.array(vals)
				h1.SetBinContent(x,y,np.average(vals[vals.nonzero()]))

	return h1


def get_pixel_data_variables_no_timing(pixels):
	intensity_list={}
	x_list={}
	y_list={}
	ail_list={}
	timing=[]
	max_ail=0
	counter=0
	for key in keys_iterator:
		intensity=[]
		counter+=1
		if counter%500 == 0:
			print 'Events processed: '+str(counter)
		name_vals=key.GetName().split('_')
		h1=key.ReadObj()
		for p_num,pixel in enumerate(pixels):
			intensity.append(h1.GetBinContent(*pixel))
		intensity=np.array(intensity)
		rc=float(name_vals[3]) #ring current
		timer=float(name_vals[4])
		timing.append(timer)

		x=x_diamond_list[idx]
		y=y_diamond_list[idx]
		ail=ail_diamond_list[idx]
		x_list[idx]=x
		y_list[idx]=y
		ail_list[idx]=ail
		intensity_list[idx]=intensity/ail

	intensity_list=np.array(intensity_list.values())*max(ail_diamond_list)
	x_list=np.array(x_list.values())
	y_list=np.array(y_list.values())

	return intensity_list,x_list,y_list,np.array(timing)

def get_pixel_data_variables(pixels):
	intensity_list={}
	x_list={}
	y_list={}
	ail_list={}
	timing=[]
	timing2_list=[]
	indices=[]
	indices_dict={}
	max_ail=0
	counter=0
	for key in keys_iterator:
		intensity=[]
		counter+=1
		if counter%500 == 0:
			print 'Events processed: '+str(counter)
		name_vals=key.GetName().split('_')
		h1=key.ReadObj()
		for p_num,pixel in enumerate(pixels):
			intensity.append(h1.GetBinContent(*pixel))
		intensity=np.array(intensity)
		rc=float(name_vals[3]) #ring current
		timer=float(name_vals[4])
		timing.append(timer)
		idx=find_nearest(timing2,timer-timing[0])
		if idx in indices:
			indices_dict[idx]=(indices_dict[idx],timer-timing[0])
			x_list[idx-1]=x_diamond_list[idx-1]
			y_list[idx-1]=y_diamond_list[idx-1]	
			intensity_list[idx-1]=intensity_list[idx]*ail_list[idx]/ail_diamond_list[idx-1]
			ail_list[idx-1]=ail_diamond_list[idx-1]
		else:
			indices_dict[idx]=timer-timing[0]
		indices.append(idx)
		x=x_diamond_list[idx]
		y=y_diamond_list[idx]
		ail=ail_diamond_list[idx]
		x_list[idx]=x
		y_list[idx]=y
		timing2_list[idx]=timing2[idx]
		ail_list[idx]=ail
		intensity_list[idx]=intensity/ail

	intensity_list=np.array(intensity_list.values())*max(ail_diamond_list)
	x_list=np.array(x_list.values())
	y_list=np.array(y_list.values())
	'''
	indices_counter=Counter(indices)
	doubled_indices=[]
	for key,val in indices_counter.iteritems():
		if val==2:
			doubled_indices.append(key)
	'''
	return intensity_list,x_list,y_list,np.array(timing)


intensity_list,x_list,y_list,timing=get_pixel_data_variables(pixels)


fix_val=(max(x_list)+min(x_list))/2

fix_val=0.617


fix_ind=find_nearest(x_list,fix_val)
val=x_list[fix_ind]
plot_indices=np.where(x_list==val)
pad2.cd()
thGraph_list=[]
for count,p in enumerate(pixels):
	intensity_pixel=np.copy(intensity_list[:,count])
	
	thGraph_list.append(ROOT.TGraph(len(plot_indices[0]),y_list[plot_indices],intensity_pixel[plot_indices]))	
	print count	
	if count==0:
		h2=ROOT.TH2D("h2","",len(set(y_list)),min(y_list),max(y_list),len(set(x_list)),min(x_list),max(x_list))
		thGraph_list[-1].SetLineWidth(2)
		thGraph_list[-1].GetYaxis().SetRangeUser(np.min(intensity_list),np.max(intensity_list))
		thGraph_list[-1].GetXaxis().SetRangeUser(np.min(y_list),np.max(y_list))
		thGraph_list[-1].GetXaxis().SetLabelSize(0.09)
		thGraph_list[-1].GetYaxis().SetLabelSize(0.09)
		thGraph_list[-1].Draw("AL")
	
	#thGraph=ROOT.TGraph2D(len(x_list),y_list,x_list,intensity_list)
	#thGraph.SetNpx(len(set(y_list)))
	#thGraph.SetNpy(len(set(x_list)))
	#thGraph.Draw("COLZ")
	else:
		thGraph_list[-1].SetLineColor(count)
		thGraph_list[-1].SetLineWidth(2)
		if np.max(intensity_pixel[plot_indices])>1000:
			thGraph_list[-1].Draw("L")

	
	print fix_val
		
	h2.FillN(len(x_list),y_list,x_list,intensity_pixel)





pad1.cd()
h2=add_missing_pixels(h2)
h2.Draw("COLZ")

h2.GetXaxis().SetTitle("y (mm)")
h2.GetYaxis().SetTitle("x (mm)")

h2.GetXaxis().SetLabelSize(0.03)
h2.GetZaxis().SetLabelSize(0.03)
h2.GetYaxis().SetLabelSize(0.03)

'''
h2.GetXaxis().SetTitleOffset(0)
h2.GetYaxis().SetTitleOffset(0)

h2.GetXaxis().SetTitleSize(1.2)
h2.GetYaxis().SetTitleSize(1.2)
'''
h2.GetXaxis().SetLabelOffset(0.02)
h2.GetYaxis().SetLabelOffset(0.02)

h2.SetStats(0)

h2.Draw("COLZ")

tlatex=ROOT.TLatex()
tlatex.SetNDC()
tlatex.SetTextAlign(22)
tlatex.SetTextFont(63)
tlatex.SetTextSizePixels(22)
tlatex.DrawLatex(0.2,0.05,"Central Pixel: %d,%d"%(pixel[0],pixel[1]))

c2.SetRightMargin(0.15)
c2.SetWindowSize(900,850)
c2.Update()

c2.SaveAs("/home/sbmuser/MaltaSW/MaltaDiamond/data/pdfs/"+runnumber+".pdf")






