#ifndef DIAMOND_STAGE_H
#define DIAMOND_STAGE_H

#include <string>
#include <vector>
#include <stdlib.h>
#include <cstdint>

/**
 * DiamondStage parses a diamond linear stage text file
 * with DiamondStage::Open, and all the positions, dates
 * and beam intensities are stored in memory. 
 * DiamondStage::GetNSteps returns the number of steps 
 * contained in the file, as well as the minimum
 * (DiamondStage::GetXmin, DiamondStage::GetYmin),
 * and maximum range 
 * (DiamondStage::GetXmax, DiamondStage::GetYmax).
 * Other parameters extracted from the file are the 
 * number of steps in the X (DiamondStage::GetXbins), 
 * and Y direction (DiamondStage::GetYbins), as well
 * as the bin size in X (DiamondStage::GetXstep),
 * and Y axis (FiamondStage::GetYstep).
 * For each step the following values are available:
 * time (DiamondStage::GetT), X (DiamondStage::GetX), 
 * Y (DiamondStage::GetY), and beam intensity 
 * (DiamondStage::GetAil).
 * 
 *
 * @brief Class to read the dat files 
 * from the Diamond Light Source
 * linear stage output
 *
 * @author Carlos.Solans@cern.ch
 * @author Patrick.Morishii.Freeman@gmail.com
 * @date May 2019
 **/
class DiamondStage{

 public:
  
  /**
   * @brief Initialize internal counters
   **/
  DiamondStage();

  /**
   * @brief clear the internal vectors
   **/
  ~DiamondStage();

  /**
   * Open a new file, clear the vectors, read the header,
   * extract the x, and y min, max and number of steps
   * from the header, read the data into the vectors.
   * @brief Open a file
   **/
  void Open(std::string path);

  /**
   * @brief Get the number of steps in the run
   **/
  uint32_t GetNSteps();
  
  /**
   * @brief Get the Time of the given step
   * @param step the integer index of the step
   **/
  float GetT(uint32_t step);
  
  /**
   * @brief Get the normalization of the step
   * @param step the integer index of the step
   **/
  float GetAil(uint32_t step);
  
  /**
   * @brief Get the x position of the step
   * @param step the integer index of the step
   **/
  float GetX(uint32_t step);
  
  /**
   * @brief Get the minimum value of the x axis
   **/
  float GetXmin();
  
  /**
   * @brief Get the maximum value of the x axis
   **/
  float GetXmax();
  
  /**
   * @brief Get the size of the step of the x axis
   **/
  float GetXstep();
  
  /**
   * @brief Get the number of steps in the x axis
   **/
  float GetXbins();
  
  /**
   * @brief Get the value of the y axis for the given step
   * @param step the integer index of the step
   **/
  float GetY(uint32_t step);
  
  /**
   * @brief Get the minimum value of the y axis
   **/
  float GetYmin();
  
  /**
   * @brief Get the maximum value of the y axis
   **/
  float GetYmax();
  
  /**
   * @brief Get the size of the step of the x axis
   **/
  float GetYstep();
  
  /**
   * @brief Get the number of steps in the x axis
   **/
  float GetYbins();

 private:
  
  std::vector<float> m_ail;
  std::vector<float> m_t;
  std::vector<float> m_x;
  float m_xmin;
  float m_xmax;
  float m_xstep;
  float m_xbins;
  std::vector<float> m_y;
  float m_ymin;
  float m_ymax;
  float m_ystep;
  float m_ybins;

  std::vector<std::string> split(std::string str, std::string token);

  
};

#endif
