#include <iostream>
#include <iomanip>
#include <MaltaDiamond/DiamondStage.h>
#include <cmdl/cmdargs.h>
#include <TFile.h>
#include <TTree.h>

using namespace std;

int main(int argc, char *argv[]) {
  
  CmdArgStr  cDiamond('d',"diamond","file","diamond data file",CmdArg::isREQ);
  CmdArgStr  cOutput('o',"output","file","output data file",CmdArg::isREQ);
  CmdArgBool cVerbose('v',"verbose","turn on verbose mode");
  
  CmdLine cmdl(*argv,&cDiamond,&cVerbose,&cOutput,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);
  
  string diamondfile(cDiamond);
  string outputfile(cOutput);

  cout << "Open diamond file: " << diamondfile << endl;
  DiamondStage ds;
  ds.Open(diamondfile);
  
  //output file
  cout << "Create output file: " << outputfile << endl;
  TFile *fw = new TFile(outputfile.c_str(),"recreate","8");
  TTree *tree = new TTree("DiamondFile","Diamond");

  float x;
  float y;
  float t;
  
  tree->Branch("x", &x);
  tree->Branch("y", &y);
  tree->Branch("t", &t);

  for(uint32_t i=0; i<ds.GetNSteps(); i++){
    x=ds.GetX(i);
    y=ds.GetY(i);
    t=ds.GetT(i);
    tree->Fill();
  }
  
  cout << "Close output file: " << outputfile << endl;
  tree->Write();
  fw->Close();
  delete fw;
  cout << "Have a nice day" << endl;
  return 0;

}

