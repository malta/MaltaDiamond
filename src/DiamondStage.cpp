#include <MaltaDiamond/DiamondStage.h>
#include <iostream>
#include <fstream>
#include <cmath>
using namespace std;

DiamondStage::DiamondStage(){
  m_xmin=0;
  m_xmax=0;
  m_xbins=0;
  m_xstep=0;
  m_ymin=0;
  m_ymax=0;
  m_ybins=0;
  m_ystep=0;
}

DiamondStage::~DiamondStage(){
  m_x.clear();
  m_y.clear();
  m_t.clear();
  m_ail.clear();
}


void DiamondStage::Open(string path){
  cout << "Open: " << path << endl;
  fstream fr;
  fr.open(path.c_str());  
  int iline = -1;
  float day = 0; //day for scans that go past midnight
  m_x.clear();
  m_y.clear();
  m_t.clear();
  m_ail.clear();
  while(fr.good()){
    string line;
    getline(fr,line);
    iline+=1;
    if(iline==6){
      string cmd=line;
      size_t p0=cmd.find("'");
      size_t p1=cmd.find("'",p0+1);
      cmd=cmd.substr(p0+1,p1-1);
      vector<string> chk=split(cmd," ");
      cout<<"Parse header: "<<cmd<<endl;
      m_xmin=stof(chk[2]);
      m_xmax=stof(chk[3]);
      if(m_xmin>m_xmax){
        float tmp=m_xmin;
        m_xmin=m_xmax;
        m_xmax=tmp;
      }
      m_xstep=stof(chk[4]);
      m_xbins=int( int((m_xmax-m_xmin)*1000)/int(m_xstep*1000) );
      m_ymin=stof(chk[6]);
      m_ymax=stof(chk[7]);
      if(m_ymin>m_ymax){
        float tmp=m_ymin;
        m_ymin=m_ymax;
        m_ymax=tmp;
      }
      m_ystep=stof(chk[8]);
      m_ybins=round( abs(m_ymax-m_ymin)*1000) / int(m_ystep*1000) ;
      //cout << " maxMin: " << round(abs(m_ymax-m_ymin)*1000) << " step: " << int(m_ystep*1000) << endl;
      cout << "xmin: " << m_xmin << endl
           << "xmax: " << m_xmax << endl
           << "xstep: " << m_xstep << endl
           << "xbins: " << m_xbins << endl;
      cout << "ymin: " << m_ymin << endl
           << "ymax: " << m_ymax << endl
           << "ystep: " << m_ystep << endl
           << "ybins: " << m_ybins << endl;

    }
    if(iline<10){
      continue;
    }
    if(line.find("\t")==string::npos){continue;}
    vector<string> chk=split(line,"\t");
    if(false){
      for(uint32_t i=0;i<chk.size();i++){
        cout << "[" << chk[i] << "]";
      }
      cout << endl;
    }
    if(iline==10||iline==2610){cout<<"For the first/last line has: x="<<stof(chk[0])<<", y ="<< stof(chk[1])<<", ail ="<<stof(chk[4])<<endl;}
    m_x.push_back(stof(chk[0]));
    m_y.push_back(stof(chk[1]));
    m_ail.push_back(stof(chk[4]));
    float h=stof(chk[6]);
    float m=stof(chk[7]);
    float s=stof(chk[8]);
    float t=day*86400.+h*3600.+m*60.+s;
    
    //check if the time has gone past midnight 
    if(m_t.size()>0 && (m_t.back()-t)>23.5*3600) {
      //we skipped a day!
      day++;
      t += 86400.;
    } 
    
    m_t.push_back(t);
  }
  cout << "Close file" << endl;
  fr.close();
}

uint32_t DiamondStage::GetNSteps(){
  return m_x.size();
}

float DiamondStage::GetT(uint32_t i){
  if(i>=m_t.size()) return 0;
  return m_t[i];
}

float DiamondStage::GetAil(uint32_t i){
  if(i>=m_ail.size()) return 0;
  return m_ail[i];
}

float DiamondStage::GetX(uint32_t i){
  if(i>=m_x.size()) return 0;
  return m_x[i];
}

float DiamondStage::GetY(uint32_t i){
  if(i>=m_y.size()) return 0;
  return m_y[i];
}

float DiamondStage::GetXmin(){
  return m_xmin;
}

float DiamondStage::GetXmax(){
  return m_xmax;
}

float DiamondStage::GetXstep(){
  return m_xstep;
}

float DiamondStage::GetXbins(){
  return m_xbins;
}

float DiamondStage::GetYmin(){
  return m_ymin;
}

float DiamondStage::GetYmax(){
  return m_ymax;
}

float DiamondStage::GetYstep(){
  return m_ystep;
}

float DiamondStage::GetYbins(){
  return m_ybins;
}


vector<string> DiamondStage::split(string str, string token){
  vector<string>result;
  while(str.size()){
    size_t index = str.find(token);
    if(index!=string::npos){
      result.push_back(str.substr(0,index));
      str = str.substr(index+token.size());
      if(str.size()==0)result.push_back(str);
    }else{
      result.push_back(str);
      str = "";
    }
  }
  return result;
}
