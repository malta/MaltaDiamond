/***********************************************
 * Diamond data reconstruction
 * Combine diamond stage and MALTA output
 * Remove duplicate hits from MALTA
 * Split MALTA events into shorter time frames
 * 
 * July 2019
 * patrick.moriishi.freeman@cern.ch
 * Carlos.Solans@cern.ch
 * Valerio.Dao@cern.ch
 * Florian.Dachs@cern.ch
 ***********************************************/

#include <iomanip>
#include <TTree.h>
#include <TFile.h>
#include <TBranch.h>
#include <TGraph.h>
#include <TGraph2D.h>
#include <TCanvas.h>
#include <TH1I.h>
#include <TH1F.h>
#include "MiniMalta/MiniMaltaData.h"
#include "MaltaDiamond/DiamondStage.h"
#include <cmdl/cmdargs.h>
#include <functional>
#include <string>
#include <iostream>

using namespace std;

//Define if a hit is connected
static bool connected(pair<int,int> hit0, pair<int,int> hit1)
{
  auto dc = std::abs(hit1.first - hit0.first);
  auto dr = std::abs(hit1.second - hit0.second);
  return (((dc == 0) && (dr <= 1)) || ((dc <= 1) && (dr == 0)));
}

//convert a vector of hits into a vector of clusters
vector<vector<pair<int,int>>> clusterize(vector<pair<int,int>> hits)
{
  vector<vector<pair<int,int>>> clusters;
  while (!hits.empty()) {
    auto clusterStart = --hits.end();
    auto clusterEnd = hits.end();

    // accumulate all connected hits at the end of the vector until
    // no more compatible hits can be found.
    // each iteration can only pick up the next neighboring pixels, so we
    // need to iterate until we find no more connected pixels.
    while (true) {
      auto unconnected = [&](pair<int,int> hit) {
        using namespace std::placeholders;
        return std::none_of(clusterStart, clusterEnd,
                            std::bind(connected, hit, _1));
      };
      // connected hits end up in [moreHits, clusterStart)
      auto moreHits = std::partition(hits.begin(), clusterStart, unconnected);
      if (moreHits == clusterStart) {
        break;
      }
      clusterStart = moreHits;
    }

    // construct cluster from connected hits
    vector<pair<int,int>> cluster;
    for (auto hit = clusterStart; hit != clusterEnd; ++hit) {
      cluster.push_back(*hit);
    }
    clusters.push_back(cluster);

    // remove clustered hits from further consideration
    hits.erase(clusterStart, clusterEnd);
  }
  return clusters;
}

//find the diamond time which is closest to MALTA time
uint32_t findStep(DiamondStage &ds, float malta_time){
  uint32_t closest_step=0xFFFFFFFF;
  float smallest_time_diff=1E15;
  for(uint32_t step=0; step<ds.GetNSteps(); step++){
    float diamond_time = ds.GetT(step)-ds.GetT(0);
    //float time_diff=diamond_time-malta_time;
    float time_diff=malta_time-diamond_time;
    if(abs(time_diff)<abs(smallest_time_diff)){
      smallest_time_diff=time_diff;
      closest_step=step;
    }
  }
  return closest_step; 
}

int main(int argc, char *argv[]) {
  
  CmdArgInt  cRun('r',"run","runnumber","run number",CmdArg::isREQ);
  CmdArgStr  cMalta('m',"malta","file","malta data file",CmdArg::isREQ);
  CmdArgStr  cDiamond('d',"diamond","file","diamond data file",CmdArg::isREQ);
  CmdArgInt  cEvents('e',"events","number","max number of L1IDs");
  CmdArgBool cVerbose('v',"verbose","turn on verbose mode");
  CmdArgStr  cOutdir('o',"output","output","output directory");
  
  CmdLine cmdl(*argv,&cRun,&cMalta,&cDiamond,&cEvents,&cVerbose,&cOutdir,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);
  
  int run(cRun);
  uint32_t maxEvents(cEvents);
  bool bMaxEvents = (cEvents.flags()&CmdArg::GIVEN);
  string maltafile(cMalta);
  string diamondfile(cDiamond);
  string outputdir(".");
  if(cOutdir.flags()&CmdArg::GIVEN){outputdir=string(cOutdir);}
  string outputfile(Form("%s/reco_%06i.root",outputdir.c_str(),run));
  
  DiamondStage ds;
  ds.Open(diamondfile);
  
  //read input tree 
  cout << "Read input file: " << maltafile << endl;
  TFile* inFile = TFile::Open(maltafile.c_str());
  TTree* inTree = (TTree*) inFile->Get("MALTA");
  uint32_t word1;
  uint32_t word2;
  uint32_t l1id;
  float    timer;

  TBranch *b_word1;
  TBranch *b_word2;
  TBranch *b_timer;
  TBranch *b_l1id;
  
  inTree->SetBranchAddress("word1",&word1, &b_word1);
  inTree->SetBranchAddress("word2",&word2, &b_word2);
  inTree->SetBranchAddress("timer",&timer, &b_timer);
  inTree->SetBranchAddress("l1id" ,&l1id,  &b_l1id );
	
  //create output tree
  cout << "Create output file: " << outputfile << endl;
  TFile *outFile = new TFile(outputfile.c_str(),"recreate","8");
  TTree *outTree = new TTree("DiamondReco","MALTA_Diamond");
  
  double stage_x;
  double stage_y;
  double ail;
  float diamond_time;
  float malta_time;
  float delta_malta_time;
  uint32_t hits[16][64];
  uint32_t cl_n;
  const uint32_t MAXCLUSTERS=10;
  float cl_x[MAXCLUSTERS];
  float cl_y[MAXCLUSTERS];
  float cl_s[MAXCLUSTERS];
  vector<pair<int,int>> vhits;
  
  //add branches to output tree
  outTree->Branch("stage_x", &stage_x);
  outTree->Branch("stage_y", &stage_y);
  outTree->Branch("ail", &ail);
  outTree->Branch("diamond_time", &diamond_time);
  outTree->Branch("malta_time", &malta_time);
  outTree->Branch("delta_malta_time", &delta_malta_time);
  outTree->Branch("hits",&hits,"hits[16][64]/i");
  outTree->Branch("cl_n",&cl_n,"cl_n/i");
  outTree->Branch("cl_x",&cl_x,"cl_x[cl_n]/f");
  outTree->Branch("cl_y",&cl_y,"cl_y[cl_n]/f");
  outTree->Branch("cl_s",&cl_s,"cl_s[cl_n]/f");

  TGraph *gTimeCorr=new TGraph();
  gTimeCorr->SetName("gTimeCorr");
  gTimeCorr->SetTitle(";Malta Time[s];Diamond Time[s]");
  
  TH1F *hMaltaDeltaTime=new TH1F("hMaltaDeltaTime",";Delta Time [s]", 60, 0, 6);
  TH1F *hMaltaEventTime=new TH1F("hMaltaEventTime",";Event Time [s]", 600, 0, 6);
  TH1F *hMaltaTime=new TH1F("hMaltaTime",";MALTA Time [ns]",1000, 0, 810e3);
  TGraph2D *gClusterSizeMap=new TGraph2D();
  gClusterSizeMap->SetName("gClusterSizeMap");
  gClusterSizeMap->SetTitle(";MALTA X [#mum];MALTA Y [#mum]");
  
  cout << "Loop over the data" << endl;
  MiniMaltaData md;
  MiniMaltaData mem_md;
  uint32_t entry=0;
  uint32_t malta_time0=0;
  float mem_malta_time=0;
  uint32_t mem_l1id=0x0FFFFFFF;
  uint32_t found_step=0;
  float mem_timing=0;
  bool first_hit=true;
  float avg_cluster_size=0;
  float events=0;

  //Loop over entries
  while(true){
    inTree->GetEntry(entry);
    if(inTree->LoadTree(entry)<0){break;}
    
    entry++;

    if(entry%100000==0) cout << "Processed " << entry << " entries... L1ID " << l1id << "/" << maxEvents << endl;
      
    if(bMaxEvents && l1id > maxEvents){
      cout << "Reached number of events requested..." << endl;
      outTree->Write();
      break;
    }
      
    if(first_hit){
      malta_time0=timer;
      first_hit = false;
    }

    malta_time=timer-malta_time0;
   
    hMaltaEventTime->Fill(malta_time-mem_malta_time);

    //unpack the MALTA words to get hit contents
    md.setWord1(word1);
    md.setWord2(word2);
    md.unpack();
    
    //duplicate removal
    if (mem_md.getL1id() == md.getL1id()  &&
    	mem_md.getBcid() == md.getBcid()  &&
	mem_md.getWinid() == md.getWinid() &&
	mem_md.getDcolumn() == md.getDcolumn() &&
	mem_md.getParity() == md.getParity()  &&
	mem_md.getGroup() == md.getGroup()  &&
	mem_md.getPixel() == md.getPixel() ) {
      continue;
    }
    
    //event splitting
    float timing= md.getBcid()*25.0;
    float timeDiff=(timing-mem_timing);
    if (timeDiff<0) timeDiff+=810e3;
    if (cVerbose) cout << " Time: " << timing << " , and timeDiff: " << timeDiff << endl;
    
    if (mem_timing!=0 and timeDiff>100) {

      events+=1;

      if(cVerbose) cout << "mem_l1id: " << mem_l1id << " l1id:" << l1id << endl;

      if(mem_l1id!=l1id){
	
	if(cVerbose){cout << "Find the closest diamond step" << endl;}
	
	found_step = findStep(ds, malta_time-0.45);
	if(found_step!=0xFFFFFFFF){
	  stage_x = ds.GetX(found_step);
	  stage_y = ds.GetY(found_step);
	  ail = ds.GetAil(found_step);
	  diamond_time=ds.GetT(found_step)-ds.GetT(0);
	  gTimeCorr->SetPoint(gTimeCorr->GetN(),malta_time,diamond_time);
	  delta_malta_time=malta_time-mem_malta_time;
	  hMaltaDeltaTime->Fill(delta_malta_time);
	  cout << "Matched: l1id " << l1id << " diamond: " << found_step << endl;
	}else{
	  cout << "Could not find a step for this l1id: " << mem_l1id << endl;
	}
	mem_malta_time=malta_time;
	mem_l1id = l1id;
	avg_cluster_size/=events;
	float malta_x=36.4*64-stage_y;
	float malta_y=stage_x;
	gClusterSizeMap->SetPoint(gClusterSizeMap->GetN(),malta_x,malta_y,avg_cluster_size);
	avg_cluster_size=0;
	events=0;
      }
      
      //build clusters
      if(cVerbose){cout << "Clusterize" << endl;}	
      vector<vector<pair<int,int>>> clusters = clusterize(vhits);
      
      //format clusters
      if(cVerbose){cout << "Format" << endl;} 
      cl_n=clusters.size();
      for(uint32_t i=0;i<clusters.size();i++){
	float cx=0;
	float cy=0;
	for(uint32_t j=0;j<clusters.at(i).size();j++){
	  cx+=clusters.at(i).at(j).first;
	  cy+=clusters.at(i).at(j).second;
	}
	cx/=(float)clusters.at(i).size();
	cy/=(float)clusters.at(i).size();
	cl_x[i]=cx;
	cl_y[i]=cy;
	cl_s[i]=clusters.at(i).size();
	avg_cluster_size+=clusters.at(i).size();
      }
      if(cVerbose){cout << "Fill" << endl;} 
      for(uint32_t i=clusters.size();i<MAXCLUSTERS;i++){
	cl_x[i]=0;
	cl_y[i]=0;
	cl_s[i]=0;
      }
      
      //Fill tree
      if(cVerbose){cout << "Fill tree" << endl;} 
      outTree->Fill();
	
      //clear the hit contents
      if(cVerbose){cout << "Clear" << endl;} 
      for (int i=0; i<16; i++){
	for(int j=0; j<64;j++){
	  hits[i][j] = 0;
	}
      }
      vhits.clear();
      
    }
    
    if(cVerbose){cout << "Accumulate hits" << endl;} 
    for (uint32_t i=0; i<md.getNhits(); i++){
      uint32_t col = md.getHitColumn(i);
      uint32_t row = md.getHitRow(i);
      hits[col][row]+=1;
      vhits.push_back(pair<int,int>(col,row));
    }

    //keep the malta word for next event
    if(cVerbose){cout << "Keep for next entry" << endl;} 
    mem_md=md;
    mem_timing = timing;
          
    hMaltaTime->Fill(timing);
  }

  //Last event might not be a change of L1ID

  if(mem_l1id!=l1id){

    if(cVerbose){cout << "Find the closest diamond step" << endl;}
    found_step = findStep(ds, malta_time);
    if(found_step!=0xFFFFFFFF){
      stage_x = ds.GetX(found_step);
      stage_y = ds.GetY(found_step);
      ail = ds.GetAil(found_step);
      diamond_time=ds.GetT(found_step)-ds.GetT(0);
      gTimeCorr->SetPoint(gTimeCorr->GetN(),malta_time,diamond_time);
      delta_malta_time=malta_time-mem_malta_time;
      hMaltaDeltaTime->Fill(delta_malta_time);
    }else{
      cout << "Could not find a step for this l1id: " << mem_l1id << endl;
    }
    avg_cluster_size/=events;
    float malta_x=36.4*64-stage_y;
    float malta_y=stage_x;
    gClusterSizeMap->SetPoint(gClusterSizeMap->GetN(),malta_x,malta_y,avg_cluster_size);
  }
  
  //build clusters
  vector<vector<pair<int,int>>> clusters = clusterize(vhits);
  //format clusters
  for(uint32_t i=0;i<clusters.size();i++){
    float cx=0;
    float cy=0;
    for(uint32_t j=0;j<clusters.at(i).size();j++){
      cx+=clusters.at(i).at(i).first;
      cy+=clusters.at(i).at(i).second;
    }
    cx/=(float)clusters.at(i).size();
    cy/=(float)clusters.at(i).size();
    cl_x[i]=cx;
    cl_y[i]=cy;
    cl_s[i]=clusters.at(i).size();
    avg_cluster_size+=clusters.at(i).size();
  }
  for(uint32_t i=clusters.size();i<MAXCLUSTERS;i++){
    cl_x[i]=0;
    cl_y[i]=0;
    cl_s[i]=0;
  }
  
  //Fill tree
  outTree->Fill();
    
  avg_cluster_size/=events;
  float malta_x=36.4*64-stage_y;
  float malta_y=stage_x;
  gClusterSizeMap->SetPoint(gClusterSizeMap->GetN(),malta_x,malta_y,avg_cluster_size);
  
  //Close output tree
  cout << "Close output file: " << outputfile << endl;
  gTimeCorr->Write();
  gClusterSizeMap->Write();
  hMaltaDeltaTime->Write();
  hMaltaEventTime->Write();
  hMaltaTime->Write();
  outFile->Close();
  
  cout << "Have a nice day" << endl;
  return 0;
}




