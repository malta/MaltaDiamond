/***********************************************
 * Diamond data reconstruction
 * Combine diamond stage and MALTA output
 * Remove duplicate hits from MALTA
 * Split MALTA events into shorter time frames
 * 
 * July 2019
 * patrick.moriishi.freeman@cern.ch
 * Carlos.Solans@cern.ch
 * Valerio.Dao@cern.ch
 * Florian.Dachs@cern.ch
 ***********************************************/

#include <iomanip>
#include <TTree.h>
#include <TFile.h>
#include <TBranch.h>
#include <TGraph.h>
#include <TGraph2D.h>
#include <TCanvas.h>
#include <TH1I.h>
#include <TH1F.h>
#include <TH2D.h>
#include "MiniMalta/MiniMaltaData.h"
#include "MaltaDiamond/DiamondStage.h"
#include <cmdl/cmdargs.h>
#include <functional>
#include <string>
#include <iostream>

using namespace std;

//Define if a hit is connected
static bool connected(pair<int,int> hit0, pair<int,int> hit1)
{
  auto dc = std::abs(hit1.first - hit0.first);
  auto dr = std::abs(hit1.second - hit0.second);
  return (((dc == 0) && (dr <= 1)) || ((dc <= 1) && (dr == 0)));
}

//convert a vector of hits into a vector of clusters
vector<vector<pair<int,int>>> clusterize(vector<pair<int,int>> hits)
{
  vector<vector<pair<int,int>>> clusters;
  while (!hits.empty()) {
    auto clusterStart = --hits.end();
    auto clusterEnd = hits.end();

    // accumulate all connected hits at the end of the vector until
    // no more compatible hits can be found.
    // each iteration can only pick up the next neighboring pixels, so we
    // need to iterate until we find no more connected pixels.
    while (true) {
      auto unconnected = [&](pair<int,int> hit) {
        using namespace std::placeholders;
        return std::none_of(clusterStart, clusterEnd,
                            std::bind(connected, hit, _1));
      };
      // connected hits end up in [moreHits, clusterStart)
      auto moreHits = std::partition(hits.begin(), clusterStart, unconnected);
      if (moreHits == clusterStart) {
        break;
      }
      clusterStart = moreHits;
    }

    // construct cluster from connected hits
    vector<pair<int,int>> cluster;
    for (auto hit = clusterStart; hit != clusterEnd; ++hit) {
      cluster.push_back(*hit);
    }
    clusters.push_back(cluster);

    // remove clustered hits from further consideration
    hits.erase(clusterStart, clusterEnd);
  }
  return clusters;
}

//find the diamond time which is closest to MALTA time
uint32_t findStep(DiamondStage &ds, float malta_time, float &closest){
  uint32_t closest_step=0xFFFFFFFF;
  float smallest_time_diff=1E15;
  for(uint32_t step=0; step<ds.GetNSteps(); step++){
    float diamond_time = ds.GetT(step)-ds.GetT(0);
    //float time_diff=diamond_time-malta_time;
    float time_diff=malta_time-diamond_time;
    //if (time_diff<0) continue;
    if (fabs(time_diff)<2) cout <<   "                                                                     " << time_diff << endl; 
    if(fabs(time_diff)<fabs(smallest_time_diff)){
      smallest_time_diff=time_diff;
      closest_step=step;
    }
  } 
  closest=smallest_time_diff;
  cout << "SMALLEST time diff: " << smallest_time_diff << endl;
  return closest_step; 
}

uint32_t sectorIrrad(int run){
  uint32_t sector_irr;
  if(run==6026 or run==6029 or run==6030 or run==6031 or run==6032 or run==6036 or run==6039 or run==6043 or run==6045 or run==6032 or run==6052 or run==6057 or run==6060 or run==6062 or run==6063) sector_irr = 1;//pwell
  else if(run==6027 or run==6033 or run==6037 or run==6040 or run==6041 or run==6046 or run==6047 or run==6048 or run==6049 or run==6053 or run==6055 or run==6058) sector_irr = 3;//ngap
  else if(run==6028 or run==6034 or run==6035 or run==6038 or run==6042 or run==6044 or run==6050 or run==6051 or run==6054 or run==6056 or run==6059 or run==6061 or run==6064 or run==6065) sector_irr = 0;//malta
  return sector_irr;
}


int main(int argc, char *argv[]) {
  
  CmdArgInt  cRun('r',"run","runnumber","run number",CmdArg::isREQ);
  CmdArgStr  cMalta('m',"malta","file","malta data file",CmdArg::isREQ);
  CmdArgStr  cDiamond('d',"diamond","file","diamond data file",CmdArg::isREQ);
  CmdArgInt  cEvents('e',"events","number","max number of L1IDs");
  CmdArgBool cVerbose('v',"verbose","turn on verbose mode");
  CmdArgStr  cOutdir('o',"output","output","output directory");
  
  CmdLine cmdl(*argv,&cRun,&cMalta,&cDiamond,&cEvents,&cVerbose,&cOutdir,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);
  
  int run(cRun);
  int sector_irr=sectorIrrad(run);
  cout << "The run number " << run << " is in the " << sector_irr << " sector" <<endl;
  uint32_t maxEvents(cEvents);
  bool bMaxEvents = (cEvents.flags()&CmdArg::GIVEN);
  string maltafile(cMalta);
  string diamondfile(cDiamond);
  string outputdir(".");
  if(cOutdir.flags()&CmdArg::GIVEN){outputdir=string(cOutdir);}
  string outputfile(Form("%s/reco_%06i.root",outputdir.c_str(),run));
  
  DiamondStage ds;
  ds.Open(diamondfile);
  
  //read input tree 
  cout << "Read input file: " << maltafile << endl;
  TFile* inFile = TFile::Open(maltafile.c_str());
  TTree* inTree = (TTree*) inFile->Get("MALTA");
  uint32_t group;
  uint32_t dcolumn;
  uint32_t word1;
  uint32_t word2;
  uint32_t l1id;
  float    timer;

  TBranch *b_group;
  TBranch *b_dcolumn;
  TBranch *b_word1;
  TBranch *b_word2;
  TBranch *b_timer;
  TBranch *b_l1id;
  
  inTree->SetBranchStatus("*",0);
  inTree->SetBranchStatus("group",1);
  inTree->SetBranchStatus("dcolumn",1);
  inTree->SetBranchStatus("word1",1);
  inTree->SetBranchStatus("word2",1);
  inTree->SetBranchStatus("timer",1);
  inTree->SetBranchStatus("l1id",1);
  
  inTree->SetBranchAddress("group",&group, &b_group);
  inTree->SetBranchAddress("dcolumn",&dcolumn, &b_dcolumn);
  inTree->SetBranchAddress("word1",&word1, &b_word1);
  inTree->SetBranchAddress("word2",&word2, &b_word2);
  inTree->SetBranchAddress("timer",&timer, &b_timer);
  inTree->SetBranchAddress("l1id" ,&l1id,  &b_l1id );
	
  //create output tree
  cout << "Create output file: " << outputfile << endl;
  TFile *outFile = new TFile(outputfile.c_str(),"recreate","8");
  TTree *outTree = new TTree("DiamondReco","MALTA_Diamond");
  
  double stage_x;
  double stage_y;
  double ail;
  float diamond_time;
  float malta_time;
  float delta_malta_time;
  uint32_t hits[16][64];
  uint32_t cl_n;
  const uint32_t MAXCLUSTERS=5;
  float cl_x[MAXCLUSTERS];
  float cl_y[MAXCLUSTERS];
  float cl_s[MAXCLUSTERS];
  vector<pair<int,int>> vhits;
  
  //add branches to output tree
  outTree->Branch("stage_x", &stage_x);
  outTree->Branch("stage_y", &stage_y);
  outTree->Branch("ail", &ail);
  outTree->Branch("diamond_time", &diamond_time);
  outTree->Branch("malta_time", &malta_time);
  outTree->Branch("delta_malta_time", &delta_malta_time);
  outTree->Branch("hits",&hits,"hits[16][64]/i");
  outTree->Branch("cl_n",&cl_n,"cl_n/i");
  outTree->Branch("cl_x",&cl_x,"cl_x[cl_n]/f");
  outTree->Branch("cl_y",&cl_y,"cl_y[cl_n]/f");
  outTree->Branch("cl_s",&cl_s,"cl_s[cl_n]/f");

  TGraph *gTimeCorr=new TGraph();
  gTimeCorr->SetName("gTimeCorr");
  gTimeCorr->SetTitle(";Malta Time[s];Diamond Time[s]");
  
  TH1F *hMaltaDeltaTime=new TH1F("hMaltaDeltaTime",";Delta Time [s]", 60, 0, 6);
  TH1F *hMaltaEventTime=new TH1F("hMaltaEventTime",";Event Time [s]", 600, 0, 6);
  TH1F *hMaltaTime=new TH1F("hMaltaTime",";MALTA Time [ns]",1000, 0, 810e3);
  TH1F *hTimeDiff=new TH1F("hTimeDiff",";Time Diff", 32401, -12.5, 810012.5); // Chiara histogram of time diff
  TH1F *htime_diff=new TH1F("htime_diff",";time_diff", 100, -2, 2); // Chiara histogram of time diff
  TGraph2D *gClusterSizeMap=new TGraph2D();
  gClusterSizeMap->SetName("gClusterSizeMap");
  gClusterSizeMap->SetTitle(";MALTA X [#mum];MALTA Y [#mum]");
  TH2D* hClusterSizeMap=new TH2D("hClusterSizeMap","ClusterSizeMap;MaltaY;MaltaX",51,2348.1,2348.2,51,1.7,1.8);
  
  TH2D* hClusterSizeMap_newcoord=new TH2D("hClSizeMap_nc","ClSizeMap;newMaltaY;newMaltaX"        ,ds.GetYbins()+1,-0.001,ds.GetYmax()-ds.GetYmin()+0.001,ds.GetXbins()+1,0-0.001,ds.GetXmax()-ds.GetXmin()+0.001);
  TH2D* hClusterSizeMap_newcoord_den=new TH2D("hClSizeMap_nc_den","ClSizeMap;newMaltaY;newMaltaX",ds.GetYbins()+1,-0.001,ds.GetYmax()-ds.GetYmin()+0.001,ds.GetXbins()+1,0-0.001,ds.GetXmax()-ds.GetXmin()+0.001);

  cout << "Loop over the data: " << ds.GetYstep() << " , " << ds.GetXstep() << endl;
  MiniMaltaData md;
  MiniMaltaData mem_md;
  uint32_t entry=0;
  float malta_time0=0;
  float mem_malta_time=0;
  uint32_t mem_l1id=0x0FFFFFFF;
  uint32_t found_step=0;
  float mem_timing=0;
  bool first_hit=true;
  float avg_cluster_size=0;
  float events=0;
  float newmalta_x;
  float newmalta_y;
  uint32_t mem_stage=300000;
  float time_shift=0;
  if (run==6028) time_shift=1;

  
  for (unsigned int s=0; s<2400; s++) {
     cout << " " << s << " : " << ds.GetX(s) << " , " <<  ds.GetY(s) << " -- " << ds.GetT(s)-ds.GetT(0);
     if (s!=0) cout << "  dT: " << ds.GetT(s)-ds.GetT(s-1);
     cout  << endl;
  }

  int countL1=0;
  //Loop over entries
  while(true){
    inTree->GetEntry(entry);
    if(inTree->LoadTree(entry)<0){break;}
    
    entry++;



    if(entry%100000==0) cout << "Processed " << entry << " entries... L1ID " << l1id << "/" << maxEvents << endl;
      
    if(bMaxEvents && l1id > maxEvents){
      cout << "Reached number of events requested..." << endl;
      outTree->Write();
      break;
    }
      
 

    
   

/////////////////////////////////////////////////////////////////////////
    // Chiara cleaning to choose the region irradiated
    if (sector_irr==1) {
	if (group==3 or group==0) continue;
	if (dcolumn>=4) continue;
    } //for p-well 
    if (sector_irr==0) {
      if (group==3 or group==1) continue;
      if (dcolumn<4) continue;
    } //for malta 
    if (sector_irr==3) {
      if (group==1 or group==0) continue;
      if (dcolumn>=4) continue;
    } //for n-gap 
   //cout << "malta_time0 " << malta_time0 << " timer: " << timer << endl;
   if(first_hit){
      malta_time0=timer;
      first_hit = false;
    }
malta_time=timer-malta_time0;
    hMaltaEventTime->Fill(malta_time-mem_malta_time);

///////////////////////////////////////////////////////////////////////
    //unpack the MALTA words to get hit contents
    md.setWord1(word1);
    md.setWord2(word2);
    md.unpack();


    if (mem_md.getL1id() == md.getL1id()  &&
    	mem_md.getBcid() == md.getBcid()  &&
	mem_md.getWinid() == md.getWinid() &&
	mem_md.getDcolumn() == md.getDcolumn() &&
	mem_md.getParity() == md.getParity()  &&
	mem_md.getGroup() == md.getGroup()  &&
	mem_md.getPixel() == md.getPixel() ) {
      continue;
    }
    countL1+=1;	    

    //event splitting
    float timing= md.getBcid()*25.0;
    float timeDiff=(timing-mem_timing);
    hTimeDiff->Fill(timeDiff);
    if (timeDiff<0) timeDiff+=810e3;
    if (cVerbose) cout << " Time: " << timing << " , and timeDiff: " << timeDiff << endl;
    
    if (mem_timing!=0 and timeDiff>100) { //this is the time cutting I can change 

      events+=1;

      if(cVerbose)cout << "mem_l1id: " << mem_l1id << " l1id:" << l1id << endl;

      if(mem_l1id!=l1id) { // or countL1<10){
	if(cVerbose){cout << "Find the closest diamond step" << endl;}
	
        float minVal=0;
        cout << endl <<"malta_time " << malta_time+time_shift << endl; //was1.3
	if (malta_time!=0) found_step = findStep(ds, malta_time+time_shift,minVal);//+0.8);//-0.45);
	else                found_step = findStep(ds, malta_time,minVal);//-0.45);
        htime_diff->Fill(minVal);
	cout << " entry: " << entry << " , countL1: " << countL1 << " ,,, " << found_step << " ,,, " << mem_stage << " -- " << l1id << " , " << mem_l1id << endl;
        if(mem_l1id!=l1id) countL1=0;

        //if (fabs(minVal)>0.60 and ) {
       if (minVal>0.60 or fabs(minVal)>0.65 ) {
	 int new_step = findStep(ds, malta_time+time_shift-1,minVal); //was 1 
	 cout << "fine tuning went from " << found_step << " to: " << new_step << endl;
         //if (fabs(minVal)<1.2) found_step=new_step; //was 1.2
         //else                   exit(-1);
	 found_step=new_step; 
        }

	/* if (mem_stage==found_step) {
          //exit(0);
          cout << "Try new procedure ..." << endl;
          found_step = findStep(ds, malta_time-0.8,minVal);
          if (mem_stage==found_step) exit(0);
        }*/
        mem_stage = found_step;
	if(found_step!=0xFFFFFFFF){
	  stage_x = ds.GetX(found_step);
	  stage_y = ds.GetY(found_step);
	  ail = ds.GetAil(found_step);
	  diamond_time=ds.GetT(found_step)-ds.GetT(0);
	  gTimeCorr->SetPoint(gTimeCorr->GetN(),malta_time,diamond_time);
	  delta_malta_time=malta_time-mem_malta_time;
	  hMaltaDeltaTime->Fill(delta_malta_time);
	  cout << "Matched: l1id " << l1id << " diamond: " << found_step << endl; //not commented
	}else{
	  cout << "Could not find a step for this l1id: " << mem_l1id << endl;
	}
	mem_malta_time=malta_time;
	mem_l1id = l1id;
	avg_cluster_size/=events;
	float malta_x=36.4*64-stage_y;
	float malta_y=stage_x;
	newmalta_x=stage_y-ds.GetYmin();
	newmalta_y=0.1-(stage_x-ds.GetXmin());
cout << "NewMaltaX is " << newmalta_x << " and NewMaltaY is " << newmalta_y <<endl;
gClusterSizeMap->SetPoint(gClusterSizeMap->GetN(),malta_x,malta_y,avg_cluster_size);
	hClusterSizeMap->Fill(malta_x,malta_y,avg_cluster_size);
	avg_cluster_size=0;
	events=0;
      }
      

      //build clusters
      if(cVerbose){cout << "Clusterize" << endl;}	
      vector<vector<pair<int,int>>> clusters = clusterize(vhits);
      
      //format clusters
      if(cVerbose){cout << "Format" << endl;} 
      cl_n=clusters.size();
      for(uint32_t i=0;i<clusters.size();i++){
	float cx=0;
	float cy=0;
	for(uint32_t j=0;j<clusters.at(i).size();j++){
	  cx+=clusters.at(i).at(j).first;
	  cy+=clusters.at(i).at(j).second;
	}
	cx/=(float)clusters.at(i).size();
	cy/=(float)clusters.at(i).size();
	cl_x[i]=cx;
	cl_y[i]=cy;
	cl_s[i]=clusters.at(i).size();
	avg_cluster_size+=clusters.at(i).size();
	hClusterSizeMap_newcoord->Fill(newmalta_x,newmalta_y, clusters.at(i).size());
	hClusterSizeMap_newcoord_den->Fill(newmalta_x,newmalta_y);
      }

      if(cVerbose){cout << "Fill" << endl;} 
      if(cVerbose){cout << "Fill tree" << endl;} 
      outTree->Fill();
      

      for(uint32_t i=clusters.size();i<MAXCLUSTERS;i++){
	cl_x[i]=0;
	cl_y[i]=0;
	cl_s[i]=0;
      }
      /*
      //clear the hit contents
      if(cVerbose){cout << "Clear" << endl;} 
      for (int i=0; i<16; i++){
	for(int j=0; j<64;j++){
	  hits[i][j] = 0;
	}
      }
      */
      vhits.clear();
      
    }
    
    if(cVerbose){cout << "Accumulate hits" << endl;} 
    for (uint32_t i=0; i<md.getNhits(); i++){
      uint32_t col = md.getHitColumn(i);
      uint32_t row = md.getHitRow(i);
      //if(md.getHitColumn(i)< 2) continue;
      //if(md.getHitColumn(i)> 5) continue;
      //if(md.getHitRow(i)<52) continue;
      //if(md.getHitRow(i)>55) continue;
      //if (md.getHitRow(i)!=22) continue;
      //if (md.getHitColumn(i)!=1) continue;
      hits[col][row]+=1;
      vhits.push_back(pair<int,int>(col,row));
    }

    //keep the malta word for next event
    if(cVerbose){cout << "Keep for next entry" << endl;} 
    mem_md=md;
    mem_timing = timing;
          
    hMaltaTime->Fill(timing);
  }

  float tmpMin=0;
  //Last event might not be a change of L1ID
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  if(mem_l1id!=l1id){
    if(cVerbose){cout << "Find the closest diamond step" << endl;}
    found_step = findStep(ds, malta_time+1.3,tmpMin);
    if(found_step!=0xFFFFFFFF){
      stage_x = ds.GetX(found_step);
      stage_y = ds.GetY(found_step);
      ail = ds.GetAil(found_step);
      diamond_time=ds.GetT(found_step)-ds.GetT(0);
      gTimeCorr->SetPoint(gTimeCorr->GetN(),malta_time,diamond_time);
      delta_malta_time=malta_time-mem_malta_time;
      hMaltaDeltaTime->Fill(delta_malta_time);
    }else{
      cout << "Could not find a step for this l1id: " << mem_l1id << endl;
    }
    avg_cluster_size/=events;
    float malta_x=36.4*64-stage_y;
    float malta_y=stage_x;

    gClusterSizeMap->SetPoint(gClusterSizeMap->GetN(),malta_x,malta_y,avg_cluster_size);
    hClusterSizeMap->Fill(malta_x,malta_y,avg_cluster_size);
  }
  
  //build clusters
  vector<vector<pair<int,int>>> clusters = clusterize(vhits);
  //format clusters
  for(uint32_t i=0;i<clusters.size();i++){
    float cx=0;
    float cy=0;
    for(uint32_t j=0;j<clusters.at(i).size();j++){
      cx+=clusters.at(i).at(i).first;
      cy+=clusters.at(i).at(i).second;
    }
    cx/=(float)clusters.at(i).size();
    cy/=(float)clusters.at(i).size();
    cl_x[i]=cx;
    cl_y[i]=cy;
    cl_s[i]=clusters.at(i).size();
    avg_cluster_size+=clusters.at(i).size();
  }
  for(uint32_t i=clusters.size();i<MAXCLUSTERS;i++){
    cl_x[i]=0;
    cl_y[i]=0;
    cl_s[i]=0;
  }
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  

  //Fill tree
  outTree->Fill();
    
  avg_cluster_size/=events;
  float malta_x=36.4*64-stage_y;
  float malta_y=stage_x;
  gClusterSizeMap->SetPoint(gClusterSizeMap->GetN(),malta_x,malta_y,avg_cluster_size);
  hClusterSizeMap->Fill(malta_x,malta_y,avg_cluster_size);
  //Close output tree
  cout << "Close output file: " << outputfile << endl;
  cout << " StageX is " << stage_x << " and StageY is " << stage_y << endl;
  cout << " MaltaX is " << malta_x << " and MaltaY is " << malta_y << ". NewMaltaX is " << newmalta_x << " and NewMaltaY is " << newmalta_y << endl;
  gTimeCorr->Write();
  gClusterSizeMap->Write();
  hMaltaDeltaTime->Write();
  hMaltaEventTime->Write();
  hClusterSizeMap->Write();
  hClusterSizeMap_newcoord->Divide(hClusterSizeMap_newcoord_den);
  hClusterSizeMap_newcoord->Write();
  hClusterSizeMap_newcoord_den->Write();
  hMaltaTime->Write();
  hTimeDiff->Write();
  htime_diff->Write();
  outFile->Close();
  
  cout << "Have a nice day" << endl;
  return 0;
}




