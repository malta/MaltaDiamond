  TTree *treeOutClusters = new TTree("clusters", "clusters");

  unsigned int clusterBcid = 0;
  unsigned int clusterId = 0;
  unsigned int clusterSize = 0;
  unsigned int clusterSizeX = 0;
  unsigned int clusterSizeY = 0;
  double clusterEccentricity = 0.;
  double clusterCharge = 0.;
  double clusterWinID = 0.;
  double clusterSeedCharge = 0.;
  double clusterX = 0.;
  double clusterY = 0.;
  vector<unsigned int> clusterHitID;
  vector<unsigned int> clusterHitX;
  vector<unsigned int> clusterHitY;
  vector<double> clusterHitWinID;

//////////
  // looping
  //////////

  unsigned int nEntries = treeIn -> GetEntries();
  cout << __PRETTY_FUNCTION__ << ":     original nEntries = " << nEntries << endl;
  if(nEntriesMax != 0){
    nEntries = nEntriesMax;
  }
  cout << __PRETTY_FUNCTION__ << ": looping over nEntries = " << nEntries << endl;

  vector<hitClass *> hit;
  unsigned int currentBcid = 0;
  for(unsigned int iEntry=0; iEntry<nEntries; iEntry++){

    loadBar(iEntry, nEntries);
    
    treeIn -> GetEntry(iEntry);

    // rejecting hits from other modules
    if(eta_module != module) continue;
    
    // rejecting hits under threshold
    const double totalCharge = charge_track + charge_noise / CHARGERESCALEFACTOR;
    if(totalCharge < threshold) continue;

    //    cout << "-------------------------------------------_____> " << iEntry << ": (" << eta_index << "," << phi_index << ") => bcid = " <<bcid  << endl;

    if(bcid != currentBcid){ // if new bcid, clusterize and reset hit map

      // cout << "++++++++++++ PROCESSING BCID " << currentBcid << endl;
      // for(unsigned int i=0; i<hit.size(); i++){
      //   cout << "---------- HIT # " << i << endl;
      //   hit[i] -> print();
      // }

      ////////////////////////////////////////////////////////////////////////////////
      // clustering
      vector<clusterClass *> cluster;
      clusterize(cluster, hit, bcid-1, clusterGap, debug);
      ////////////////////////////////////////////////////////////////////////////////

      // analyzing clusters
      if(debug) cout << __PRETTY_FUNCTION__ << ": analyzing clusters" << endl;
      for(unsigned int iCluster=0; iCluster<cluster.size(); iCluster++){
        cluster[iCluster] -> analyze();
      }
      
      // writing text file
      if(debug) cout << __PRETTY_FUNCTION__ << ": writing clusters to file" << endl;
      if(writeTxtFile){
        writeClustersToFile(cluster, fileTxt);
      }

      // writing output tree
      if(debug) cout << __PRETTY_FUNCTION__ << ": writing output tree" << endl;
      for(unsigned int iCluster=0; iCluster<cluster.size(); iCluster++){
        clusterBcid = cluster[iCluster] -> getBcid();
        clusterId = cluster[iCluster] -> getId();
        clusterSize = cluster[iCluster] -> getNHits();
        clusterSizeX = cluster[iCluster] -> getSizeX();
        clusterSizeY = cluster[iCluster] -> getSizeY();
        clusterEccentricity = cluster[iCluster] -> getEccentricity();
        clusterCharge = cluster[iCluster] -> getCharge();
        clusterWinID = cluster[iCluster] -> getWinID();
        clusterSeedCharge = cluster[iCluster] -> getSeedCharge();
        clusterX = cluster[iCluster] -> getX();
        clusterY = cluster[iCluster] -> getY();
        for(unsigned int iHit=0; iHit<cluster[iCluster] -> getNHits(); iHit++){
          clusterHitID.push_back(cluster[iCluster] -> getHit(iHit) -> getID());
          clusterHitX.push_back(cluster[iCluster] -> getHit(iHit) -> getX());
          clusterHitY.push_back(cluster[iCluster] -> getHit(iHit) -> getY());
          clusterHitWinID.push_back(cluster[iCluster] -> getHit(iHit) -> getWinID());
        }
        treeOutClusters -> Fill();
        clusterHitID.clear();
        clusterHitX.clear();
        clusterHitY.clear();
        clusterHitWinID.clear();
      }
      
      // clearing cluster vector
      if(debug) cout << __PRETTY_FUNCTION__ << ": clearing cluster vector" << endl;
      for(unsigned int iCluster=0; iCluster<cluster.size(); iCluster++){
        delete cluster[iCluster];
      }
      cluster.clear();
      
      // clearing hit vector
      if(debug) cout << __PRETTY_FUNCTION__ << ": clearing hit vector" << endl;
      for(unsigned int iHit=0; iHit<hit.size(); iHit++){
        delete hit[iHit];
      }
      hit.clear();
      
      // going to next bcid
      currentBcid = bcid;
      if(debug) cout << __PRETTY_FUNCTION__ << ": processing bcid " << currentBcid << endl;
    }
    else{
      ;
    }

    //    if(debug) cout << __PRETTY_FUNCTION__ << ": adding bcid=" << bcid << " x,y=" << eta_index << "," << phi_index << endl;
    hit.push_back(new hitClass(eta_index, phi_index, totalCharge, windowID, iEntry));
  }

  
