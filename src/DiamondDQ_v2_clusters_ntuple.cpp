//Combine diamond .dat files and MaltaMultiDAQ output
//April 26, 2019 
//patrick.moriishi.freeman@cern.ch
//Carlos.Solans@cern.ch
//ATLAS pixel group

#include <iomanip>
#include <TTree.h>
#include <TFile.h>
#include <TBranch.h>
#include <TGraph.h>
#include <TCanvas.h>
#include <TH1I.h>
#include <TH1F.h>
#include "MiniMalta/MiniMaltaData.h"
#include "MaltaDiamond/DiamondStage.h"
#include <cmdl/cmdargs.h>
#include <functional>
#include <string>
#include <iostream>

using namespace std;

void printCluster(vector<pair<int,int>> clu) {
  cout << " Clusterof size: " << clu.size();
  for (unsigned int p=0; p<clu.size(); p++) {
    cout << " {" << (clu[p]).first << "," << (clu[p]).second << "} ";
  }
  cout << endl;
}

static bool connected(pair<int,int> hit0, pair<int,int> hit1)
{
  auto dc = std::abs(hit1.first - hit0.first);
  auto dr = std::abs(hit1.second - hit0.second);
  return (((dc == 0) && (dr <= 1)) || ((dc <= 1) && (dr == 0)));
}

vector<vector<pair<int,int>>> clusterize(vector<pair<int,int>> hits)
{
  vector<vector<pair<int,int>>> clusters;
  while (!hits.empty()) {
    auto clusterStart = --hits.end();
    auto clusterEnd = hits.end();

    // accumulate all connected hits at the end of the vector until
    // no more compatible hits can be found.
    // each iteration can only pick up the next neighboring pixels, so we
    // need to iterate until we find no more connected pixels.
    while (true) {
      auto unconnected = [&](pair<int,int> hit) {
        using namespace std::placeholders;
        return std::none_of(clusterStart, clusterEnd,
                            std::bind(connected, hit, _1));
      };
      // connected hits end up in [moreHits, clusterStart)
      auto moreHits = std::partition(hits.begin(), clusterStart, unconnected);
      if (moreHits == clusterStart) {
        break;
      }
      clusterStart = moreHits;
    }

    // construct cluster from connected hits
    vector<pair<int,int>> cluster;
    for (auto hit = clusterStart; hit != clusterEnd; ++hit) {
      cluster.push_back(*hit);
    }
    clusters.push_back(cluster);

    // remove clustered hits from further consideration
    hits.erase(clusterStart, clusterEnd);
  }
  return clusters;
}

//function to find diamond time which is closest to MALTA time
uint32_t findStep(DiamondStage &ds, vector<bool>&used_step,
	     // float & diamond_time,
	      float malta_time, uint32_t mem_l1idC,
	      bool cVerbose){
  float  diamond_time;
  uint32_t closest_step=0xFFFFFFFF;
  bool step_found=false;
  float smallest_time_diff=1E15;
  for(uint32_t step=0; step<ds.GetNSteps(); step++){
    if(used_step[step]==true){continue;}
    diamond_time = ds.GetT(step)-ds.GetT(0);
    float time_diff=diamond_time-malta_time;
    /*if(cVerbose){
      cout << " Test" 
	   << " L1ID: " << mem_l1idC
	   << " Step: " << step 
	   << " Diamond: " << diamond_time 
	   << " Malta: " << malta_time
	   << " TimeDiff: " << time_diff 
	   << endl;
    }*/
    if(abs(time_diff)<abs(smallest_time_diff)){
      smallest_time_diff=time_diff;
      closest_step=step;
      step_found=true;
      diamond_time = ds.GetT(closest_step)-ds.GetT(0);
    }
  }
  if(step_found){
    if(cVerbose){
      cout << " ====" 
	   << " L1ID: " << mem_l1idC
	   << " Step: " << closest_step 
	   << " TimeDiff: " << smallest_time_diff 
	   << endl;
    }
    //used_step[closest_step]=true;
  }
  return closest_step; 
}

uint32_t sectorIrrad(int run){
  uint32_t sector_irr=0;
  if(run==6026 or run==6029 or run==6030 or run==6031 or run==6032 or run==6036 or run==6039 or run==6043 or run==6045 or run==6032 or run==6052 or run==6057 or run==6060 or run==6062 or run==6063) sector_irr = 1;//pwell
  else if(run==6027 or run==6033 or run==6037 or run==6040 or run==6041 or run==6046 or run==6047 or run==6048 or run==6049 or run==6053 or run==6055 or run==6058) sector_irr = 3;//ngap
  else if(run==6028 or run==6034 or run==6035 or run==6038 or run==6042 or run==6044 or run==6050 or run==6051 or run==6054 or run==6056 or run==6059 or run==6061 or run==6064 or run==6065) sector_irr = 0;//malta
  return sector_irr;
}

int main(int argc, char *argv[]) {
  
  CmdArgInt  cRun('r',"run","runnumber","run number",CmdArg::isREQ);
  CmdArgStr  cMalta('m',"malta","file","malta data file",CmdArg::isREQ);
  CmdArgStr  cDiamond('d',"diamond","file","diamond data file",CmdArg::isREQ);
  CmdArgInt  cEvents('e',"events","number","max number of L1IDs");
  CmdArgBool cVerbose('v',"verbose","turn on verbose mode");
  CmdArgStr  cOutdir('o',"output","output","output directory");
  
  CmdLine cmdl(*argv,&cRun,&cMalta,&cDiamond,&cEvents,&cVerbose,&cOutdir,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);
  
  int run(cRun);
  string maltafile(cMalta);
  string diamondfile(cDiamond);
  string outputdir(".");
  uint32_t sector_irr = sectorIrrad(run);
  if(cOutdir.flags()&CmdArg::GIVEN){outputdir=string(cOutdir);}
  string outputfile(Form("%s/anaClustersNtuple_%06i.root",outputdir.c_str(),run));

  //declare diamond stage class for parsing .dat file
  DiamondStage ds;
  ds.Open(diamondfile);
  
  //read input tree 
  cout << "Read input file: " << maltafile << endl;
  TFile* inFile = TFile::Open(maltafile.c_str());
  TTree* inTree = (TTree*) inFile->Get("MALTA");
  uint32_t word1;
  uint32_t word2;
  uint32_t l1idC;
  float    timer;

  TBranch *b_word1;
  TBranch *b_word2;
  TBranch *b_timer;
  TBranch *b_l1idC;
  
  inTree->SetBranchAddress("word1",&word1, &b_word1);
  inTree->SetBranchAddress("word2",&word2, &b_word2);
  inTree->SetBranchAddress("timer",&timer, &b_timer);
  inTree->SetBranchAddress("l1idC",&l1idC, &b_l1idC);
	
  //create output tree
  cout << "Create output file: " << outputfile << endl;
  TFile* outFile = new TFile(outputfile.c_str(),"recreate","8");
  TTree *outTree = new TTree("DiamondAnaTuple","MALTA_Diamond");

  TGraph *gTimeCorr=new TGraph();
  gTimeCorr->SetName("gTimeCorr");
  gTimeCorr->SetName("Malta Time[s];Diamond Time[s]");
  
  //TH1F *hDeltaDiamond=new TH1F("hDeltaDiamond",";Delta Time [s]", 100, 0, 5);
  //TH1F *hDeltaMalta=new TH1F("hDeltaMalta",";Delta Time [s]", 100, 0, 5);
  TH1I *hClusterSize=new TH1I("hClusterSize",";Cluster Size",5, 0.5, 5.5);
  TH1F *hTiming=new TH1F("hTiming",";Timing",1000, 0, 810e3);
  double x, y, ail;
  float diamond_time,malta_time,malta_time_gap, delta_malta_time, malta_time_last;
  uint32_t pixelX, pixelY, clusterSize; 


  //add bracnhes to output tree
  outTree->Branch("x", &x);
  outTree->Branch("y", &y);
  outTree->Branch("ail", &ail);
  outTree->Branch("diamond_time", &diamond_time);
  outTree->Branch("malta_time", &malta_time);
  outTree->Branch("delta_malta_time", &delta_malta_time);
  outTree->Branch("malta_time_gap", &malta_time_gap); //time between first and last entry in l1 trigger
  outTree->Branch("pixelX",&pixelX);
  outTree->Branch("pixelY",&pixelY);
  outTree->Branch("clusterSize",&clusterSize);
  
  cout << "Loop over the data" << endl;
  MiniMaltaData md;
  uint32_t entry=0;
  uint32_t malta_time0=0;
  uint32_t mem_l1idC=1;
  uint32_t mem_found_step=0;
  uint32_t found_step=0;
  uint32_t next_found_step=0;
  bool first_hit=true;
  vector<bool> used_step; 
  vector<pair <int, int>> seeds, neighbors;
  pair <int, int> pixel;
  int dX, dY, i, j, k, m;
  int maltaGroup = -1;
  int maltaDColumn = -1;
  int maltaL1id = -1;
  int maltaBCID = -1;
  int maltaParity = -1;
  int maltaPixel = -1;
  int maltaWinID = -1;
 
  int prevGroup = -1;
  int prevDColumn = -1;
  int prevL1id = -1;
  int prevBCID = -1;
  int prevParity = -1;
  int prevPixel = -1;
  int prevWinID = -1;
  //set used steps to false
  for(uint32_t step=0; step<ds.GetNSteps(); step++){
    used_step.push_back(false);
  }
  // checking if level 1 trigger has changed
  //////////////////////////////////////////////////////////////////////////////////
  int newBcid{0}, oldBcid{0},  timing{0}, skips{0}, dC{0}, dR{0}, currentCluster{2};
  int oldCol{0}, oldRow{0}, col{66}, row{66};
  while(true){
    //loop over all entries
    inTree->GetEntry(entry);

    md.setWord1(word1);
    md.setWord2(word2);
    md.unpack();

    entry++;

    maltaGroup = md.getGroup();
    maltaL1id = md.getL1id();
    maltaBCID = md.getBcid();
    maltaParity = md.getParity();
    maltaPixel = md.getPixel();
    maltaDColumn = md.getDcolumn();
    maltaWinID = md. getWinid();
   //cut out irrelevant sectors

  //  cout<<"malta group: " << maltaGroup << endl;
   // cout<<"l1idC: " << l1idC << endl;
   // if (maltaDColumn>=5) continue;
   // Duplicate removal

    if ( prevL1id == maltaL1id  &&
      prevBCID == maltaBCID  && 
      prevWinID== maltaWinID &&
      prevDColumn == maltaDColumn &&
      prevParity  == maltaParity  &&
      prevGroup== maltaGroup   &&
      prevPixel== maltaPixel ) {
    continue; 
    }
     
 
    prevGroup = maltaGroup;
    prevL1id = maltaL1id;
    prevBCID = maltaBCID;
    prevParity = maltaParity;
    prevPixel = maltaPixel;
    prevDColumn = maltaDColumn;
    prevWinID = maltaWinID;
    
    if(inTree->LoadTree(entry)<0){break;}

    if(cEvents.flags()&CmdArg::GIVEN && l1idC>=(uint32_t)cEvents){
      cout << "Reached number of events requested..." << endl;
      break;
    }
    malta_time_last = timer - malta_time0;
    //for steps with new level 1 trigger ////////////////////
    if(l1idC!=mem_l1idC){
      mem_l1idC=l1idC;  
      malta_time_gap = malta_time_last-malta_time;
      //option 2
      if(first_hit){
        malta_time0=timer;
        first_hit = false;
      }
      
      malta_time=timer-malta_time0;
      
      if(cVerbose){cout << "Find the closest diamond step" << endl;}
      found_step = findStep(ds, used_step,
                              malta_time, mem_l1idC,
                              cVerbose);

      if(found_step!=0xFFFFFFFF){
        x = ds.GetX(found_step);
        y = ds.GetY(found_step);
        //cout<<"the found step is: " <<found_step<<" with x= "<<x << "and y= "<<y<< endl;
        ail = ds.GetAil(found_step);
        diamond_time=ds.GetT(found_step)-ds.GetT(0);
        //cout << "Step: " << setw(4) << (found_step+1) << "/" << ds.GetNSteps() << endl;	
        gTimeCorr->SetPoint(gTimeCorr->GetN(),malta_time,diamond_time);
        mem_found_step=found_step;
        used_step.at(found_step)=true;   
      }else{
        cout << "Could not find a step for this l1id: " << mem_l1idC << endl;
      }
      //end of conditional for a new l1id    
      //keep the l1id in memory
    }
    ///end of conditional for new L1id
    //if it's the same l1id...
    //unpack the MALTA words to get hit contents
   // if (maltaGroup==3 or maltaGroup==0) continue;
    if (abs(x) <= 1e-3 or abs(y) <1e-3) continue;
    if (sector_irr==1) {
	if (maltaGroup==3 or maltaGroup==0) continue;
	if (maltaDColumn>=4) continue;
    } //for p-well 
    if (sector_irr==0) {
      if (maltaGroup==3 or maltaGroup==1) continue;
      if (maltaDColumn<4) continue;
    } //for malta 
    if (sector_irr==3) {
      if (maltaGroup==1 or maltaGroup==0) continue;
      if (maltaDColumn>=4) continue;
    } //for n-gap 
     /*
    //fiducial region cut
    int Xcoord= maltaDColumn*2+i/8;
    int Ycoord= maltaGroup*16+(1-maltaParity)*8+i%8;
    if (Ycoord<25) continue;
    if (Ycoord>28) continue;
    if (Xcoord< 3) continue;
    if (Xcoord> 6) continue;  
    */
    oldBcid = newBcid;
    newBcid = md.getBcid();
    uint32_t oldTiming = timing;
    timing = (newBcid - oldBcid)*25;
    //correct for overflow that results in negative time differences
    if (timing<0) timing+=810e3;
    
    //  if time difference between words is large, read out old cluster and make a new cluster
    if (timing > 100 && newBcid != 0){
      vector<vector<pair <int, int>>> dupClusters;
      vector<pair <int, int>> cHits,  duplicates;
      for(auto hit: seeds){
       cHits.push_back(hit); 
       /* if(find(cHits.begin(),cHits.end(),hit) != cHits.end()){
          duplicates.push_back(hit);
          dupClusters.push_back(duplicates);
          duplicates.erase(duplicates.begin());  
	}else cHits.push_back(hit); */
      }//add hits from seeds that are unique
      seeds.clear();
      vector<vector<pair<int,int>>> clusters = clusterize(cHits);
      
      for(auto cluster: clusters){
        hClusterSize->Fill(cluster.size()); 
        pixelX = cluster[0].first;
        pixelY = cluster[0].second;
        clusterSize = cluster.size();
        outTree->Fill();
      }
      /*
      for(auto cluster: dupClusters){
        hClusterSize->Fill(cluster.size()); 
        pixelX = cluster[0].first;
        pixelY = cluster[0].second;
        clusterSize = cluster.size();
        outTree->Fill();
      }
      */
    } // end of time difference conditional for clusterization

    for (uint32_t i=0; i<md.getNhits(); i++){
      oldCol = col;
      oldRow = row;
      col = md.getHitColumn(i);
      row = md.getHitRow(i);
      pixel.first = col;
      pixel.second = row;
      seeds.push_back(pixel);
    }
   
    hTiming->Fill(timing);


  } //////////////////////////////////////////////////////////////////////////////
  //end of while loop over entries
 
  //print out the empty steps
  for(uint32_t step=0; step<ds.GetNSteps(); step++){
    if(used_step[step]==false){
      cout << "No data found for step: " << step << endl;
    }
  }
  
  TCanvas *c1 = new TCanvas("c1");
  hClusterSize->Draw("");
  hClusterSize->Write("hCluster");
  c1->SaveAs(Form("%s/ClusterSizes_%i.C",outputdir.c_str(),run));
  c1->SaveAs(Form("%s/ClusterSizes_%i.png",outputdir.c_str(),run));
  c1->SaveAs(Form("%s/ClusterSizes_%i.pdf",outputdir.c_str(),run));

  
  TCanvas *cTime = new TCanvas("cTime");
  hTiming->Draw("");
  hTiming->Write("hTiming");
  c1->SaveAs(Form("%s/ClusterTiming_%i.C",outputdir.c_str(),run));
  c1->SaveAs(Form("%s/ClusterTiming_%i.png",outputdir.c_str(),run));
  c1->SaveAs(Form("%s/ClusterTiming_%i.pdf",outputdir.c_str(),run));


  //Close output tree
  cout << "Close output file: " << outputfile << endl;
  gTimeCorr->Write();
  outFile->Write();
  outFile->Close();
  
  cout << "Have a nice day" << endl;
  return 0;
} //end of main




