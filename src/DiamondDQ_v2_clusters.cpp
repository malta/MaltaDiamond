//Combine diamond .dat files and MaltaMultiDAQ output
//April 26, 2019 
//patrick.moriishi.freeman@cern.ch
//Carlos.Solans@cern.ch
//ATLAS pixel group

#include <iomanip>
#include <TTree.h>
#include <TFile.h>
#include <TBranch.h>
#include <TGraph.h>
#include <TCanvas.h>
#include <TH1I.h>
#include <TH1F.h>
#include "MiniMalta/MiniMaltaData.h"
#include "MaltaDiamond/DiamondStage.h"
#include <cmdl/cmdargs.h>
#include <functional>
#include <string>
#include <iostream>

using namespace std;

static bool connected(pair<int,int> hit0, pair<int,int> hit1)
{
  auto dc = std::abs(hit1.first - hit0.first);
  auto dr = std::abs(hit1.second - hit0.second);
  return (((dc == 0) && (dr <= 1)) || ((dc <= 1) && (dr == 0)));
}

vector<vector<pair<int,int>>> clusterize(vector<pair<int,int>> hits)
{
  vector<vector<pair<int,int>>> clusters;
  while (!hits.empty()) {
    auto clusterStart = --hits.end();
    auto clusterEnd = hits.end();

    // accumulate all connected hits at the end of the vector until
    // no more compatible hits can be found.
    // each iteration can only pick up the next neighboring pixels, so we
    // need to iterate until we find no more connected pixels.
    while (true) {
      auto unconnected = [&](pair<int,int> hit) {
        using namespace std::placeholders;
        return std::none_of(clusterStart, clusterEnd,
                            std::bind(connected, hit, _1));
      };
      // connected hits end up in [moreHits, clusterStart)
      auto moreHits = std::partition(hits.begin(), clusterStart, unconnected);
      if (moreHits == clusterStart) {
        break;
      }
      clusterStart = moreHits;
    }

    // construct cluster from connected hits
    vector<pair<int,int>> cluster;
    for (auto hit = clusterStart; hit != clusterEnd; ++hit) {
      cluster.push_back(*hit);
    }
    clusters.push_back(cluster);

    // remove clustered hits from further consideration
    hits.erase(clusterStart, clusterEnd);
  }
  return clusters;
}

//function to find diamond time which is closest to MALTA time
uint32_t findStep(DiamondStage &ds, vector<bool>&used_step,
	     // float & diamond_time,
	      float malta_time, uint32_t mem_l1idC,
	      bool cVerbose){
  float  diamond_time;
  uint32_t closest_step=0xFFFFFFFF;
  bool step_found=false;
  float smallest_time_diff=1E15;
  for(uint32_t step=0; step<ds.GetNSteps(); step++){
    if(used_step[step]==true){continue;}
    diamond_time = ds.GetT(step)-ds.GetT(0);
    float time_diff=diamond_time-malta_time;
    /*if(cVerbose){
      cout << " Test" 
	   << " L1ID: " << mem_l1idC
	   << " Step: " << step 
	   << " Diamond: " << diamond_time 
	   << " Malta: " << malta_time
	   << " TimeDiff: " << time_diff 
	   << endl;
    }*/
    if(abs(time_diff)<abs(smallest_time_diff)){
      smallest_time_diff=time_diff;
      closest_step=step;
      step_found=true;
      diamond_time = ds.GetT(closest_step)-ds.GetT(0);
    }
  }
  if(step_found){
    if(cVerbose){
      cout << " ====" 
	   << " L1ID: " << mem_l1idC
	   << " Step: " << closest_step 
	   << " TimeDiff: " << smallest_time_diff 
	   << endl;
    }
    //used_step[closest_step]=true;
  }
  return closest_step; 
}

int main(int argc, char *argv[]) {
  
  CmdArgInt  cRun('r',"run","runnumber","run number",CmdArg::isREQ);
  CmdArgStr  cMalta('m',"malta","file","malta data file",CmdArg::isREQ);
  CmdArgStr  cDiamond('d',"diamond","file","diamond data file",CmdArg::isREQ);
  CmdArgInt  cEvents('e',"events","number","max number of L1IDs");
  CmdArgBool cVerbose('v',"verbose","turn on verbose mode");
  CmdArgStr  cOutdir('o',"output","output","output directory");
  
  CmdLine cmdl(*argv,&cRun,&cMalta,&cDiamond,&cEvents,&cVerbose,&cOutdir,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);
  
  int run(cRun);
  string maltafile(cMalta);
  string diamondfile(cDiamond);
  string outputdir(".");
  if(cOutdir.flags()&CmdArg::GIVEN){outputdir=string(cOutdir);}
  string outputfile(Form("%s/anaClusters_%06i.root",outputdir.c_str(),run));

  //declare diamond stage class for parsing .dat file
  DiamondStage ds;
  ds.Open(diamondfile);
  
  //read input tree 
  cout << "Read input file: " << maltafile << endl;
  TFile* inFile = TFile::Open(maltafile.c_str());
  TTree* inTree = (TTree*) inFile->Get("MALTA");
  uint32_t word1;
  uint32_t word2;
  uint32_t l1idC;
  float    timer;

  TBranch *b_word1;
  TBranch *b_word2;
  TBranch *b_timer;
  TBranch *b_l1idC;
  
  inTree->SetBranchAddress("word1",&word1, &b_word1);
  inTree->SetBranchAddress("word2",&word2, &b_word2);
  inTree->SetBranchAddress("timer",&timer, &b_timer);
  inTree->SetBranchAddress("l1idC",&l1idC, &b_l1idC);
	
  //create output tree
  cout << "Create output file: " << outputfile << endl;
  TFile* outFile = new TFile(outputfile.c_str(),"recreate","8");
  TTree *outTree = new TTree("DiamondAna","MALTA_Diamond");

  TGraph *gTimeCorr=new TGraph();
  gTimeCorr->SetName("gTimeCorr");
  gTimeCorr->SetName("Malta Time[s];Diamond Time[s]");
  
  //TH1F *hDeltaDiamond=new TH1F("hDeltaDiamond",";Delta Time [s]", 100, 0, 5);
  //TH1F *hDeltaMalta=new TH1F("hDeltaMalta",";Delta Time [s]", 100, 0, 5);
  TH1I *hClusterSize=new TH1I("hClusterSize",";Cluster Size",5, 0.5, 5.5);
  TH1F *hTiming=new TH1F("hTiming",";Timing",1000, 0, 810e3);
  double x, y, ail;
  float diamond_time,malta_time,malta_time_gap, delta_malta_time, malta_time_last;
  uint32_t hits[16][64]; 
  uint32_t clusterCounts[16][64]; 

  //add bracnhes to output tree
  outTree->Branch("x", &x);
  outTree->Branch("y", &y);
  outTree->Branch("ail", &ail);
  outTree->Branch("diamond_time", &diamond_time);
  outTree->Branch("malta_time", &malta_time);
  outTree->Branch("delta_malta_time", &delta_malta_time);
  outTree->Branch("malta_time_gap", &malta_time_gap); //time between first and last entry in l1 trigger
  outTree->Branch("hits",&hits,"hits[16][64]/i");
  outTree->Branch("clusterCounts",&clusterCounts,"clusterCounts[16][64]/i");
  
  cout << "Loop over the data" << endl;
  MiniMaltaData md;
  uint32_t entry=0;
  uint32_t malta_time0=0;
  uint32_t mem_l1idC=1;
  uint32_t mem_found_step=0;
  uint32_t found_step=0;
  uint32_t next_found_step=0;
  bool first_hit=true;
  vector<bool> used_step; 
  vector<pair <int, int>> seeds, neighbors;
  vector<int> clusterSize;
  pair <int, int> pixel;
  int dX, dY, i, j, k, m;
  for(uint32_t step=0; step<ds.GetNSteps(); step++){
    used_step.push_back(false);
  }
  /////////////////////////////////////////////////////
  //loop over all entrys
  // checking if level 1 trigger has changed
  //////////////////////////////////////////////////////////////////////////////////
  int newBcid{0}, oldBcid{0},  timing{0}, skips{0}, dC{0}, dR{0}, currentCluster{2};
  int oldCol{0}, oldRow{0}, col{66}, row{66};
  while(true){
    inTree->GetEntry(entry);
    if(inTree->LoadTree(entry)<0){break;}
    
    entry++;

    if(cEvents.flags()&CmdArg::GIVEN && l1idC>=(uint32_t)cEvents){
      cout << "Reached number of events requested..." << endl;
      break;
    }
    malta_time_last = timer - malta_time0;
    //for steps with new level 1 trigger ////////////////////
    if(l1idC!=mem_l1idC){
      mem_l1idC=l1idC;  
      malta_time_gap = malta_time_last-malta_time;
      //option 2
      if(first_hit){
        malta_time0=timer;
        first_hit = false;
      }
      
      malta_time=timer-malta_time0;
      
      if(cVerbose){cout << "Find the closest diamond step" << endl;}

     // found_step=mem_found_step+1;//found_step=mem_found_step+1;
      //diamond_time=(ds.GetT(found_step)-ds.GetT(0));
      //if(abs(diamond_time-malta_time)>1.0){
        found_step = findStep(ds, used_step,
                             // diamond_time,
                              malta_time, mem_l1idC,
                              cVerbose);
 

      //}
      
      if(found_step!=0xFFFFFFFF){
        x = ds.GetX(found_step-1);
        y = ds.GetY(found_step-1);
        //cout<<"the found step is: " <<found_step<<" with x= "<<x << "and y= "<<y<< endl;
        ail = ds.GetAil(found_step);
        diamond_time=ds.GetT(found_step)-ds.GetT(0);
        //cout << "Step: " << setw(4) << (found_step+1) << "/" << ds.GetNSteps() << endl;	
        //fill in the tree
        outTree->Fill();
        //Time
        gTimeCorr->SetPoint(gTimeCorr->GetN(),malta_time,diamond_time);
        mem_found_step=found_step;
        used_step.at(found_step)=true;   
      }else{
        cout << "Could not find a step for this l1id: " << mem_l1idC << endl;
      }
      //end of conditional for a new l1id    
      //keep the l1id in memory
      
      
      //clear the hit contents
      for (int i=0; i<16; i++){ 
        for(int j=0; j<64;j++){
          hits[i][j] = 0;
          clusterCounts[i][j] = 0;
        }
      }	

    }
    ///end of conditional for new L1id
    //if it's the same l1id...
    //unpack the MALTA words to get hit contents
   
    md.setWord1(word1);
    md.setWord2(word2);
    md.unpack();
    

    int maltaGroup = md.getGroup();
    int maltaDColumn = md.getDcolumn();
  
    if (maltaGroup==3 or maltaGroup==0) continue;
    if (maltaDColumn==5) continue;
    oldBcid = newBcid;
    newBcid = md.getBcid();
    timing = (newBcid - oldBcid)*25;
    if (timing<0) timing+=810e3;
    
    //  if time difference between words is large, read out old cluster and make a new cluster
    if (timing > 100 && newBcid != 0){
      //cout << "building clusters" << endl;
      //cout << "nSeeds: " << seeds.size() << endl;
      //remove duplcate pixels
      vector<pair <int, int>> cHits;
      //cout << "uncleaned cHits:" << endl;
      //for(auto hit:seeds) cout << hit.first << " / " << hit.second << endl;
      for(auto hit: seeds){
       if(find(cHits.begin(),cHits.end(),hit) == cHits.end()) cHits.push_back(hit); 
      }

      //cout << "ncHits: " << cHits.size() << endl;

      //cout << "cleaned cHits:" << endl;
      //for(auto hit:cHits) cout << hit.first << " / " << hit.second << endl;

      vector<vector<pair<int,int>>> clusters = clusterize(cHits);

     // cout << "cluster sizes " << clusters.size() << endl;

     // cout << "clusters:" << endl;
      for(auto cluster: clusters){
       // cout << "Cluster position ("<< cluster[0].first << ","<< cluster[0].second<<")" << endl;
       // cout<< "The cluster size is: " << cluster.size()<<endl;
        hClusterSize->Fill(cluster.size()); 
        hits[cluster[0].first][cluster[0].second] += 1;
        clusterCounts[cluster[0].first][cluster[0].second] += cluster.size();
        for(auto hit: cluster) {
        //  cout << hit.first << " / " << hit.second << endl;
        }
        //cout << endl << endl;
      }

      /*unsigned int i  = 0;//seeds.begin();
      while( i< seeds.size()){
        unsigned int j = i + 1;
        while(j<seeds.size()){
          if(seeds[i] == seeds[j] ){seeds.erase(seeds.at(j)); j--;}
          ++j;
        } 
        ++i;
      }*/
/*
      for (size_t i{0}; i < seeds.size(); i++ ){
        clusterSize.push_back(1); 
        neighbors.clear();
        for (size_t j{i+1}; j< seeds.size(); j++){
          dX = abs(seeds[i].first - seeds[j].first);
          dX = abs(seeds[i].second - seeds[j].second);
          if (dX +dY == 1){//count clusters of ith hit, move them to a vector called neighbors
            neighbors.push_back(seeds.at(j));
            seeds.erase(j);
            clusterSize.at(i) += 1;
            j = j-1;
          }
        }
       //loop over neighbors, count their neighbors
        for (size_t k{0}; k< neighbors.size(); j++){
          m = seeds.begin();
          for (size_t m{i+1}; m< seeds.size(); m++){
            dX = abs(seeds[m].first - neighbors[k].first );
            dY = abs(seeds[m].second - neighbors[k].second); 
            if (dX +dY == 1){
              seeds.erase(m);
              clusterSize.at(i) += 1;
              m = m-1;
            }
          }
        }
      }
     
      //clear the x and y vectors
      seeds.clear();
      */
      //cout << "clusters: " << clusters << endl;  
      seeds.clear();
    } // end of time difference conditional for clusterization
   
    //cout << "adding hits" << endl;
    for (uint32_t i=0; i<md.getNhits(); i++){
      oldCol = col;
      oldRow = row;
      col = md.getHitColumn(i);
      row = md.getHitRow(i);
      if (row == oldRow && col == oldCol) continue;
      pixel.first = col;
      pixel.second = row;
      seeds.push_back(pixel);
    }
   
    hTiming->Fill(timing);
    /*
     for (uint32_t i=0; i<md.getNhits(); i++){
      col = md.getHitColumn(i);
      row = md.getHitRow(i);
      if (row == oldRow && col == oldCol && newBcid == oldBcid )continue; 
      if ( i == i ){cout << " (x,y) = ("<<col<<","<<row<<"),  the BCID is: "<<newBcid <<endl;}
      dC = abs(col - oldCol);
      dR = abs(row - oldRow);
      oldRow = row;
      oldCol = col;
      //check if this hit is continuing a cluster
      if ( dR <= 1 && dC <= 1 && dR + dC >0 &&  timing < 1000 && skips == 0){ 
        skips = 2;
      }
      //if you have no skips and a new pair, right the old event, reset cluster count
      if ( dR == 0 && dC == 0  &&  timing == 0){
      }else if (skips == 0){
	 hClusterSize->Fill(currentCluster/2);
         hTiming->Fill(timing);
         currentCluster = 2;
      } 
      //if you it is part of the same cluster, increment the cluster counter and remove one skip
      if (skips >0) {
        skips = skips - 1;
        currentCluster = currentCluster + 1;

        if (skips == 0){
	  hClusterSize->Fill(currentCluster/2);
	    if (currentCluster/2 > 2){
      	      cout<<"There is a cluster of "<<currentCluster/2<<endl;
	    }
	} 
 
      }else{
        hits[col][row]+=1;
      }
      // if the hit is identical to the previous, don't use if for cluster size stats (since there seem to be identical events)
      if (skips == 0){}
    }
*/
  }
  //end of while loop over entries
  
  /*
  found_step = findStep(ds, used_step,
				// diamond_time,
				 malta_time, mem_l1idC,
				 cVerbose);
  if(found_step!=0xFFFFFFFF){
    x = ds.GetX(found_step);
    y = ds.GetY(found_step);
    ail = ds.GetAil(found_step);
    diamond_time=ds.GetT(found_step)-ds.GetT(0);
    if (found_step%100==0){cout << "Step: " << setw(4) << (found_step+1) << "/" << ds.GetNSteps() << endl;}	
    //fill in the tree
    outTree->Fill();
    //Time
    gTimeCorr->SetPoint(gTimeCorr->GetN(),malta_time,diamond_time);
  }else{
    cout << "Could not find a step for this l1id: " << mem_l1idC << endl;
  }
  */
 
  //print out the empty steps
  for(uint32_t step=0; step<ds.GetNSteps(); step++){
    if(used_step[step]==false){
      cout << "No data found for step: " << step << endl;
    }
  }
  
  TCanvas *c1 = new TCanvas("c1");
  hClusterSize->Draw("");
  hClusterSize->Write("hCluster");
  c1->SaveAs(Form("%s/ClusterSizes_%i.C",outputdir.c_str(),run));
  c1->SaveAs(Form("%s/ClusterSizes_%i.png",outputdir.c_str(),run));
  c1->SaveAs(Form("%s/ClusterSizes_%i.pdf",outputdir.c_str(),run));

  
  TCanvas *cTime = new TCanvas("cTime");
  hTiming->Draw("");
  hTiming->Write("hTiming");
  c1->SaveAs(Form("%s/ClusterTiming_%i.C",outputdir.c_str(),run));
  c1->SaveAs(Form("%s/ClusterTiming_%i.png",outputdir.c_str(),run));
  c1->SaveAs(Form("%s/ClusterTiming_%i.pdf",outputdir.c_str(),run));


  //Close output tree
  cout << "Close output file: " << outputfile << endl;
  gTimeCorr->Write();
  outFile->Write();
  outFile->Close();
  
  cout << "Have a nice day" << endl;
  return 0;
}




