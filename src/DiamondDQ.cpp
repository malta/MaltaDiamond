//Combine diamond .dat files and MaltaMultiDAQ output
//April 26, 2019 
//patrick.moriishi.freeman@cern.ch
//Carlos.Solans@cern.ch
//ATLAS pixel group

#include <iomanip>
#include <TTree.h>
#include <TFile.h>
#include <TBranch.h>
#include <TGraph.h>
#include <TH1F.h>
#include "MiniMalta/MiniMaltaData.h"
#include "MaltaDiamond/DiamondStage.h"
#include <cmdl/cmdargs.h>

using namespace std;

uint32_t findStep(DiamondStage &ds, vector<bool>&used_step,
	      float & diamond_time,
	      float malta_time, uint32_t mem_l1idC,
	      bool cVerbose){
  uint32_t closest_step=0xFFFFFFFF;
  bool found_step=false;
  float smallest_time_diff=1E15;
  for(uint32_t step=0; step<ds.GetNSteps(); step++){
    if(used_step[step]==true){continue;}
    diamond_time = ds.GetT(step)-ds.GetT(0);
    float time_diff=abs(diamond_time-malta_time);
    //float time_diff=diamond_time-malta_time;
    //float time_diff=malta_time-diamond_time;
    if(cVerbose){
      cout << " Test" 
	   << " L1ID: " << mem_l1idC
	   << " Step: " << step 
	   << " Diamond: " << diamond_time 
	   << " Malta: " << malta_time
	   << " TimeDiff: " << time_diff 
	   << endl;
    }
    if(time_diff<smallest_time_diff && time_diff<2){
      smallest_time_diff=time_diff;
      closest_step=step;
      found_step=true;
      diamond_time = ds.GetT(closest_step)-ds.GetT(0);
    }
  }
  if(found_step){
    if(cVerbose){
      cout << " ====" 
	   << " L1ID: " << mem_l1idC
	   << " Step: " << closest_step 
	   << " TimeDiff: " << smallest_time_diff 
	   << endl;
    }
    //used_step[closest_step]=true;
  }
  return closest_step; 
}

int main(int argc, char *argv[]) {
  
  CmdArgInt  cRun('r',"run","runnumber","run number",CmdArg::isREQ);
  CmdArgStr  cMalta('m',"malta","file","malta data file",CmdArg::isREQ);
  CmdArgStr  cDiamond('d',"diamond","file","diamond data file",CmdArg::isREQ);
  CmdArgInt  cEvents('e',"events","number","max number of L1IDs");
  CmdArgBool cVerbose('v',"verbose","turn on verbose mode");
  CmdArgStr  cOutdir('o',"output","output","output directory");
  
  CmdLine cmdl(*argv,&cRun,&cMalta,&cDiamond,&cEvents,&cVerbose,&cOutdir,NULL);
  CmdArgvIter arg_iter(argc-1,argv+1);
  cmdl.parse(arg_iter);
  
  int run(cRun);
  string maltafile(cMalta);
  string diamondfile(cDiamond);
  string outputdir(".");
  if(cOutdir.flags()&CmdArg::GIVEN){outputdir=string(cOutdir);}
  string outputfile(Form("%s/ana_%06i.root",outputdir.c_str(),run));

  //declare diamond stage class for parsing .dat file
  DiamondStage ds;
  ds.Open(diamondfile);
  
  //read input tree 
  cout << "Read input file: " << maltafile << endl;
  TFile* inFile = TFile::Open(maltafile.c_str());
  TTree* inTree = (TTree*) inFile->Get("MALTA");
  uint32_t word1;
  uint32_t word2;
  uint32_t l1idC;
  float    timer;
  TBranch *b_word1;
  TBranch *b_word2;
  TBranch *b_timer;
  TBranch *b_l1idC;
  
  inTree->SetBranchAddress("word1",&word1, &b_word1);
  inTree->SetBranchAddress("word2",&word2, &b_word2);
  inTree->SetBranchAddress("timer",&timer, &b_timer);
  inTree->SetBranchAddress("l1idC",&l1idC, &b_l1idC);
	
  //create output tree
  cout << "Create output file: " << outputfile << endl;
  TFile* outFile = new TFile(outputfile.c_str(),"recreate","8");
  TTree *outTree = new TTree("DiamondAna","MALTA_Diamond");

  TGraph *gTimeCorr=new TGraph();
  gTimeCorr->SetName("gTimeCorr");
  gTimeCorr->SetName(";Malta Time[s];Diamond Time[s]");
  
  //TH1F *hDeltaDiamond=new TH1F("hDeltaDiamond",";Delta Time [s]", 100, 0, 5);
  //TH1F *hDeltaMalta=new TH1F("hDeltaMalta",";Delta Time [s]", 100, 0, 5);
  
  double x, y, ail;
  float diamond_time,malta_time,delta_malta_time;
  uint32_t hits[16][64]; 
  
  outTree->Branch("x", &x);
  outTree->Branch("y", &y);
  outTree->Branch("ail", &ail);
  outTree->Branch("diamond_time", &diamond_time);
  outTree->Branch("malta_time", &malta_time);
  outTree->Branch("delta_malta_time", &delta_malta_time);
  outTree->Branch("hits",&hits,"hits[16][64]/i");
  
  /*
  cout << "Loop over Diamond steps" << endl;
  MiniMaltaData md;
  uint32_t entry=1;
  uint32_t malta_time0=0;
  float diamond_last_time=0.;
  float malta_last_time=0.;
  for(uint32_t step=0; step<ds.GetNSteps(); step++){
    if(cEvents.flags()&CmdArg::GIVEN && step>=(uint32_t)cEvents){
      cout << "Reached number of events requested..." << endl;
      break;
    }
    cout << "Step: " << step << "/" << ds.GetNSteps() << endl;
    //get diamond data
    x            = ds.GetX(step);
    y            = ds.GetY(step);
    diamond_time = ds.GetT(step)-ds.GetT(0);
    
    //get malta data
    for (int i=0; i<16; i++){ 
      for(int j=0; j<64;j++){
	hits[i][j] = 0;
      }
    }	
    while(true){
      inTree->GetEntry(entry);
      if(inTree->LoadTree(entry)<0){break;}
      if(entry<2){
	malta_time0=timer;
      }
      entry++;
      malta_time=timer-malta_time0;
      delta_malta_time=malta_time-malta_last_time;
      //cout << " DiamondTime: " << diamond_time 
      //   << " malta_time: " << malta_time << endl;
      //if(diamond_time-malta_time>15){break;}
      if(l1idC!=(step+1)){
	malta_last_time=malta_time;
	cout << "Next L1ID" << endl;
	break;
      }
      md.setWord1(word1);
      md.setWord2(word2);
      md.unpack();
      for (uint32_t i=0; i<md.getNhits(); i++){
	hits[md.getHitColumn(i)][md.getHitRow(i)]+=1;
      }
    }

    gTimeCorr->SetPoint(gTimeCorr->GetN(),malta_time,diamond_time);
    outTree->Fill();		
  }
  */
  
  cout << "Loop over the data" << endl;
  MiniMaltaData md;
  uint32_t entry=0;
  uint32_t malta_time0=0;
  uint32_t mem_l1idC=1;
  uint32_t mem_found_step=0;
  bool first_hit=true;
  vector<bool> used_step;
  for(uint32_t step=0; step<ds.GetNSteps(); step++){
    used_step.push_back(false);
  }
  while(true){
    inTree->GetEntry(entry);
    if(inTree->LoadTree(entry)<0){break;}
    
    entry++;
    
    if(cEvents.flags()&CmdArg::GIVEN && l1idC>=(uint32_t)cEvents){
      cout << "Reached number of events requested..." << endl;
      break;
    }
    
    if(l1idC!=mem_l1idC){
      
      //option 2
      if(first_hit){
        malta_time0=timer;
        first_hit = false;
      }
      
      //FK
      malta_time=timer-malta_time0;
      
      if(cVerbose){cout << "Find the closest diamond step" << endl;}

      uint32_t found_step;
      found_step=mem_found_step+1;
      diamond_time=(ds.GetT(found_step)-ds.GetT(0));
      if(abs(diamond_time-malta_time)>1.0){
        found_step = findStep(ds, used_step,
                              diamond_time,
                              malta_time, mem_l1idC,
                              cVerbose);
      }
      //cout<<"found step" << found_step<<endl;
      if(found_step!=0xFFFFFFFF){
        x = ds.GetX(found_step);
        y = ds.GetY(found_step);
        ail = ds.GetAil(found_step);
        diamond_time=ds.GetT(found_step)-ds.GetT(0);
        cout << "Step: " << setw(4) << (found_step+1) << "/" << ds.GetNSteps() << endl;	
        //fill in the tree
        outTree->Fill();
        //Time
        gTimeCorr->SetPoint(gTimeCorr->GetN(),malta_time,diamond_time);
        mem_found_step=found_step;
	used_step.at(found_step)=true;   
      	cout<< "the step is used: "<<used_step.at(found_step)<<endl;   
      }else{
        cout << "Could not find a step for this l1id: " << mem_l1idC << endl;
      }
      
      //first_hit = true;
      
      //keep the l1id in memory
      mem_l1idC=l1idC;
      
      //clear the hit contents
      for (int i=0; i<16; i++){ 
        for(int j=0; j<64;j++){
          hits[i][j] = 0;
        }
      }	
      
    }
    
    
    md.setWord1(word1);
    md.setWord2(word2);
    md.unpack();
    for (uint32_t i=0; i<md.getNhits(); i++){
      hits[md.getHitColumn(i)][md.getHitRow(i)]+=1;
    }

  }
  
  uint32_t found_step = findStep(ds, used_step,
				 diamond_time,
				 malta_time, mem_l1idC,
				 cVerbose);
  if(found_step!=0xFFFFFFFF){
    x = ds.GetX(found_step);
    y = ds.GetY(found_step);
    ail = ds.GetAil(found_step);
    diamond_time=ds.GetT(found_step)-ds.GetT(0);
    cout << "Step: " << setw(4) << (found_step+1) << "/" << ds.GetNSteps() << endl;	
    //fill in the tree
    outTree->Fill();
    //Time
    gTimeCorr->SetPoint(gTimeCorr->GetN(),malta_time,diamond_time);
  }else{
    cout << "Could not find a step for this l1id: " << mem_l1idC << endl;
  }
  
  //Close output tree
  cout << "Close output file: " << outputfile << endl;
  gTimeCorr->Write();
  outFile->Write();
  outFile->Close();
  
  cout << "Have a nice day" << endl;
  return 0;
}




