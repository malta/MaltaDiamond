# MaltaDiamond {#PackageMaltaDiamond}

MaltaDiamond contains the software to analyse MiniMalta at the Diamond Lighsource, RAL, UK

## Applications

- DiamondDQ: Combine the MiniMaltaTree files and the Diamdon linear stage files
- DiamondAna: Another version of DiamondDQ
- DiamondReco: Another version of DiamondDQ

## Libraries

- MaltaDiamond: Helper tools for data taking at Diamond

### MaltaDiamond library

- DiamondStage: Library to read the Diamond Lightsource linear stage output files
