#!/usr/bin/env python

import os
import sys

class DiamondStage:
    def __init__(self):
        self.steps=[]
        self.xmin=0
        self.xmax=0
        self.xstep=0
        self.xbins=0
        self.ymin=0
        self.ymax=0
        self.ystep=0
        self.ybins=0        
        pass
    def Open(self,path):
        print "Open: %s" % path
        fr = open(path,"r")
        iline=-1
        for line in fr.readlines():
            iline+=1
            if iline==6:
                cmd=line.strip()
                p0=cmd.find("'")
                p1=cmd.find("'",p0+1)
                cmd=cmd[p0+1:p1-1]
                chk=cmd.split()
                print "Parse header: %s" % cmd
                self.xmin=float(chk[2])
                self.xmax=float(chk[3])
                if self.xmin>self.xmax:
                    tmp=self.xmin
                    self.xmin=self.xmax
                    self.xmax=tmp
                    pass
                self.xstep=float(chk[4])
                self.xbins=int((self.xmax-self.xmin)/self.xstep)
                self.ymin=float(chk[6])
                self.ymax=float(chk[7])
                if self.ymin>self.ymax:
                    tmp=self.ymin
                    self.ymin=self.ymax
                    self.ymax=tmp
                    pass
                self.ystep=float(chk[8])
                self.ybins=abs(int((self.ymax-self.ymin)/self.ystep))
                print "xmin: %.2f" % self.xmin
                print "xmax: %.2f" % self.xmax
                print "xstep: %.2f" % self.xstep
                print "xbins: %i" % self.xbins
                print "ymin: %.2f" % self.ymin
                print "ymax: %.2f" % self.ymax
                print "ystep: %.2f" % self.ystep
                print "ybins: %i" % self.ybins
                pass
            if iline<10: continue
            chk=line.strip().split("\t")
	    entry={}
            entry["x"]=float(chk[0])
            entry["y"]=float(chk[1])
            entry["a"]=float(chk[4])
            entry["r"]=float(chk[5])
	    self.steps.append(entry)
            pass
        fr.close()
        pass

    
    def GetNSteps(self):
        return len(self.steps)
    
    def GetStep(self, i):
        i=int(i)
        if i>=len(self.steps): return []
        return self.steps[i]

    def GetX(self,i):
        i=int(i)
        if i>=len(self.steps): return 0
        return self.steps[i]['x']

    def GetY(self,i):
        i=int(i)
        if i>=len(self.steps): return 0
        return self.steps[i]['y']

    def GetXmin(self):
        return self.xmin

    def GetXmax(self):
        return self.xmax
    
    def GetXstep(self):
        return self.xstep
    
    def GetXbins(self):
        return self.xbins

    def GetYmin(self):
        return self.ymin

    def GetYmax(self):
        return self.ymax
    
    def GetYstep(self):
        return self.ystep
    
    def GetYbins(self):
        return self.ybins
    
    pass
