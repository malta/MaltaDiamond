#!/usr/bin/env python
###############################
# Plot diamond eff
# 
# patrick.moriishi.freeman@cern.ch
# July 2019
###############################
import os
import sys
import ROOT
import time
import math
import signal
import argparse
import Tuple
import AtlasStyle
import numpy as np
import configparser

#script to plot cluster sizes 



#dictionary of runs 

from runDictionary import runDict 

def sectorNumber(sectName):
  sectNum = 8
  if sectName == 'malta':
    sectNum = 0 
  elif sectName == 'ngap' or sectName == 'n gap':
    sectNum = 1 
  elif sectName == 'pwell':
    sectNum = 2
  if sectNum == 8:
    print 'the sector seems to be named incorrectly'  
  return sectNum 

parser = argparse.ArgumentParser()
parser.add_argument('-i','--inputPath',type=str,help="input path",default = "/eos/user/a/adecmos/TB_Diamond_April2019/processed/")
parser.add_argument('-s','--suffix',type=str,help="output file suffix",default = "")
parser.add_argument('-o','--output',type=str,help="output file",default="/eos/user/a/adecmos/TB_Diamond_April2019/processed/")
args=parser.parse_args()

#tgraphs for each configuration

W1R9_8V_eff=ROOT.TGraphErrors()
W1R9_15V_eff=ROOT.TGraphErrors()
W2R11_6V_eff=ROOT.TGraphErrors()
W2R1_15V_eff=ROOT.TGraphErrors()
W2R1_6V_eff=ROOT.TGraphErrors()
W2R1_20V_eff=ROOT.TGraphErrors()
W2R9_15V_eff=ROOT.TGraphErrors()
W4R9_6V_eff=ROOT.TGraphErrors()
W5R9_6V_eff=ROOT.TGraphErrors()

#etc.

#W1R9widthX8V=ROOT.TGraphErrors()

#widthYW1R98V=ROOT.TGraphErrors()

#read and plot data

print ("Output file:"+args.output+'EffTable_'+args.suffix+'.txt')
dEff=open(args.output+'EffTable_'+args.suffix+'.txt',"w+")
dEff.write("MiniMALTA Diamond Test Beam analysis\n")
dEff.write("Sensor \t Votlage (V) \t Sector \t Efficiency \t Std Dev \t SD of Mean \n" )

for i in range (len(runDict)):
  run = runDict[i]['run'] 
  #read the last line of the data file
  #read text file
  fileName = args.inputPath+'run_00'+str(run)+'/clusters/DQ_00'+str(run)+'.txt'
  print 'reading file '+fileName
  fileHandle = open(fileName, "r")
  lineList = fileHandle.readlines()
  fileHandle.close()
  stats = lineList[1].split('=')
  sect = sectorNumber(runDict[i]['sector'])
  #write data to file
  dEff.write("%s \t %f \t %s \t %f \t %f \t %f \n  " % ( runDict[i]['sensor'], runDict[i]['voltage'], runDict[i]['sector'], float(0), float(0), float( 0 ))   )
  #add data to plots	
  if runDict[i]['sensor'] == 'w1r9' and runDict[i]['voltage'] == 8:
    W1R9_8V_eff.SetPoint(sect,sect, float(stats[1]) )
    W1R9_8V_eff.SetPointError(sect,0, float(0) )
   # W1R9widthX8V.SetPoint(sect, stats[],0,0) 
   # W1R9widthY8V.SetPoint(sect, stats[],0,0) 
  elif runDict[i]['sensor'] == 'w1r9' and runDict[i]['voltage'] == 1.5:
    W1R9_15V_eff.SetPoint(sect,sect, float(stats[1]) )
    W1R9_15V_eff.SetPointError(sect,0, float(0) )
  elif runDict[i]['sensor'] == 'w2r11' and runDict[i]['voltage'] == 6:
    W2R11_6V_eff.SetPoint(sect,sect, float(stats[1]) )
    W2R11_6V_eff.SetPointError(sect,0, float(0) )
  elif runDict[i]['sensor'] == 'w2r1' and runDict[i]['voltage'] == 1.5:
    W2R1_15V_eff.SetPoint(sect-1,sect, float(stats[1]) )
    W2R1_15V_eff.SetPointError(sect-1,0, float(0) )
  elif runDict[i]['sensor'] == 'w2r1' and runDict[i]['voltage'] == 6:
    W2R1_6V_eff.SetPoint(sect,sect, float(stats[1]) )
    W2R1_6V_eff.SetPointError(sect,0, float(0) )
  elif runDict[i]['sensor'] == 'w2r1' and runDict[i]['voltage'] == 20:
    W2R1_20V_eff.SetPoint(sect,sect, float(stats[1]) )
    W2R1_20V_eff.SetPointError(sect,0, float(0) )
  elif runDict[i]['sensor'] == 'w2r9' and runDict[i]['voltage'] == 1.5:
    W2R9_15V_eff.SetPoint(sect,sect, float(stats[1]) )
    W2R9_15V_eff.SetPointError(sect,0, float(0) )
  elif runDict[i]['sensor'] == 'w4r9' and runDict[i]['voltage'] == 6:
    W4R9_6V_eff.SetPoint(sect,sect, float(stats[1]) )
    W4R9_6V_eff.SetPointError(sect,0, float(0) )
  elif runDict[i]['sensor'] == 'w5r9' and runDict[i]['voltage'] == 6:
    W5R9_6V_eff.SetPoint(sect,sect, float(stats[1]) )
    W5R9_6V_eff.SetPointError(sect,0, float(0) )
    #W1R9widthY15V.SetPoint(sect, stats[],0,0) 
# 24x tgrapherrors... so much code ... should i try arrays/dictionaries?



#draw efficiencies
print 'plotting efficiencies'
cEff = ROOT.TCanvas('cEff','cEff', 800, 1000) 

W1R9_8V_eff.SetTitle('Relative Efficiencies in each sector')

ROOT.gStyle.SetEndErrorSize(20)

W1R9_8V_eff.SetLineColorAlpha(7, 05)
W1R9_8V_eff.SetMarkerColor(7)
W1R9_8V_eff.SetMarkerSize(3)
W1R9_8V_eff.SetLineWidth(2)
W1R9_8V_eff.SetLineStyle(1)
W1R9_8V_eff.SetMarkerStyle(41)
W1R9_8V_eff.GetXaxis().SetTitle('Sector')
W1R9_8V_eff.GetYaxis().SetTitle('Mean Cluster Size')
W1R9_8V_eff.GetYaxis().SetRangeUser(1, 1.3)
W1R9_8V_eff.GetXaxis().SetRangeUser(-0.2, 2.2)
W1R9_8V_eff.Draw("")
W1R9_8V_eff.Draw("lp")

W1R9_15V_eff.SetLineColor(38)
W1R9_15V_eff.SetMarkerColor(38)
W1R9_15V_eff.SetLineWidth(2)
W1R9_15V_eff.SetLineStyle(1)
W1R9_15V_eff.SetMarkerStyle(43)
W1R9_15V_eff.SetMarkerSize(3)
W1R9_15V_eff.Draw("lp")

W2R11_6V_eff.SetLineColor(1)
W2R11_6V_eff.SetMarkerColor(1)
W2R11_6V_eff.SetLineWidth(2)
W2R11_6V_eff.SetLineStyle(1)
W2R11_6V_eff.SetMarkerStyle(41)
W2R11_6V_eff.SetMarkerSize(3)
W2R11_6V_eff.Draw("lp")

W2R1_15V_eff.SetLineColor(46)
W2R1_15V_eff.SetMarkerColor(46)
W2R1_15V_eff.SetLineStyle(1)
W2R1_15V_eff.SetMarkerStyle(43)
W2R1_15V_eff.SetMarkerSize(3)
W2R1_15V_eff.SetLineStyle(1)
W2R1_15V_eff.SetLineWidth(2)
W2R1_15V_eff.Draw("lp")

W2R1_6V_eff.SetLineColor(28)
W2R1_6V_eff.SetMarkerColor(28)
W2R1_6V_eff.SetMarkerStyle(41)
W2R1_6V_eff.SetMarkerSize(3)
W2R1_6V_eff.SetLineWidth(2)
W2R1_6V_eff.SetLineStyle(1)
W2R1_6V_eff.Draw("lp")

W2R1_20V_eff.SetLineColor(2)
W2R1_20V_eff.SetMarkerColor(2)
W2R1_20V_eff.SetLineStyle(3)
W2R1_20V_eff.SetLineStyle(1)
W2R1_20V_eff.SetLineWidth(2)
W2R1_20V_eff.SetLineStyle(1)
W2R1_20V_eff.SetMarkerStyle(43)
W2R1_20V_eff.SetMarkerSize(3)
W2R1_20V_eff.Draw("lp")

W2R9_15V_eff.SetLineColor(30)
W2R9_15V_eff.SetMarkerColor(30)
W2R9_15V_eff.SetLineWidth(2)
W2R9_15V_eff.SetLineStyle(1)
W2R9_15V_eff.SetMarkerStyle(41)
W2R9_15V_eff.SetMarkerSize(3)
W2R9_15V_eff.Draw("lp")

W4R9_6V_eff.SetLineColor(3)
W4R9_6V_eff.SetMarkerColor(3)
W4R9_6V_eff.SetLineWidth(2)
W4R9_6V_eff.SetLineStyle(1)
W4R9_6V_eff.SetMarkerStyle(43)
W4R9_6V_eff.SetMarkerSize(3)
W4R9_6V_eff.Draw("lp")

W5R9_6V_eff.SetLineColor(8)
W5R9_6V_eff.SetMarkerColor(8)
W5R9_6V_eff.SetLineWidth(2)
W5R9_6V_eff.SetLineStyle(1)
W5R9_6V_eff.SetMarkerStyle(41)
W5R9_6V_eff.SetMarkerSize(3)
W5R9_6V_eff.Draw("lp")

legend = ROOT.TLegend(1, 0.67 , 2.15, .8, "Legend","p")
legend.AddEntry(W1R9_15V_eff, "W1R9 7.1e14 neq/cm^2 91 Mrad 1.5 V", "p")
legend.AddEntry(W1R9_8V_eff, "W1R9 7.1e14 neq/cm^2 91 Mrad 8 V", "p")
legend.AddEntry(W2R1_15V_eff, "W2R1 1e15 neq/cm^2 1.5 V", "p")
legend.AddEntry(W2R1_6V_eff, "W2R1 1e15 neq/cm^2 6 V", "p")
legend.AddEntry(W2R1_20V_eff, "W2R1 1e15 neq/cm^2 20 V", "p")
legend.AddEntry(W2R11_6V_eff, "W2R11 unirradiated 6 V", "p")
legend.AddEntry(W2R9_15V_eff, "W2R9 5.0e14 neq/cm^2 66 Mrad 1.5 V", "p")
legend.AddEntry(W4R9_6V_eff, "W4R9 7e13 neq/cm^2 9 Mrad 6 V","p")
legend.AddEntry(W5R9_6V_eff, "W5R9 5.6e14 neq/cm^2 74 Mrad 6 V", "p")
legend.Draw("")


cEff.SaveAs(args.output+'Cluster_plot_'+args.suffix+'.pdf')
cEff.SaveAs(args.output+'Cluster_plot_'+args.suffix+'.png')
#cEff.SaveAs(args.output+'Cluster_plot_'+args.suffix+'.C')
#draw widths






 
#write output to file









