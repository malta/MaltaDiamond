#!/usr/bin/env python
###############################
# Plot diamond scan data
# 
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# Kaloyan.Metodiev@cern.ch
# Patrick.Freeman@cern.ch
# Maria.Mironova@cern.ch
# April 2019
###############################
import os
import sys
import ROOT
import time
import math
import signal
import argparse
import Tuple
import AtlasStyle

def pix2bin(x,y):
    return x*64+y

def pix2sector(x,y):
    if(x>=8 and x<=15 and y>= 0 and y<=15): return 0;
    if(x>=8 and x<=15 and y>=16 and y<=31): return 1;
    if(x>=8 and x<=15 and y>=32 and y<=47): return 2;
    if(x>=8 and x<=15 and y>=48 and y<=63): return 3;
    if(x>=0 and x<= 7 and y>= 0 and y<=15): return 4;
    if(x>=0 and x<= 7 and y>=16 and y<=31): return 5;
    if(x>=0 and x<= 7 and y>=32 and y<=47): return 6;
    if(x>=0 and x<= 7 and y>=48 and y<=63): return 7;
    return None

parser=argparse.ArgumentParser()
parser.add_argument('-f','--file',type=str,help="input file",required=True)
parser.add_argument('-o','--output',type=str,help="output file",default="./output.root")
parser.add_argument('-b','--batch',help="enable batch mode",action="store_true")
parser.add_argument('-v','--verbose',help="enable verbose mode",action="store_true")
parser.add_argument('-r','--resolution',help="beam resolution [um]",type=float,default=2)
parser.add_argument('-p','--pixel',help="do single pixel plots",action="store_true")
args=parser.parse_args()

AtlasStyle.SetAtlasStyle()

if args.batch: ROOT.gROOT.SetBatch(True)

fr=args.file
tree=Tuple.Tuple()
chain=ROOT.TChain("DiamondReco")
chain.AddFile(fr)

bres=args.resolution
mymax=math.ceil(36.4*64)
mybin=int(mymax/bres)
mxmax=math.ceil(36.4*16)+1
mxbin=int(mxmax/bres)

print("xbin: %i, xmin: %.3f, xmax: %.3f" % (mxbin,0,mxmax))
print("ybin: %i, ymin: %.3f, ymax: %.3f" % (mybin,0,mymax))
hCluS=ROOT.TProfile2D("hCluS",";Malta X [#mum];Malta Y [#mum]", mxbin, 0, mxmax, mybin, 0, mymax)
hHitM=ROOT.TH2I("hHitM",";Malta X [#mum];Malta Y [#mum]", mxbin, 0, mxmax, mybin, 0, mymax)
hYpos=ROOT.TH1I("hYpos",";Stage Y [mm]",2*500,-19.000-0.001,-17.000+0.001)
hXpos=ROOT.TH1I("hXpos",";Stage X [mm]",2*500,  0.500-0.001,  2.500+0.001)
gDpos=ROOT.TGraph()
gDpos.SetNameTitle("gDpos","Postition Map;Stage X [mm]; Stage Y [mm]")
gDpos.SetMarkerColor(ROOT.kRed)
gDpos.SetMarkerStyle(ROOT.kFullSquare)

gPixels={}
for pixx in xrange(16):
    gPixels[pixx]={}
    for pixy in xrange(64):
        gPixels[pixx][pixy]=ROOT.TGraph2D()
        gPixels[pixx][pixy].SetName("gPixel_%02i_%02i" % (pixx,pixy))
        gPixels[pixx][pixy].SetTitle(";Stage Y [mm]; Stage X [mm]")
        pass
    pass

xoff=  0.4995
yoff=-18.6055
vxpos=[]
vypos=[]
xmin=99999
xmax=-9999
ymin=99999
ymax=-9999
hmin=99999
hmax=-9999
effs=0.
effn=0.
timestart=time.time()
entry=1
while True:
    chain.GetEntry(entry)
    if chain.LoadTree(entry)<0:
        break
    if entry%100==0:
        print "Processed %i events," % entry, " time taken: %.2f sec"%(time.time()-timestart)
        timestart=time.time()
        pass
    entry += 1
    tree.LoadBranches(chain)
        
    xpos=tree.stage_x.GetValue()
    ypos=tree.stage_y.GetValue()

    if not xpos in vxpos: vxpos.append(xpos)
    if not ypos in vypos: vypos.append(ypos)

    hXpos.Fill(xpos)
    hYpos.Fill(ypos)
    gDpos.SetPoint(gDpos.GetN(),xpos,ypos)
    
    for pixx in xrange(16):
        for pixy in xrange(64):
            n=gPixels[pixx][pixy].GetN()
            hits=tree.hits.GetValue(pix2bin(pixx,pixy))
            if(args.pixel): gPixels[pixx][pixy].SetPoint(n,ypos,xpos,hits)
            pass
        pass

    for pixx in xrange(16):
        for pixy in xrange(64):
            hits=int(tree.hits.GetValue(pix2bin(pixx,pixy)))
            if hits==0:continue
            #px=pixx*36.4+18.2
            #py=pixy*36.4+18.2
            nx=(ypos-yoff)*1000
            ny=(xpos-xoff)*1000
            ny=(36.4*64)-ny
            if xmin>nx:xmin=nx
            if xmax<nx:xmax=nx
            if ymin>ny:ymin=ny
            if ymax<ny:ymax=ny
            #print ("x,y = %.3f,%.3f"%(nx,ny))
            minx=hHitM.GetXaxis().GetXmin()
            maxx=hHitM.GetXaxis().GetXmax()
            miny=hHitM.GetYaxis().GetXmin()
            maxy=hHitM.GetYaxis().GetXmax()
            if nx<minx or nx>maxx:
                print("X out of range: %.3f [%.3f,%.3f] ypos: %.3f"%(nx,minx,maxx,ypos))
                pass
            if ny<miny or ny>maxy:
                print("Y out of range: %.3f [%.3f,%.3f] xpos: %.3f"%(ny,miny,maxy,xpos))
                pass
            hHitM.Fill(nx,ny,hits)
            pass
        pass

    for c in xrange(int(tree.cl_n.GetValue())):
        cl_x=tree.cl_x.GetValue(c)
        cl_y=tree.cl_y.GetValue(c)
        cl_s=tree.cl_s.GetValue(c)
        #print("Cluster: : %.3f %.3f %.3f"%(cl_x,cl_y,cl_s))
        hCluS.Fill(cl_x,cl_y,cl_s)
        pass
    pass


if args.verbose:
    for x in vxpos: print ("%0.3f => %6.3f" % (x,((x-xoff)*1000)))
    for y in vypos: print ("%0.3f => %6.3f" % (y,((y-yoff)*1000)))
    
fw=ROOT.TFile.Open(args.output,"RECREATE")
dn=os.path.dirname(args.output)
if dn=="": dn="."
run=0
if "_" in args.file:
    run=int(os.path.basename(args.file).split(".")[-2].split("_")[-1])
    pass

cPos=ROOT.TCanvas("cPos","cPos",800,600)
cPos.Divide(2,1)
cPos.cd(1).Divide(1,2)
cPos.cd(1).cd(1)
hXpos.Draw()
hXpos.Write()
cPos.cd(1).cd(2)
hYpos.Draw()
hYpos.Write()
cPos.cd(2)
gDpos.Draw()
gDpos.Write()
cPos.Update()
cPos.Write()

cHitM=ROOT.TCanvas("cHitM","Hit Map",800,600)
cHitM.cd()
hHitM.GetXaxis().SetRangeUser(xmin-18,xmax+18)
hHitM.GetYaxis().SetRangeUser(ymin-18,ymax+18)
hHitM.Draw("COLZ")
print "hmin: %i, hmax: %i" % (hmin,hmax)
#hHitM.SetMaximum(hmax*3)
hHitM.SetMaximum(hmax*3)
hHitM.SetMinimum(hmax)
hHitM.Write()
cHitM.Update()
cHitM.Write()
cHitM.SaveAs("%s/HitMap.pdf" % dn)
#this if for the automatic plotting
hEff=hHitM.Clone("Eff")
hEff.Write()

if(args.pixel):
    for pixx in xrange(16):
        for pixy in xrange(64):
            gPixels[pixx][pixy].Write()
            pass
        pass
    pass

print("Be nice")

print ("Output DQ file: DQ_%06i.txt"%(run))
dq=open("%s/DQ_%06i.txt"%(dn,run),"w+")
dq.write("Status: %r\n" % True)
dq.write("AvgEff: %.2f%%" % 0 )
dq.close()

if not args.batch: raw_input("Press any key to continue.")
fw.Write()
fw.Close()
print("Have a nice day")
