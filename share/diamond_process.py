#!/usr/bin/env python
##########################
# Process diamond data
#
# Carlos.Solans@cern.ch
# July 2019
##########################
import os
import sys
import argparse

parser=argparse.ArgumentParser("")
parser.add_argument("-r","--run",help="Particular run to analyse")
args=parser.parse_args()

runs=[
	6019,6026,6027,6028,6032,6033,6034,6035,6036,6037,6038,
	6041,6042,6043,6044,6045,6046,6047,6048,6049,6050,6051,
	6052,6053,6054,6055,6056,6057,6058,6060,6061,6062,6063,
	6064,6065]

cmd="export EOS_TB_DIR=/eos/user/a/adecmos/TB_Diamond_April2019;"
cmd+="MaltaDQ_Diamond_v2.py --skip-copy --local-dut-path $EOS_TB_DIR/MALTA --local-ref-path $EOS_TB_DIR/Diamond --local-proc-path $EOS_TB_DIR/processed -r RUN"

for run in runs:
    if args.run:
        if run!=int(args.run): continue
        pass
    c=cmd.replace("RUN",str(run))
    print(c)
    os.system(c)
    pass

	
