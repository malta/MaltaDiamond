#!/usr/bin/env python
import os
import sys
import ROOT
import time
import signal
import argparse
import AtlasStyle
import PyMiniMaltaData

cont=True
def signal_handler(signal, frame):
    print "You pressed ctrl+C to quit" 
    global cont
    cont = False
    return

class Tuple(dict):
    def __init__(self):
        self.treenumber=-1
        pass
    def LoadBranches(self,t):
        if self.treenumber == t.GetTreeNumber(): return
        self.treenumber = t.GetTreeNumber()
        for i in range(t.GetListOfLeaves().GetEntries()):
            print "Load", t.GetListOfLeaves().At(i).GetName()
            self.__setattr__(t.GetListOfLeaves().At(i).GetName(),
                             t.GetListOfLeaves().At(i))
            pass
        pass
    pass

parser=argparse.ArgumentParser()
parser.add_argument('-f','--file',help="input file",required=True)
args=parser.parse_args()

tree=Tuple()
chain=ROOT.TChain("MALTA")
chain.AddFile(args.file)

md=PyMiniMaltaData.MiniMaltaData()

AtlasStyle.SetAtlasStyle()
c1=ROOT.TCanvas("c1","c1",800,600)
h1=ROOT.TH2I("h1",";Column;Row",16,0,16,64,0,64)
h1.SetStats(0)
tt=ROOT.TLatex()

entry=0
l1id=0
intensity=[]

signal.signal(signal.SIGINT, signal_handler)

while cont:
    chain.GetEntry(entry)
    if chain.LoadTree(entry)<0:
        break
    if entry%10000==0:
        print "Processed %i events" % entry
        pass
    entry += 1
    tree.LoadBranches(chain)

    if tree.l1idC.GetValue()!=l1id:
	h1.Draw("COLZ")
        tt.DrawLatexNDC(0.2,0.95,"L1ID: %i"%l1id)
        c1.Update()
        time.sleep(0.01)
        #raw_input("press any key to continue")
	h1.Reset()
        l1id=tree.l1idC.GetValue()
        pass

    if tree.pixel.GetValue()==0: continue
    
    md.setWord1(int(tree.word1.GetValue()))
    md.setWord2(int(tree.word2.GetValue()))
    md.unpack()
    
    for i in xrange(md.getNhits()):
        h1.Fill(md.getHitColumn(i),md.getHitRow(i))
        pass
    pass

