#!/usr/bin/env python
##################################################################
# MaltaDQ_Diamond_v2
# Abhishek Sharma
# Carlos Solans
# patrick.moriishi.freeman@cern.ch
# Apr 2019
##################################################################
# - Copy data files from ps07 to ps02
# - Copy linear stage .dat files from mounted drive to ps02
# - Run analysis script
# - Run plotting script
##################################################################

import os
import sys
import pexpect
import getpass
import argparse
import glob
import base64
import urllib2
import GoogleSpreadsheet

parser = argparse.ArgumentParser()
parser.add_argument("-r",   "--run",help="run number", type=int,required=True)
parser.add_argument("-n",   "--nevents",help="number of events",type=int)
parser.add_argument("-v",   "--verbose",help="enable verbose mode",action="store_true")
parser.add_argument("--skip-copy",help="skip copy from dut to localhost",action="store_true")
parser.add_argument("--skip-archive",help="skip archive from localhost to eos",action="store_true")
parser.add_argument("--skip-publish",help="skip publish from localhost to web",action="store_true")
parser.add_argument("--dut-host",default="192.168.0.107")
parser.add_argument("--dut-path",default="/eos/user/a/adecmos/TB_Diamond_April2019/MALTA/")
parser.add_argument("--ref-host",default="192.168.0.107")
parser.add_argument("--ref-path",default="/mnt/b16/data/2019/mm22061-1/")
parser.add_argument("--local-dut-path",default="/eos/user/a/adecmos/TB_Diamond_April2019/MALTA/")
parser.add_argument("--local-ref-path",default="/eos/user/a/adecmos/TB_Diamond_April2019/Diamond")
parser.add_argument("--local-proc-path",default="/eos/user/a/adecmos/TB_Diamond_April2019/processed")
parser.add_argument("--archive-host",default="adecmos@lxplus.cern.ch")
parser.add_argument("--archive-dut-path",default="/eos/user/a/adecmos/TB_Diamond_April2019/MALTA")
parser.add_argument("--archive-ref-path",default="/eos/user/a/adecmos/TB_Diamond_April2019/Diamond")
parser.add_argument("--archive-proc-path",default="/eos/user/a/adecmos/TB_Diamond_April2019/processed")
parser.add_argument("--publish-host",default="adecmos@lxplus.cern.ch")
parser.add_argument("--publish-path",default="/eos/user/a/adecmos/www/ade-pixel-testbeam/files/TB_Diamond_April2019")
parser.add_argument("--max-proc",help="Max number of events for quick tests", type=int)
args=parser.parse_args()

adecmospassword = base64.b64decode("S2lnZUJlbGUxOQ==")#getpass.getpass("Enter adecmos password:")

#Do we really want to do this blindly?
#Copy Linear Stage & Data files from PS07 to PS02:
'''
if False:
    mntDir="/mnt/b16/data/2019/mm22061-1/"
    diamondDir="/home/sbmuser/MaltaSW/data/Diamond/"
    MMaltaDataSrc="sbmuser@192.168.0.107:/home/sbmuser/MaltaSW/data/"
    MMaltaDataDest="/home/sbmuser/MaltaSW/data/MiniMalta/"
    os.system("scp %s %s"(mntDir,diamondDir))
    os.system("scp %s %s"(mntDir,diamondDir))
    #Copy Linear Stage & Data files from PS02 to EOS:
    #eosDir=
    #os.system("scp %s  %s"(mndDir,diamondDir))
    #os.system("scp %s  %s"(mndDir,diamondDir))

    pass
'''
print ("Load google spreadsheet")
gs=GoogleSpreadsheet.GoogleSpreadsheet()
gs.SetVerbose(args.verbose)
gs.Open("https://docs.google.com/spreadsheets/d/1k7Gm0MHYZHw7TdtcPYdwS4nMSBo1Sp0JOLqSplnGK6E/export?format=csv&id=1k7Gm0MHYZHw7TdtcPYdwS4nMSBo1Sp0JOLqSplnGK6E&gid=1719069121")
#gs.Open2("1k7Gm0MHYZHw7TdtcPYdwS4nMSBo1Sp0JOLqSplnGK6E",8)


malta_run=args.run
diamond_run=0
malta_run_col=gs.GetCol("run number")
diamond_run_col=gs.GetCol("Linear")
print ("Malta run col: %i" % malta_run_col)
print ("Diamond run col: %i" % diamond_run_col)

for row in xrange(gs.GetNumRows()):
    if args.verbose: print (gs.GetCell(row,malta_run_col))
    if str(args.run) in gs.GetCell(row,malta_run_col):
        diamond_run = gs.GetCell(row,diamond_run_col)
        break
    pass

print ("MALTA run: %s Diamond run: %s" % (malta_run,diamond_run))
'''
if not args.skip_copy:
    print ("Copy file out from %s:%s to localhost:%s" % (args.dut_host,
                                                         args.dut_path,
                                                         args.local_dut_path))
    cmd='rsync -r --progress %s:%s/*%s* %s/.' % (args.dut_host,
                                                 args.dut_path,
                                                 malta_run,
                                                 args.local_dut_path)
    print (cmd)
    os.system(cmd)

    print ("Copy file out from %s:%s to localhost:%s" % (args.ref_host,
                                                         args.ref_path,
                                                         args.local_ref_path))
    cmd='rsync -r --progress %s:%s/*%s* %s/.' % (args.ref_host,
                                                 args.ref_path,
                                                 diamond_run,
                                                 args.local_ref_path)
    print (cmd)
    os.system(cmd)
    pass
'''
print ("Fetch the absolute path to the files")
print ("")
find_malta=glob.glob("%s/*%s*.root"%(args.local_dut_path,malta_run))
find_diamond=glob.glob("%s/*%s*"%(args.local_ref_path,diamond_run))
if len(find_malta)==0:
    print ("MALTA file not found in: %s"%args.local_dut_path)
    sys.exit(0)
    pass
if len(find_diamond)==0:
    print ("Diamond file not found in: %s"%args.local_ref_path)
    sys.exit(0)
    pass
malta_file=find_malta[0]
diamond_file=find_diamond[0]
print ("Malta: %s"%malta_file)
print ("Diamond: %s"%diamond_file)
print ("")

print ("Make proc path")
print ("")
proc_path="%s/run_%06i" % (args.local_proc_path,int(malta_run))
conf_path="%s/config_%04i.ini" % (proc_path,int(malta_run))
cmd="mkdir -p %s" % (proc_path)
print (cmd)
os.system(cmd)
print ("")

print ("Run DiamondDQ")
print ("")
cmd="DiamondDQ2 -r %s -m %s -d %s -o %s" % (malta_run, malta_file,diamond_file,proc_path)
if args.max_proc: cmd+=" -e %i" % args.max_proc
if args.verbose: cmd+=" -v"
print (cmd)
os.system(cmd)
print ("")

print ("Run diamond_plot_scan_v2")
print ("")
if malta_run == 6054 or malta_run==6061:
  res = 1;
else:
  res = 2;
pass
cmd="diamond_plot_scan_v2_test.py -b -n -f %s/ana_%06i.root -o %s/plots_%06i.root -r %i -c %s" % (proc_path,int(malta_run),proc_path,int(malta_run), int(res), conf_path)
if args.verbose: cmd+=" -v"
print (cmd)
os.system(cmd)
print ("")

print ("Publish the results to the web")
if not args.skip_publish:
    print ("Publish proc folder from localhost to %s:%s" % (args.publish_host,
                                                            args.publish_path))
    
    cmd='rsync -r --progress %s %s:%s/.' % (proc_path,
                                            args.publish_host,
                                            args.publish_path)
    print (cmd)
    child = pexpect.spawn(cmd)
    i=child.expect(["Password:","sending"])
    if i==0: child.sendline(adecmospassword)
    child.logfile = sys.stdout
    child.expect(pexpect.EOF, timeout=300)
    child.interact()

'''
print ("Archive the results")
if not args.skip_archive:
    print ("Archive malta data from localhost to %s:%s" % (args.archive_host,
                                                     args.archive_dut_path))
    
    cmd='rsync -r --progress %s %s:%s/.' % (malta_file,
                                               args.archive_host,
                                               args.archive_dut_path)
    print (cmd)
    child = pexpect.spawn(cmd)
    i=child.expect(["Password:","sending"])
    if i==0: child.sendline(adecmospassword)
    child.logfile = sys.stdout
    child.expect(pexpect.EOF, timeout=300)
    child.interact()

    print ("Archive diamond data from localhost to %s:%s" % (args.archive_host,
                                                             args.archive_ref_path))
    
    cmd='rsync -r --progress %s %s:%s/.' % (diamond_file,
                                               args.archive_host,
                                               args.archive_ref_path)
    print (cmd)
    child = pexpect.spawn(cmd)
    i=child.expect(["Password:","sending"])
    if i==0: child.sendline(adecmospassword)
    child.logfile = sys.stdout
    child.expect(pexpect.EOF, timeout=300)
    child.interact()

    print ("Archive proc folder from localhost to %s:%s" % (args.archive_host,
                                                            args.archive_proc_path))
    
    cmd='rsync -r --progress %s %s:%s/.' % (proc_path,
                                            args.archive_host,
                                            args.archive_proc_path)
    print (cmd)
    child = pexpect.spawn(cmd)
    i=child.expect(["Password:","sending"])
    if i==0: child.sendline(adecmospassword)
    child.logfile = sys.stdout
    child.expect(pexpect.EOF, timeout=300)
    child.interact()

    pass

'''
