#!/usr/bin/env python
###############################
# Plot diamond width y
# 
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# Kaloyan.Metodiev@cern.ch
# Patrick.Freeman@cern.ch
# Maria.Mironova@cern.ch
# July 2019
###############################
import os
import sys
import ROOT
import time
import math
import signal
import argparse
import Tuple
import AtlasStyle
import numpy as np
import configparser

#script to plot y extents as a a function of sector



#dictionary of runs to plot y extents for

from runDictionary import runDict 

def sectorNumber(sectName):
  sectNum = 8
  if sectName == 'malta':
    sectNum = 0 
  elif sectName == 'ngap' or sectName == 'n gap':
    sectNum = 1 
  elif sectName == 'pwell':
    sectNum = 2
  if sectNum == 8:
    print 'the sector seems to be named incorrectly'  
  return sectNum 

parser = argparse.ArgumentParser()
parser.add_argument('-i','--inputPath',type=str,help="input path",default = "/eos/user/a/adecmos/TB_Diamond_April2019/processed/")
parser.add_argument('-s','--suffix',type=str,help="output file suffix",default = "")
parser.add_argument('-o','--output',type=str,help="output file",default="/eos/user/a/adecmos/TB_Diamond_April2019/processed/")
args=parser.parse_args()

#tgraphs for each configuration

W1R9_8V_wY=ROOT.TGraphErrors()
W1R9_15V_wY=ROOT.TGraphErrors()
W2R11_6V_wY=ROOT.TGraphErrors()
W2R1_15V_wY=ROOT.TGraphErrors()
W2R1_6V_wY=ROOT.TGraphErrors()
W2R1_20V_wY=ROOT.TGraphErrors()
W2R9_15V_wY=ROOT.TGraphErrors()
W4R9_6V_wY=ROOT.TGraphErrors()
W5R9_6V_wY=ROOT.TGraphErrors()

#etc.

#W1R9widthy8V=ROOT.TGraphErrors()

#widthYW1R98V=ROOT.TGraphErrors()

#read and plot data

print ("Output file:"+args.output+'extentYTable_'+args.suffix+'.txt')

for i in range (len(runDict)):
  run = runDict[i]['run'] 
  #read the last line of the data file
  #read text file
  fileName = args.inputPath+'run_00'+str(run)+'/DQ_00'+str(run)+'.txt'
  print 'reading file '+fileName
  fileHandle = open(fileName, "r")
  lineList = fileHandle.readlines()
  fileHandle.close()
  stats = lineList[len(lineList)-1].split('\t')
  sect = sectorNumber(runDict[i]['sector'])
  #write data to file
  #add data to plots	
  if runDict[i]['sensor'] == 'w1r9' and runDict[i]['voltage'] == 8:
    W1R9_8V_wY.SetPoint(sect,sect, float(stats[6]) )
    W1R9_8V_wY.SetPointError(sect,0, float(stats[3])*float(stats[6]) )
   # W1R9widthY8V.SetPoint(sect, stats[],0,0) 
   # W1R9widthY8V.SetPoint(sect, stats[],0,0) 
  elif runDict[i]['sensor'] == 'w1r9' and runDict[i]['voltage'] == 1.5:
    W1R9_15V_wY.SetPoint(sect,sect, float(stats[6]) )
    W1R9_15V_wY.SetPointError(sect,0, float(stats[3])*float(stats[6]) )
  elif runDict[i]['sensor'] == 'w2r11' and runDict[i]['voltage'] == 6:
    W2R11_6V_wY.SetPoint(sect,sect, float(stats[6]) )
    W2R11_6V_wY.SetPointError(sect,0, float(stats[3])*float(stats[6]) )
  elif runDict[i]['sensor'] == 'w2r1' and runDict[i]['voltage'] == 1.5:
    W2R1_15V_wY.SetPoint(sect-1,sect, float(stats[6]) )
    W2R1_15V_wY.SetPointError(sect-1,0, float(stats[3])*float(stats[6]) )
  elif runDict[i]['sensor'] == 'w2r1' and runDict[i]['voltage'] == 6:
    W2R1_6V_wY.SetPoint(sect,sect, float(stats[6]) )
    W2R1_6V_wY.SetPointError(sect,0, float(stats[3])*float(stats[6]) )
  elif runDict[i]['sensor'] == 'w2r1' and runDict[i]['voltage'] == 20:
    W2R1_20V_wY.SetPoint(sect,sect, float(stats[6]) )
    W2R1_20V_wY.SetPointError(sect,0, float(stats[3])*float(stats[6]) )
  elif runDict[i]['sensor'] == 'w2r9' and runDict[i]['voltage'] == 1.5:
    W2R9_15V_wY.SetPoint(sect,sect, float(stats[6]) )
    W2R9_15V_wY.SetPointError(sect,0, float(stats[3])*float(stats[6]) )
  elif runDict[i]['sensor'] == 'w4r9' and runDict[i]['voltage'] == 6:
    W4R9_6V_wY.SetPoint(sect,sect, float(stats[6]) )
    W4R9_6V_wY.SetPointError(sect,0, float(stats[3])*float(stats[6]) )
  elif runDict[i]['sensor'] == 'w5r9' and runDict[i]['voltage'] == 6:
    W5R9_6V_wY.SetPoint(sect,sect, float(stats[6]) )
    W5R9_6V_wY.SetPointError(sect,0, float(stats[3])*float(stats[6]) )
    #W1R9widthY15V.SetPoint(sect, stats[],0,0) 
# 24x tgrapherrors... so much code ... should i try arrays/dictionaries?



#draw widths
print 'plotting widths'
cwY = ROOT.TCanvas('cwY','cwY', 800, 1000) 

W1R9_8V_wY.SetTitle('Number of hits in each sector')

ROOT.gStyle.SetEndErrorSize(20)

W1R9_8V_wY.SetLineColorAlpha(7, 05)
W1R9_8V_wY.SetMarkerColor(7)
W1R9_8V_wY.SetMarkerSize(3)
W1R9_8V_wY.SetLineWidth(2)
W1R9_8V_wY.SetLineStyle(1)
W1R9_8V_wY.SetMarkerStyle(41)
W1R9_8V_wY.GetXaxis().SetTitle('Sector')
W1R9_8V_wY.GetYaxis().SetTitle('Average hits in central region')
W1R9_8V_wY.GetYaxis().SetRangeUser(0, 1.3e4)
W1R9_8V_wY.GetXaxis().SetRangeUser(-0.2, 2.2)
W1R9_8V_wY.Draw("")
W1R9_8V_wY.Draw("lp")

W1R9_15V_wY.SetLineColor(38)
W1R9_15V_wY.SetMarkerColor(38)
W1R9_15V_wY.SetLineWidth(2)
W1R9_15V_wY.SetLineStyle(1)
W1R9_15V_wY.SetMarkerStyle(43)
W1R9_15V_wY.SetMarkerSize(3)
W1R9_15V_wY.Draw("lp")

W2R11_6V_wY.SetLineColor(1)
W2R11_6V_wY.SetMarkerColor(1)
W2R11_6V_wY.SetLineWidth(2)
W2R11_6V_wY.SetLineStyle(1)
W2R11_6V_wY.SetMarkerStyle(41)
W2R11_6V_wY.SetMarkerSize(3)
W2R11_6V_wY.Draw("lp")

W2R1_15V_wY.SetLineColor(46)
W2R1_15V_wY.SetMarkerColor(46)
W2R1_15V_wY.SetLineStyle(1)
W2R1_15V_wY.SetMarkerStyle(43)
W2R1_15V_wY.SetMarkerSize(3)
W2R1_15V_wY.SetLineStyle(1)
W2R1_15V_wY.SetLineWidth(2)
W2R1_15V_wY.Draw("lp")

W2R1_6V_wY.SetLineColor(28)
W2R1_6V_wY.SetMarkerColor(28)
W2R1_6V_wY.SetMarkerStyle(41)
W2R1_6V_wY.SetMarkerSize(3)
W2R1_6V_wY.SetLineWidth(2)
W2R1_6V_wY.SetLineStyle(1)
W2R1_6V_wY.Draw("lp")

W2R1_20V_wY.SetLineColor(2)
W2R1_20V_wY.SetMarkerColor(2)
W2R1_20V_wY.SetLineStyle(3)
W2R1_20V_wY.SetLineStyle(1)
W2R1_20V_wY.SetLineWidth(2)
W2R1_20V_wY.SetLineStyle(1)
W2R1_20V_wY.SetMarkerStyle(43)
W2R1_20V_wY.SetMarkerSize(3)
W2R1_20V_wY.Draw("lp")

W2R9_15V_wY.SetLineColor(30)
W2R9_15V_wY.SetMarkerColor(30)
W2R9_15V_wY.SetLineWidth(2)
W2R9_15V_wY.SetLineStyle(1)
W2R9_15V_wY.SetMarkerStyle(41)
W2R9_15V_wY.SetMarkerSize(3)
W2R9_15V_wY.Draw("lp")

W4R9_6V_wY.SetLineColor(3)
W4R9_6V_wY.SetMarkerColor(3)
W4R9_6V_wY.SetLineWidth(2)
W4R9_6V_wY.SetLineStyle(1)
W4R9_6V_wY.SetMarkerStyle(43)
W4R9_6V_wY.SetMarkerSize(3)
W4R9_6V_wY.Draw("lp")

W5R9_6V_wY.SetLineColor(8)
W5R9_6V_wY.SetMarkerColor(8)
W5R9_6V_wY.SetLineWidth(2)
W5R9_6V_wY.SetLineStyle(1)
W5R9_6V_wY.SetMarkerStyle(41)
W5R9_6V_wY.SetMarkerSize(3)
W5R9_6V_wY.Draw("lp")

legend = ROOT.TLegend(1, 2.5e3 , 2.15, 5e3, "Legend","p")
legend.AddEntry(W1R9_15V_wY, "W1R9 7.1e14 neq/cm^2 91 Mrad 1.5 V", "p")
legend.AddEntry(W1R9_8V_wY, "W1R9 7.1e14 neq/cm^2 91 Mrad 8 V", "p")
legend.AddEntry(W2R1_15V_wY, "W2R1 1e15 neq/cm^2 1.5 V", "p")
legend.AddEntry(W2R1_6V_wY, "W2R1 1e15 neq/cm^2 6 V", "p")
legend.AddEntry(W2R1_20V_wY, "W2R1 1e15 neq/cm^2 20 V", "p")
legend.AddEntry(W2R11_6V_wY, "W2R11 unirradiated 6 V", "p")
legend.AddEntry(W2R9_15V_wY, "W2R9 5.0e14 neq/cm^2 66 Mrad 1.5 V", "p")
legend.AddEntry(W4R9_6V_wY, "W4R9 7e13 neq/cm^2 9 Mrad 6 V","p")
legend.AddEntry(W5R9_6V_wY, "W5R9 5.6e14 neq/cm^2 74 Mrad 6 V", "p")
legend.Draw("")


cwY.SaveAs(args.output+'N_plot_'+args.suffix+'.pdf')
cwY.SaveAs(args.output+'N_plot_'+args.suffix+'.png')
cwY.SaveAs(args.output+'N_plot_'+args.suffix+'.C')
#draw widths






 
#write output to file









