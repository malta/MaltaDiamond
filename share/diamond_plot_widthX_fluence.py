#!/usr/bin/env python
###############################
# Plot diamond eff
# 
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# Kaloyan.Metodiev@cern.ch
# Patrick.Freeman@cern.ch
# Maria.Mironova@cern.ch
# July 2019
###############################
import os
import sys
import ROOT
import time
import math
import signal
import argparse
import Tuple
import AtlasStyle
import numpy as np
import configparser

#script to plot efficiencies and widths as a a function of sector



#dictionary of runs to plot efficiencies for

from runDictionary import runDict 

def sectorNumber(sectName):
  sectNum = 8
  if sectName == 'malta':
    sectNum = 0 
  elif sectName == 'ngap' or sectName == 'n gap':
    sectNum = 1 
  elif sectName == 'pwell':
    sectNum = 2
  if sectNum == 8:
    print 'the sector seems to be named incorrectly'  
  return sectNum 

parser = argparse.ArgumentParser()
parser.add_argument('-i','--inputPath',type=str,help="input path",default = "/eos/user/a/adecmos/TB_Diamond_April2019/processed/")
parser.add_argument('-s','--suffix',type=str,help="output file suffix",default = "")
parser.add_argument('-o','--output',type=str,help="output file",default="/eos/user/a/adecmos/TB_Diamond_April2019/processed/")
args=parser.parse_args()

#tgraphs for each configuration

MALTA_eff=ROOT.TGraphErrors()
pwell_eff=ROOT.TGraphErrors()
ngap_eff=ROOT.TGraphErrors()

#etc.
print ("Output file:"+args.output+'EffTable_'+args.suffix+'.txt')

#read and plot data
mCount = 0
pCount = 0 
nCount = 0 

for i in range (len(runDict)):
  run = runDict[i]['run'] 
  #read the last line of the data file
  #read text file
  fileName = args.inputPath+'run_00'+str(run)+'/DQ_00'+str(run)+'.txt'
  print 'reading file '+fileName
  fileHandle = open(fileName, "r")
  lineList = fileHandle.readlines()
  fileHandle.close()
  stats = lineList[len(lineList)-1].split('\t')
  sect = sectorNumber(runDict[i]['sector'])
  #write data to file
  #add data to plots

  fluence = runDict[i]['fluence']	
  if runDict[i]['sector'] == 'malta':
    MALTA_eff.SetPoint(mCount, fluence, float(stats[4]) )
    MALTA_eff.SetPointError(mCount,fluence*.2, float(0) )
    print runDict[i]['sector'],stats[1],'the fluence is',fluence 
    mCount+=1
  if runDict[i]['sector'] == 'ngap':
    ngap_eff.SetPoint(nCount, fluence, float(stats[4]) )
    ngap_eff.SetPointError(nCount,fluence*.2, float(0) )
    print runDict[i]['sector'], stats[1],fluence
    nCount+=1
  if runDict[i]['sector'] == 'pwell':
    pwell_eff.SetPoint(pCount, fluence, float(stats[4]) )
    pwell_eff.SetPointError(pCount,fluence*.2, float(0) )
    pCount+=1
  pass
pass



#draw efficiencies
print 'plotting efficiencies'
cEff = ROOT.TCanvas('cEff','cEff', 800, 1000) 

#MALTA_eff.SetTitle('Relative Efficiencies in each sector')

ROOT.gStyle.SetEndErrorSize(20)

MALTA_eff.SetLineColorAlpha(7, 05)
MALTA_eff.SetMarkerColor(7)
MALTA_eff.SetMarkerSize(3)
MALTA_eff.SetLineWidth(2)
MALTA_eff.SetLineStyle(0)
MALTA_eff.SetMarkerStyle(41)
MALTA_eff.GetXaxis().SetTitle('Fluence')
MALTA_eff.GetYaxis().SetTitle('x Extent (um)')
MALTA_eff.GetYaxis().SetRangeUser(0, 8)
#MALTA_eff.GetXaxis().SetRangeUser(-0.2, 2.2)
MALTA_eff.Draw("")
MALTA_eff.Draw("p")


pwell_eff.SetLineColorAlpha(2, 05)
pwell_eff.SetMarkerColor(2)
pwell_eff.SetMarkerSize(3)
pwell_eff.SetLineWidth(2)
pwell_eff.SetLineStyle(0)
pwell_eff.SetMarkerStyle(43)
pwell_eff.Draw("p")

ngap_eff.SetLineColorAlpha(4, 05)
ngap_eff.SetMarkerColor(4)
ngap_eff.SetMarkerSize(3)
ngap_eff.SetLineWidth(2)
ngap_eff.SetLineStyle(0)
ngap_eff.SetMarkerStyle(41)
ngap_eff.Draw("p")


legend = ROOT.TLegend(2e14, 0.5 , 6e14, 1.5, "Legend","p")
legend.AddEntry(MALTA_eff, "MALTA sector", "p")
legend.AddEntry(ngap_eff, "n-gap sector", "p")
legend.AddEntry(pwell_eff, "p-well sector", "p")
legend.Draw("")


cEff.SaveAs(args.output+'xExtent_v_fluence'+args.suffix+'.pdf')
cEff.SaveAs(args.output+'xExtent_v_fluence'+args.suffix+'.png')
cEff.SaveAs(args.output+'xExtent_v_fluence'+args.suffix+'.C')

#draw widths






 
#write output to file









