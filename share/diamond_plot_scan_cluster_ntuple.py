#!/usr/bin/env python
###############################
# Plot diamond scan data of ntuple clusters
# 
# Carlos.Solans@cern.ch
# Patrick.Freeman@cern.ch
# Abhishek.Sharma@cern.ch
# August 2019
###############################
import os
import sys
import ROOT
import time
import math
import signal
import argparse
import Tuple
import AtlasStyle
import numpy as np
import configparser

def pix2bin(x,y):
    return x*64+y

def cSharingNperc(TH1Eff, nperc, pedestal):
  #function to get intetrpolated location of points which the relative efficiency falls below nperc percent of the maximum. Intended for use on TH1D projections of 2D histograms of pixel hitmatps

  xMax = TH1Eff.GetMaximumBin()
  valMax = TH1Eff.GetMaximum() - pedestal
  print "the Maximum value is ",valMax
  xLim = xMax
  xHi = xMax
  x = xMax
  while True:
    x = x -1 
    if TH1Eff.GetBinContent(x) - pedestal <= valMax * nperc/100.0:
      dy = TH1Eff.GetBinContent(x+1) - TH1Eff.GetBinContent(x)
      dx = TH1Eff.GetBinCenter(x+1) - TH1Eff.GetBinCenter(x)
      dyInt = valMax * nperc/100.0  - (TH1Eff.GetBinContent(x) - pedestal)
      xLo = TH1Eff.GetBinCenter(x) + dyInt * (dx/dy)
      break
  
  x = xMax
  while True: 
    x = x + 1 
    if TH1Eff.GetBinContent(x) - pedestal <= valMax * nperc/100.0:
      dy = TH1Eff.GetBinContent(x) - TH1Eff.GetBinContent(x-1)
      dx = TH1Eff.GetBinCenter(x) - TH1Eff.GetBinCenter(x-1)
      dyInt = valMax * nperc/100.0  -  (TH1Eff.GetBinContent(x) - pedestal)
      xHi = TH1Eff.GetBinCenter(x) + dyInt * (dx/dy)
      break

  return [xLo, xHi]

def interpolateHitMap(hHitM): 
  mxbin  = hHitM.GetNbinsX()
  mybin  = hHitM.GetNbinsY()
  for x in range(mxbin):
    for y in range(mybin):
      neighbors = [hHitM.GetBinContent(x-1,y), hHitM.GetBinContent(x+1,y), hHitM.GetBinContent(x,y-1), hHitM.GetBinContent(x,y+1)]
      intVal = 0
      nNeighbors=0.00000000000000001
      for i in range(4):
        if neighbors[i] > 1:
          nNeighbors+=1
          intVal+=neighbors[i] 
      intVal = intVal/nNeighbors 
      if nNeighbors >= 2 and hHitM.GetBinContent(x,y) == 0:
        hHitM.SetBinContent(x,y,intVal)
      pass
    pass
  pass


def normalizeMap(hHitM, xCenter, yCenter,res, width):
  #returns normalization constant for a 2d histogram
  #constant calculated as mean of square with side lengths of 2*width 
  xCenter = xCenter/res
  yCenter = yCenter/res  
  xlo = int( np.floor(xCenter - width/res))
  xhi = int( np.ceil(xCenter + width/res))
  ylo = int( np.floor(yCenter - width/res))
  yhi = int( np.ceil(yCenter + width/res))
  pixAvg=1e-9
  counts=1e-9
  squares = 0 
  for x in range (xlo,xhi):
    for y in range (ylo, yhi):
      if hHitM.GetBinContent(x,y) > 0:
	scale = 1.0
	if x == xlo:
	  scale = scale * ( xlo + 1 - (xCenter-width/res)  ) 
	if x == xhi - 1:
	  scale = scale * ( xCenter + width/res - (xhi -1) )
	if y == ylo:
	  scale = scale * (ylo + 1 - (yCenter -width/res) )
	if y == yhi - 1:
	  scale = scale * (yCenter +width/res - (yhi - 1) )
        pixAvg += hHitM.GetBinContent(x,y)*scale
        squares += hHitM.GetBinContent(x,y)* hHitM.GetBinContent(x,y) *scale
        counts += scale
    pass
  pass
  pixAvg = pixAvg/counts
  stdDev = np.sqrt(squares/counts - pixAvg*pixAvg)
  stdDevMean = stdDev/np.sqrt(counts -1)
  print "the average number of hits is",pixAvg
  #hHitM.Scale(1/pixAvg)
  return pixAvg, stdDev, stdDevMean

#############################################################################################

runDict = []
runDict.append({'run':6056, 'sensor':'W1R9','voltage':8,'sector':'MALTA'})
runDict.append({'run':6057, 'sensor':'W1R9','voltage':8,'sector':'p-well'})
runDict.append({'run':6058, 'sensor':'W1R9','voltage':8,'sector':'n-gap'})

runDict.append({'run':6035, 'sensor':'W1R9','voltage':1.5,'sector':'MALTA'})
runDict.append({'run':6036, 'sensor':'W1R9','voltage':1.5,'sector':'p-well'})
runDict.append({'run':6037, 'sensor':'W1R9','voltage':1.5,'sector':'n-gap'})

runDict.append({'run':6026, 'sensor':'W2R11','voltage':6,'sector':'p-well'})
runDict.append({'run':6027, 'sensor':'W2R11','voltage':6,'sector':'n-gap'})
runDict.append({'run':6028, 'sensor':'W2R11','voltage':6,'sector':'MALTA'})

runDict.append({'run':6032, 'sensor':'W2R1','voltage':1.5,'sector':'p-well'})
runDict.append({'run':6033, 'sensor':'W2R1','voltage':1.5,'sector':'n-gap'})

runDict.append({'run':6049, 'sensor':'W2R1','voltage':6,'sector':'n-gap'})
runDict.append({'run':6050, 'sensor':'W2R1','voltage':6,'sector':'MALTA'})

runDict.append({'run':6051, 'sensor':'W2R1','voltage':20,'sector':'MALTA'})
runDict.append({'run':6052, 'sensor':'W2R1','voltage':20,'sector':'p-well'})
runDict.append({'run':6055, 'sensor':'W2R1','voltage':20,'sector':'n-gap'})

runDict.append({'run':6038, 'sensor':'W2R9','voltage':1.5,'sector':'MALTA'})
runDict.append({'run':6039, 'sensor':'W2R9','voltage':1.5,'sector':'p-well'})
runDict.append({'run':6040, 'sensor':'W2R9','voltage':1.5,'sector':'n-gap'})

runDict.append({'run':6044, 'sensor':'W4R9','voltage':6,'sector':'MALTA'})
runDict.append({'run':6045, 'sensor':'W4R9','voltage':6,'sector':'p-well'})
runDict.append({'run':6048, 'sensor':'W4R9','voltage':6,'sector':'n-gap'})

runDict.append({'run':6041, 'sensor':'W5R9','voltage':6,'sector':'n gap'})
runDict.append({'run':6042, 'sensor':'W5R9','voltage':6,'sector':'MALTA'})
runDict.append({'run':6043, 'sensor':'W5R9','voltage':6,'sector':'p-well'})
#######################################################################################

parser=argparse.ArgumentParser()
parser.add_argument('-f','--file',type=str,help="input file",required=True)
parser.add_argument('-o','--output',type=str,help="output file",default="./output.root")
parser.add_argument('-b','--batch',help="enable batch mode",action="store_true")
parser.add_argument('-v','--verbose',help="enable verbose mode",action="store_true")
parser.add_argument('-r','--resolution',help="beam resolution [um]",type=float,default=2)
parser.add_argument('-c','--configFile',help="configuration file",type=str, default='test.ini')
args=parser.parse_args()

AtlasStyle.SetAtlasStyle()

if args.batch: ROOT.gROOT.SetBatch(True)

fr=args.file
tree=Tuple.Tuple()
chain=ROOT.TChain("DiamondAnaTuple")
chain.AddFile(fr)

bres=args.resolution
mymax=math.ceil(36.4*64)
mybin=int(mymax/bres)
mxmax=math.ceil(36.4*16)+1
mxbin=int(mxmax/bres)

print("xbin: %i, xmin: %.3f, xmax: %.3f" % (mxbin,0,mxmax))
print("ybin: %i, ymin: %.3f, ymax: %.3f" % (mybin,0,mymax))

run=0
if "_" in args.file:
    run=int(os.path.basename(args.file).split(".")[-2].split("_")[-1])
    pass

# configuration
if not (os.path.exists(args.configFile)):
  print "can't find config file, stopping"
  sys.exit()
print "using configuration file",args.configFile 
config = configparser.ConfigParser()
config.read(args.configFile)
refx = float(config['REF']['REFX'])
refy = float(config['REF']['REFY'])
pixxy = config['REF']['PIXXY'] 
pixxy = pixxy.split('-')
nPix = pixxy.__len__() # number of pixels for efficiencies and fitting
goodPixxy = config['REF']['GOODPIXXY'] 
goodPixxy = goodPixxy.split('-')
nGoodPix = goodPixxy.__len__()
nFullPix =  int(config['REF']['NFULLPIX'] )
for i in range(nPix):
  pixxy[i] = pixxy[i].split(',')
  pixxy[i][0] = float(pixxy[i][0])
  pixxy[i][1] = float(pixxy[i][1])

for i in range(nGoodPix):
  goodPixxy[i] = goodPixxy[i].split(',')
  goodPixxy[i][0] = float(goodPixxy[i][0])
  goodPixxy[i][1] = float(goodPixxy[i][1])
#mask noisy pixels
masked = []
masked = config['MASKING']['MASKED'] 
masked = masked.split('-')
nMask = masked.__len__() # number of pixels for suimmary tables and plots
if config['MASKING']['MASKED'] == '':
  masked = []
else:	
  for i in range(nMask):
    masked[i] = masked[i].split(',')
    masked[i][0] = int(masked[i][0])
    masked[i][1] = int(masked[i][1])

print "the run number is: ",run,", the masked pixels are:",masked

#declare histograms 

for i in range(len(runDict)):
  if runDict[i]['run'] == run:
    sector = runDict[i]['sector']
    sensor = runDict[i]['sensor']
    voltage = runDict[i]['voltage']

hHitM = ROOT.TH2F("hHitM","Clustered Hit Map %s %.1f V %s sector ;MALTA X [#mum];MALTA Y [#mum]" %(sensor,voltage, sector), mxbin, 0, mxmax, mybin, 0, mymax)
hClusM = ROOT.TH2F("hClusM ","Average Cluser Size %s %.1f V %s sector ;MALTA X [#mum];MALTA Y [#mum]" %(sensor,voltage, sector), mxbin, 0, mxmax, mybin, 0, mymax)
hMeanCM = ROOT.TH2F("hMeanCM","Clustered Hit Map %s %.1f V %s sector ;MALTA X [#mum];MALTA Y [#mum]" %(sensor,voltage, sector), mxbin, 0, mxmax, mybin, 0, mymax)
hHitNorm = ROOT.TH2F("hHitNorm","Normalized Clustered Hit Map %s %.1f V %s sector;MALTA X [#mum];MALTA Y [#mum]" %(sensor,voltage, sector), mxbin, 0, mxmax, mybin, 0, mymax)

gPixels={}
for pixx in xrange(4):
    gPixels[pixx]={}
    for pixy in xrange(4):
        gPixels[pixx][pixy]=ROOT.TH2F("Pixel_map","Pixel_map",mxbin, 0, mxmax, mybin, 0, mymax)
        gPixels[pixx][pixy].SetName("gPixel_%02i_%02i" % (pixx,pixy))
        gPixels[pixx][pixy].SetTitle(";MALTA X [um]; MALTA Y [um]")
        pass
    pass

xoff=  0.2095#0.2095
yoff= -18.6055 # -18.6055
vxpos=[]
vypos=[]
xmin=99999
xmax=-9999
ymin=99999
ymax=-9999
hmin=99999
hmax=-9999
effs=0.
effn=0.
timestart=time.time()
entry=1

dn=os.path.dirname(args.output)
if dn=="": dn="."
if not os.path.exists(dn):
    os.makedirs(dn)

print ("Output DQ file: DQ_clustered%06i.txt"%(run))
dq=open("%s/DQ_clustered%06i.txt"%(dn,run),"w+")
dq.write("MiniMALTA Diamond Test Beam analysis\n")
dq.write("Pixel \t Efficiency \t Uncertainty \t Uncertainty of Mean \t N hits\n")


##################################################################################################
#loop over entries in the ntuple

while True:
    chain.GetEntry(entry)
    if chain.LoadTree(entry)<0:
        break
    if entry%1000000==0:
        print "Processed %i events," % entry, " time taken: %.2f sec"%(time.time()-timestart)
        timestart=time.time()
        pass
    entry += 1
    tree.LoadBranches(chain)
    
    #get diamond postions  
    pixX = tree.pixelX.GetValue()
    pixY = tree.pixelY.GetValue()
    if pixX < pixxy[0][0]-1 or pixX > pixxy[3][0] + 1 or pixY < pixxy[0][1] -1 or pixY > pixxy[3][1] +1: continue 
    xpos=tree.x.GetValue()
    ypos=tree.y.GetValue()
    pixAil=tree.ail.GetValue()
    clusterSize = tree.clusterSize.GetValue()
    if not xpos in vxpos: vxpos.append(xpos)
    if not ypos in vypos: vypos.append(ypos)

    #convert from Diamond postion in mm to MALTA readout position in um
    nx=(ypos-yoff)*1000
    ny=(xpos-xoff)*1000
    ny=(36.4*64)-ny
    #get the limits in MALTA coordinates
    if xmin>nx:xmin=nx
    if xmax<nx:xmax=nx
    if ymin>ny:ymin=ny
    if ymax<ny:ymax=ny
    if run == 6035: # hard coding to handle two runs concatenated into one
      xmin=356
      xmax=456
      ymin=90
      ymax=190
    #values for the 2-d histogram limits
    maxy=hHitM.GetYaxis().GetXmax() 
    miny=hHitM.GetYaxis().GetXmin()
    maxx=hHitM.GetXaxis().GetXmax()
    minx=hHitM.GetXaxis().GetXmin()   

    #cut on fiducial region 

    #fill the hit and cluster maps
    hClusM.Fill(nx,ny,clusterSize);
    hHitM.Fill(nx,ny,1)
    gPixels[pixX - (pixxy[0][0] - 1) ][pixY  - (pixxy[0][1] - 1)].Fill(nx, ny, 1)
pass





#sum new normalized, clustered map from normalized gPixels
centerWidth = 5
for i in range(4):
  for j in range(4): 
    xCenter = refx + 36.4*(i-1) + 18.2
    yCenter = refy + 36.4*(j-1) + 18.2
    interpolateHitMap(gPixels[i][j])
    stats = normalizeMap(gPixels[i][j],xCenter, yCenter, bres, centerWidth)
    pixAvg = stats[0]
    gPixels[i][j].Scale(1/pixAvg)
      

respAvg = [[0 for x in range(2)] for y in range(2)]
respDev = [[0 for x in range(2)] for y in range(2)]
respDevMean = [[0 for x in range(2)] for y in range(2)]
for i in range(2):
  for j in range(2): 
    xCenter = refx + 36.4*(i) + 18.2
    yCenter = refy + 36.4*(j) + 18.2
    stats = normalizeMap(gPixels[i+1][j+1],xCenter, yCenter, bres, 18.2)
    respAvg[i][j] = stats[0]
    statsCenter = normalizeMap(gPixels[i+1][j+1],xCenter, yCenter, bres, centerWidth)
    respDev[i][j] = stats[1]
    respDevMean[i][ j] = stats[2] 
    #for k in range(gPixels[i+1][j+1].GetNbinsX()):
      #for m in range(gPixels[i+1][j+1].GetNbinsY()):
      #if gPixels[i+1][j+1].GetBinContent(k,m) < .07 : gPixels[i+1][j+1].SetBinContent(k,m,0)
    print "The average response is: ",respAvg[i][j]
    hHitNorm.Add(gPixels[i+1][j+1])

resp = np.mean(respAvg)
sigma = np.sqrt(np.mean(respDev))
sigmaMean = np.sqrt(np.mean(respDevMean))
centerAvg = np.mean(pixAvg)

dq.write("Averages: \t %f \t %f \t %f \t %f \n" %( resp, sigma, sigmaMean, centerAvg) )
dq.close()

if args.verbose:
    for x in vxpos: print "%0.3f => %6.3f" % (x,((x-xoff)*1000))
    for y in vypos: print "%0.3f => %6.3f" % (y,((y-yoff)*1000))
    
fw=ROOT.TFile.Open(args.output,"RECREATE")

#######################################
###### INTERPOLATION OF HIT MAPS ######
########################################
#interpolation of hitMap

print "Interpolating hit map..."
interpolateHitMap(hHitM)
interpolateHitMap(hClusM)


#divide hit maps to get average cluster size

hClusM.Divide(hHitM)
#xmin = refx - 10 
#xmax = refx + 72.4 + 10 
#ymin = refy - 10
#ymax = refy + 72.4 + 10
hRoom = 20
cClusM = ROOT.TCanvas()
hClusM.GetXaxis().SetRangeUser(xmin, xmax)
hClusM.GetYaxis().SetRangeUser(ymin, ymax + hRoom)
hClusM.GetZaxis().SetRangeUser(1,2.5)
hClusM.Draw("colz")
pt2 = ROOT.TPaveText(xmin+1, ymax+3, xmax-1, ymax+30)
pt2.SetTextFont(22)
pt2.SetTextSize(0.04)
pt2.AddText( "%s %.1f V %s sector Mean Cluster Size" %(sensor,voltage, sector) )
#pt2.AddText("Mean single-pixel normalized response = %.1f  +/- %.1f (%.1f) N = %.0f " %(resp*100, sigmaMean*100, sigma*100, centerAvg))
pt2.Draw()

cHitM = ROOT.TCanvas()
hHitM.GetXaxis().SetRangeUser(xmin, xmax)
hHitM.GetYaxis().SetRangeUser(ymin, ymax + hRoom )
hHitM.Draw("colz")
pt1 = ROOT.TPaveText(xmin+1, ymax+3, xmax-1, ymax+30)
pt1.SetTextFont(22)
pt1.SetTextSize(0.04)
for i in range(len(runDict)):
  if runDict[i]['run'] == run:
    sector = runDict[i]['sector']
    sensor = runDict[i]['sensor']
    voltage = runDict[i]['voltage']
pt1.AddText( "%s %.1f V %s sector Clustered Hit Map" %(sensor,voltage, sector) )
#pt1.AddText("Mean single-pixel normalized response = %.1f  +/- %.1f (%.1f) N = %.0f " %(resp*100, sigmaMean*100, sigma*100, centerAvg))
pt1.Draw()

cHitNormM = ROOT.TCanvas()
hHitNorm.GetXaxis().SetRangeUser(xmin, xmax)
hHitNorm.GetYaxis().SetRangeUser(ymin, ymax + hRoom)
hHitNorm.GetZaxis().SetRangeUser(0,1.2)
hHitNorm.Draw("colz")

pt = ROOT.TPaveText(xmin+1, ymax+3, xmax-1, ymax+30)
pt.SetTextFont(22)
pt.SetTextSize(0.04)
pt.AddText( "%s %.1f V %s sector Normalized Clustered Hit Map" %(sensor,voltage, sector) )
#pt.AddText("Mean single-pixel normalized response = %.1f  +/- %.1f (%.1f) N = %.0f " %(resp*100, sigmaMean*100, sigma*100, centerAvg))
pt.Draw()

cHitM.SaveAs("%s/ClusteredHitMap%i.png" % (dn, run) )
cHitM.SaveAs("%s/ClusteredHitMap%i.pdf" % (dn, run) )
cHitM.SaveAs("%s/ClusteredHitMap%i.C" % (dn, run) )

cHitNormM.SaveAs("%s/ClusteredNormHitMap%i.png" % (dn, run) )
cHitNormM.SaveAs("%s/ClusteredNormHitMap%i.pdf" % (dn, run) )
cHitNormM.SaveAs("%s/ClusteredNormHitMap%i.C" % (dn, run) )

cClusM.SaveAs("%s/ClusterSizeMap%i.png" % (dn, run) )
cClusM.SaveAs("%s/ClusterSizeMap%i.pdf" % (dn, run) )
cClusM.SaveAs("%s/ClusterSizeMap%i.C" % (dn, run) )

############################################################################
if not args.batch: raw_input("Press any key to continue.")
fw.Write()
fw.Close()
print("Have a nice day")

