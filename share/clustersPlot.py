#!/usr/bin/env python
###############################
# Plot diamond scan data
# 
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# Kaloyan.Metodiev@cern.ch
# Patrick.Freeman@cern.ch
# Maria.Mironova@cern.ch
# April 2019
###############################
import os
import sys
import ROOT
import time
import math
import signal
import argparse
import Tuple
import AtlasStyle

parser=argparse.ArgumentParser()
parser.add_argument('-f','--file',type=str,help="input file",required=True)
args=parser.parse_args()

AtlasStyle.SetAtlasStyle()


fr=ROOT.TFile.Open(args.file)
g1=fr.Get("gClusterSizeMap")

c1=ROOT.TCanvas("c1","c1",800,600)
g1.Draw("COLZ")

raw_input("wait...")
print("Have a nice day...")

    
