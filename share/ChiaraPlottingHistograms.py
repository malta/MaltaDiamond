#!/usr/bin/env python
import os
import sys
import time
import importlib
import argparse
import ROOT
import math
import configparser

ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetOptStat(00000)
ROOT.gStyle.SetPalette(1)


parser=argparse.ArgumentParser()
parser.add_argument('-r','--run',help='number of run',required=True)
parser.add_argument('-i','--histogram',help="initial file with histogram",required=False)
parser.add_argument('-c' ,'--coord',help='.ini file with cooor',required=False)
args=parser.parse_args()

run=args.run
print 'Run number: ' + run

ofolder="Results_"+run
os.system("mkdir -p "+ofolder+"/")

'''
##VD: I don't understand this :-(
coord=configparser.ConfigParser()
coord.read(args.coord)

nFullPix =  int(coord['REF']['NFULLPIX'])
print 'Number of pixels selected: ' + str(nFullPix)

pixxy = coord['REF']['PIXXY'] 
pixxy = pixxy.split('-')
nPix = pixxy.__len__() 
for i in range(nPix):
  pixxy[i] = pixxy[i].split(',')
  pixxy[i][0] = float(pixxy[i][0]) # Pixel X
  pixxy[i][1] = float(pixxy[i][1]) # Pixel Y

minX=pixxy[0][0]
maxX=pixxy[3][0]
minY=pixxy[0][1]
maxY=pixxy[3][1]

if nFullPix==4: 
  minX=minX-1
  maxX=maxX+1
  minY=minY-1
  maxY=maxY+1
else : print 'Number of pixels is not 4'
print 'minX is: ' + str(minX)+ ' maxX is: ' + str(maxX)
print 'minY is: ' + str(minY)+ ' maxY is: ' + str(maxY)
'''

tmpFile=ROOT.TFile(args.histogram)
CSmap_old=tmpFile.Get("hClSizeMap_nc")
CSmap_old_den=tmpFile.Get("hClSizeMap_nc_den")

c0=ROOT.TCanvas("c0","c0",800,1600)
c0.Divide(1,2)
c0.cd(1)
CSmap_old_den.Draw("COLZ")
c0.cd(2)
CSmap_old.Draw("COLZ")
c0.Print( ofolder+"/orig_"+run+".pdf")
os.system( "evince "+ofolder+"/orig_"+run+".pdf &")


#content of the original histograms
Xbins=CSmap_old.GetNbinsX()
Ybins=CSmap_old.GetNbinsY()
CSmap    =ROOT.TH2D("ClusterSizeMap"    , "ClusterSizeMap; PixelX; PixelY"    ,Xbins, 0-0.001,0.1+0.001, Ybins, 0-0.001, 0.1+0.001)
CSmap_den=ROOT.TH2D("ClusterSizeMap_den", "ClusterSizeMap_den; PixelX; PixelY",Xbins, 0-0.001,0.1+0.001, Ybins, 0-0.001, 0.1+0.001)

count=0
for i in range(Xbins+1):
  for j in range (Ybins+1):
     bins=CSmap_old.GetBinContent(i,j)
     if bins!=0: count+=1

inte=CSmap_old_den.Integral()
mean= inte/count
print ("")
print "Number of bins not empty: " + str(count) +" Integral: "+ str(inte) + " Mean: "+ str(mean)
print ("")


Xbins_den=CSmap_old_den.GetNbinsX()
Ybins_den=CSmap_old_den.GetNbinsY()
for i in range(Xbins_den+1):
  for j in range (Ybins_den+1): 
     bincontent=CSmap_old.GetBinContent(i,j)
     bincontent_den=CSmap_old_den.GetBinContent(i,j)
     if bincontent_den> (1.5*mean):
          CSmap_den.SetBinContent(i,j,0)
          CSmap.SetBinContent(i,j,0)
          print "PixelX: " +str(i)+ " PixelY: "+str(j)+ " has bincontent "+ str(bincontent_den)
     else :
          CSmap_den.SetBinContent(i,j,bincontent_den)
          CSmap.SetBinContent(i,j,bincontent)


c=ROOT.TCanvas("c1","c1",800,1600)
c.Divide(1,2)
c.cd(1)
CSmap_den.SetMaximum(mean*1.3)
CSmap_den.SetMinimum(mean*0.7)
CSmap_den.Draw("COLZ")
c.cd(2)
CSmap.SetMinimum(1)
CSmap.SetMaximum(2.4)
CSmap.Draw("COLZ")
c.Update()
c.SaveAs(ofolder+"/corr_"+run+".pdf")
os.system("evince "+ofolder+"/corr_"+run+".pdf &")
