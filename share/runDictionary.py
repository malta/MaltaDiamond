#dictionary of runs to plot y extents for
runDict = []
'''
runDict.append({'run':6056, 'sensor':'w1r9','voltage':8,'sector':'malta','fluence':7.1e14})
runDict.append({'run':6057, 'sensor':'w1r9','voltage':8,'sector':'ngap','fluence':7.1e14})
runDict.append({'run':6058, 'sensor':'w1r9','voltage':8,'sector':'pwell','fluence':7.1e14})

runDict.append({'run':6035, 'sensor':'w1r9','voltage':1.5,'sector':'malta','fluence':7.1e14})
runDict.append({'run':6036, 'sensor':'w1r9','voltage':1.5,'sector':'pwell','fluence':7.1e14})
runDict.append({'run':6037, 'sensor':'w1r9','voltage':1.5,'sector':'ngap','fluence':7.1e14})
'''
runDict.append({'run':6026, 'sensor':'w2r11','voltage':6,'sector':'pwell','fluence':0})
runDict.append({'run':6027, 'sensor':'w2r11','voltage':6,'sector':'ngap','fluence':0})
runDict.append({'run':6028, 'sensor':'w2r11','voltage':6,'sector':'malta','fluence':0})

runDict.append({'run':6032, 'sensor':'w2r1','voltage':1.5,'sector':'pwell','fluence':1e15})
runDict.append({'run':6033, 'sensor':'w2r1','voltage':1.5,'sector':'ngap','fluence':1e15})

runDict.append({'run':6049, 'sensor':'w2r1','voltage':6,'sector':'ngap','fluence':1e15})
runDict.append({'run':6050, 'sensor':'w2r1','voltage':6,'sector':'malta','fluence':1e15})

runDict.append({'run':6051, 'sensor':'w2r1','voltage':20,'sector':'malta','fluence':1e15})
runDict.append({'run':6052, 'sensor':'w2r1','voltage':20,'sector':'pwell','fluence':1e15})
runDict.append({'run':6055, 'sensor':'w2r1','voltage':20,'sector':'ngap','fluence':1e15})

runDict.append({'run':6038, 'sensor':'w2r9','voltage':1.5,'sector':'malta','fluence':5.0e14})
runDict.append({'run':6039, 'sensor':'w2r9','voltage':1.5,'sector':'pwell','fluence':5.0e14})
runDict.append({'run':6040, 'sensor':'w2r9','voltage':1.5,'sector':'ngap','fluence':5.0e14})

runDict.append({'run':6044, 'sensor':'w4r9','voltage':6,'sector':'malta','fluence':7e13})
runDict.append({'run':6045, 'sensor':'w4r9','voltage':6,'sector':'pwell','fluence':7e13})
runDict.append({'run':6048, 'sensor':'w4r9','voltage':6,'sector':'ngap','fluence':7e13})

runDict.append({'run':6041, 'sensor':'w5r9','voltage':6,'sector':'ngap','fluence':5.6e14})
runDict.append({'run':6042, 'sensor':'w5r9','voltage':6,'sector':'malta','fluence':5.6e14})
runDict.append({'run':6043, 'sensor':'w5r9','voltage':6,'sector':'pwell','fluence':5.6e14})

