#!/usr/bin/env python
import os
import sys
import ROOT
import time
import math
import Tuple
import signal
import argparse
import AtlasStyle
import PyMiniMaltaData
import DiamondStage

cont = True
def signal_handler(signal, frame):
    print "You pressed ctrl+C to quit" 
    global cont
    cont = False
    return

def findfile(name, path):
    for root, dirs, files in os.walk(path):
        for f in files:
            if name in f:
                return os.path.join(root, f)
            pass
        pass
    return ""

parser=argparse.ArgumentParser()
parser.add_argument('-m','--malta-file',help="malta run number")
parser.add_argument('-d','--diamond-file',help="diamond run number")
parser.add_argument('-mr','--malta-run',help="malta run number")
parser.add_argument('-dr','--diamond-run',help="diamond run number")
parser.add_argument('-o','--output-dir',help="output directory")
parser.add_argument('-b','--batch',help="enable batch mode",action='store_true')
args=parser.parse_args()

malta_file = args.malta_file
if not malta_file: malta_file=findfile(args.malta_run,"/home/sbmuser/MaltaSW/data")

diamond_file = args.diamond_file
if not diamond_file: diamond_file = findfile(args.diamond_run,"/mnt/b16/data/2019/mm22061-1")

run = args.malta_file.split("_")[-2]

dpos=DiamondStage.DiamondStage()
dpos.Open(diamond_file)

tree=Tuple.Tuple()
chain=ROOT.TChain("MALTA")
chain.AddFile(malta_file)

md=PyMiniMaltaData.MiniMaltaData()

if args.batch: ROOT.gROOT.SetBatch(True)
od="./"
if args.output_dir: od=args.output_dir
fw=ROOT.TFile("%s/align_%s.root"%(od,run),"RECREATE")

AtlasStyle.SetAtlasStyle()
tt=ROOT.TLatex()

gCorrX=ROOT.TGraph()
gCorrX.SetName("gCorrX")
gCorrX.SetTitle(";Malta X [pixel]; Diamond Y [mm]")
gCorrY=ROOT.TGraph()
gCorrY.SetName("gCorrX")
gCorrY.SetTitle(";Malta Y [pixel]; Diamond -X [mm]")

ymin=dpos.GetYmin()-dpos.GetXstep()/2
ymax=dpos.GetYmax()+dpos.GetXstep()/2
print "ymin: %.3f"%ymin
print "ymax: %.3f"%ymax
hCorrX =ROOT.TH2D("hCorrX", ";Malta X [pixel]; Diamond Y [mm]", 16,-0.5,     15.5,      dpos.GetYbins(),ymin,ymax)
hCorrXb=ROOT.TH2D("hCorrXb",";Malta X [#mum];  Diamond Y [mm]", 16,-0.5*36.4,15.5*36.4, dpos.GetYbins(),ymin,ymax)
ymin=-dpos.GetXmax()-dpos.GetXstep()/2
ymax=-dpos.GetXmin()+dpos.GetXstep()/2
print "ymin: %.3f"%ymin
print "ymax: %.3f"%ymax
hCorrY =ROOT.TH2D("hCorrY", ";Malta Y [pixel]; Diamond -X [mm]", 64,-0.5,     64.5,      dpos.GetXbins(),ymin,ymax)
hCorrYb=ROOT.TH2D("hCorrYb",";Malta Y [#mum];  Diamond -X [mm]", 64,-0.5*36.4,64.5*36.4, dpos.GetYbins(),ymin,ymax)

hCosTheta=ROOT.TH1D("hDotProd",";x*y'+y*(-x)' [cos#theta]",1000,-1,1)

hPosX=ROOT.TH1D("hPosX",";Diamond Y - Malta X [mm];", 120, dpos.GetYmin()-2, dpos.GetYmax()+2)
hPosY=ROOT.TH1D("hPosY",";Diamond X + Malta Y [mm];",  64, dpos.GetXmin()-2, dpos.GetXmax()+2)

signal.signal(signal.SIGINT, signal_handler)

entry=0
while cont:
    chain.GetEntry(entry)
    if chain.LoadTree(entry)<0:
        break
    if entry%1000==0:
        print "Processed %i events" % entry
        pass
    entry += 1
    tree.LoadBranches(chain)
    
    if tree.pixel.GetValue()==0: continue
    
    md.setWord1(int(tree.word1.GetValue()))
    md.setWord2(int(tree.word2.GetValue()))
    md.unpack()
    
    for i in xrange(md.getNhits()):
        gCorrX.SetPoint(gCorrX.GetN(),
                        md.getHitColumn(i),
                        dpos.GetY(tree.l1id.GetValue()))
        gCorrY.SetPoint(gCorrY.GetN(),
                        md.getHitRow(i),
                        -dpos.GetX(tree.l1id.GetValue()))
        hCorrX.Fill(md.getHitColumn(i), dpos.GetY(tree.l1id.GetValue()-1))
        hCorrY.Fill(md.getHitRow(i),   -dpos.GetX(tree.l1id.GetValue()-1))
        hCorrXb.Fill(md.getHitColumn(i)*36.4, dpos.GetY(tree.l1id.GetValue()-1))
        hCorrYb.Fill(md.getHitRow(i)*36.4,   -dpos.GetX(tree.l1id.GetValue()-1))

        xpix=md.getHitColumn(i)*36.4
        ypix=md.getHitRow(i)*36.4
        xmot=dpos.GetX(tree.l1id.GetValue()-1)*1000
        ymot=dpos.GetY(tree.l1id.GetValue()-1)*1000

        hPosX.Fill((ymot-xpix)/1000)
        hPosY.Fill((xmot+ypix)/1000)

        xmot0=xmot-dpos.GetXmin()*1000
        ymot0=ymot-dpos.GetYmin()*1000
        mpix=math.sqrt(xpix*xpix+ypix*ypix)+0.001
        mmot=math.sqrt(xmot*xmot+ymot*ymot)+0.001
        costheta=(xpix*ymot+ypix*(-xmot))/(mpix*mmot)
        hCosTheta.Fill(costheta)
        
        pass
    pass

c1=ROOT.TCanvas("c1","c1",800,1200)
c1.Divide(2,1)
c1.cd(1)
gCorrX.Draw("AP")
gCorrX.Write()
c1.cd(2)
gCorrY.Draw("AP")
gCorrX.Write()
c1.Update()

c2=ROOT.TCanvas("c2","c2",800,1200)
c2.Divide(2,1)
c2.cd(1)
hCorrX.Draw("COLZ")
hCorrX.Write()
c2.cd(2)
hCorrY.Draw("COLZ")
hCorrY.Write()
c2.Update()

c3=ROOT.TCanvas("c3","c3",800,1200)
c3.Divide(2,1)
c3.cd(1)
hCorrXb.Draw("COLZ")
hCorrXb.Write()
c3.cd(2)
hCorrYb.Draw("COLZ")
hCorrYb.Write()
c3.Update()

c4=ROOT.TCanvas("c4","c4",800,600)
hCosTheta.Draw()
hCosTheta.Write()
c4.Update()

c5=ROOT.TCanvas("c5","c5",800,1200)
c5.Divide(1,2)
c5.cd(1)
hPosX.Draw()
hPosX.Write()
c5.cd(2)
hPosY.Draw()
hPosY.Write()
c5.Update()

if not args.batch: raw_input("Press any key to continue")
fw.Close()

print("Have a nice day")
