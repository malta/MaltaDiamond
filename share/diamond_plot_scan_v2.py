#!/usr/bin/env python
###############################
# Plot diamond scan data
# 
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# Kaloyan.Metodiev@cern.ch
# Patrick.Freeman@cern.ch
# Maria.Mironova@cern.ch
# April 2019
###############################
import os
import sys
import ROOT
import time
import math
import signal
import argparse
import Tuple
#import AtlasStyle
import numpy as np
import configparser


def pix2bin(x,y):
    return x*64+y

def cSharingNperc(TH1Eff, nperc, pedestal):
  #function to get intetrpolated location of points which the relative efficiency falls below nperc percent of the maximum. Intended for use on TH1D projections of 2D histograms of pixel hitmatps

  xMax = TH1Eff.GetMaximumBin()
  valMax = TH1Eff.GetMaximum() - pedestal
  print "the Maximum value is ",valMax
  xLim = xMax
  xHi = xMax
  x = xMax
  while True:
    x = x -1 
    if TH1Eff.GetBinContent(x) - pedestal <= valMax * nperc/100.0:
      dy = TH1Eff.GetBinContent(x+1) - TH1Eff.GetBinContent(x)
      dx = TH1Eff.GetBinCenter(x+1) - TH1Eff.GetBinCenter(x)
      dyInt = valMax * nperc/100.0  - (TH1Eff.GetBinContent(x) - pedestal)
      xLo = TH1Eff.GetBinCenter(x) + dyInt * (dx/dy)
      break
  
  x = xMax
  while True: 
    x = x + 1 
    if TH1Eff.GetBinContent(x) - pedestal <= valMax * nperc/100.0:
      dy = TH1Eff.GetBinContent(x) - TH1Eff.GetBinContent(x-1)
      dx = TH1Eff.GetBinCenter(x) - TH1Eff.GetBinCenter(x-1)
      dyInt = valMax * nperc/100.0  -  (TH1Eff.GetBinContent(x) - pedestal)
      xHi = TH1Eff.GetBinCenter(x) + dyInt * (dx/dy)
      break

  return [xLo, xHi]
#############################################################################################

runDict = []
runDict.append({'run':6056, 'sensor':'W1R9','voltage':8,'sector':'MALTA'})
runDict.append({'run':6057, 'sensor':'W1R9','voltage':8,'sector':'p-well'})
runDict.append({'run':6058, 'sensor':'W1R9','voltage':8,'sector':'n-gap'})

runDict.append({'run':6035, 'sensor':'W1R9','voltage':1.5,'sector':'MALTA'})
runDict.append({'run':6036, 'sensor':'W1R9','voltage':1.5,'sector':'p-well'})
runDict.append({'run':6037, 'sensor':'W1R9','voltage':1.5,'sector':'n-gap'})

runDict.append({'run':6026, 'sensor':'W2R11','voltage':6,'sector':'p-well'})
runDict.append({'run':6027, 'sensor':'W2R11','voltage':6,'sector':'n-gap'})
runDict.append({'run':6028, 'sensor':'W2R11','voltage':6,'sector':'MALTA'})

runDict.append({'run':6032, 'sensor':'W2R1','voltage':1.5,'sector':'p-well'})
runDict.append({'run':6033, 'sensor':'W2R1','voltage':1.5,'sector':'n-gap'})

runDict.append({'run':6049, 'sensor':'W2R1','voltage':6,'sector':'n-gap'})
runDict.append({'run':6050, 'sensor':'W2R1','voltage':6,'sector':'MALTA'})

runDict.append({'run':6051, 'sensor':'W2R1','voltage':20,'sector':'MALTA'})
runDict.append({'run':6052, 'sensor':'W2R1','voltage':20,'sector':'p-well'})
runDict.append({'run':6055, 'sensor':'W2R1','voltage':20,'sector':'n-gap'})

runDict.append({'run':6038, 'sensor':'W2R9','voltage':1.5,'sector':'MALTA'})
runDict.append({'run':6039, 'sensor':'W2R9','voltage':1.5,'sector':'p-well'})
runDict.append({'run':6040, 'sensor':'W2R9','voltage':1.5,'sector':'n-gap'})

runDict.append({'run':6044, 'sensor':'W4R9','voltage':6,'sector':'MALTA'})
runDict.append({'run':6045, 'sensor':'W4R9','voltage':6,'sector':'p-well'})
runDict.append({'run':6048, 'sensor':'W4R9','voltage':6,'sector':'n-gap'})

runDict.append({'run':6041, 'sensor':'W5R9','voltage':6,'sector':'n gap'})
runDict.append({'run':6042, 'sensor':'W5R9','voltage':6,'sector':'MALTA'})
runDict.append({'run':6043, 'sensor':'W5R9','voltage':6,'sector':'p-well'})
#######################################################################################

parser=argparse.ArgumentParser()
parser.add_argument('-f','--file',type=str,help="input file",required=True)
parser.add_argument('-o','--output',type=str,help="output file",default="./output.root")
parser.add_argument('-b','--batch',help="enable batch mode",action="store_true")
parser.add_argument('-v','--verbose',help="enable verbose mode",action="store_true")
parser.add_argument('-r','--resolution',help="beam resolution [um]",type=float,default=2)
parser.add_argument('-c','--configFile',help="configuration file",type=str, default='test.ini')
parser.add_argument('-n','--normalization',help="enable hit map normalization",action="store_true")
parser.add_argument('-clus','--clusters',help="enable settings for clusters",action="store_true")
#parser.add_argument('-s','--sector',help="beam resolution [um]",type=float,default=2)
args=parser.parse_args()

#AtlasStyle.SetAtlasStyle()

if args.batch: ROOT.gROOT.SetBatch(True)

fr=args.file
tree=Tuple.Tuple()
chain=ROOT.TChain("DiamondAna")
chain.AddFile(fr)

bres=args.resolution
mymax=math.ceil(36.4*64)
mybin=int(mymax/bres)
mxmax=math.ceil(36.4*16)+1
mxbin=int(mxmax/bres)

print("xbin: %i, xmin: %.3f, xmax: %.3f" % (mxbin,0,mxmax))
print("ybin: %i, ymin: %.3f, ymax: %.3f" % (mybin,0,mymax))

run=0
if "_" in args.file:
    run=int(os.path.basename(args.file).split(".")[-2].split("_")[-1])
    pass

# configuration
if not (os.path.exists(args.configFile)):
  print "can't find config file, stopping"
  sys.exit()
print "using configuration file",args.configFile 
config = configparser.ConfigParser()
config.read(args.configFile)
refx = float(config['REF']['REFX'])
refy = float(config['REF']['REFY'])
pixxy = config['REF']['PIXXY'] 
pixxy = pixxy.split('-')
nPix = pixxy.__len__() # number of pixels for efficiencies and fitting
goodPixxy = config['REF']['GOODPIXXY'] 
goodPixxy = goodPixxy.split('-')
nGoodPix = goodPixxy.__len__()
nFullPix =  int(config['REF']['NFULLPIX'] )
for i in range(nPix):
  pixxy[i] = pixxy[i].split(',')
  pixxy[i][0] = float(pixxy[i][0])
  pixxy[i][1] = float(pixxy[i][1])

for i in range(nGoodPix):
  goodPixxy[i] = goodPixxy[i].split(',')
  goodPixxy[i][0] = float(goodPixxy[i][0])
  goodPixxy[i][1] = float(goodPixxy[i][1])
#mask noisy pixels
masked = []
masked = config['MASKING']['MASKED'] 
masked = masked.split('-')
nMask = masked.__len__() # number of pixels for suimmary tables and plots
if config['MASKING']['MASKED'] == '':
  masked = []
else:	
  for i in range(nMask):
    masked[i] = masked[i].split(',')
    masked[i][0] = int(masked[i][0])
    masked[i][1] = int(masked[i][1])

print "the run number is: ",run,", the masked pixels are:",masked

#declare histograms 
hHitM=ROOT.TH2F("hHitM",";MALTA X [#mum];MALTA Y [#mum]", mxbin, 0, mxmax, mybin, 0, mymax)
gHitM=ROOT.TGraph2D()
gHitM.SetTitle("Interpolated In-Pixel response; MALTA X; MALTA Y; Hits ");
hYpos=ROOT.TH1I("hYpos",";Stage Y [mm]",2*500 +1,-19.000-0.001,-17.000+0.001)
hXpos=ROOT.TH1I("hXpos",";Stage X [mm]",2*500 +1,  0.500-0.001,  2.500+0.001)
gDpos=ROOT.TGraph()
gDpos.SetNameTitle("gDpos","Postition Map;Stage X [mm]; Stage Y [mm]")
gDpos.SetMarkerColor(ROOT.kRed)
gDpos.SetMarkerStyle(ROOT.kFullSquare)

gPixels={}
for pixx in xrange(16):
    gPixels[pixx]={}
    for pixy in xrange(64):
        gPixels[pixx][pixy]=ROOT.TH2F("Pixel_map","Pixel_map",mxbin, 0, mxmax, mybin, 0, mymax)
        gPixels[pixx][pixy].SetName("gPixel_%02i_%02i" % (pixx,pixy))
        gPixels[pixx][pixy].SetTitle(";MALTA X [um]; MALTA Y [um]")
        pass
    pass

xoff=  0.2095#0.2095
yoff= -18.6055 # -18.6055
vxpos=[]
vypos=[]
xmin=99999
xmax=-9999
ymin=99999
ymax=-9999
hmin=99999
hmax=-9999
effs=0.
effn=0.
timestart=time.time()
entry=1
#loop over l1 triggers (called entry here)
hCentralPixelX = ROOT.TH1I("CentralPixelX","CentralPixelX", 16, 0, 15 )
hCentralPixelY = ROOT.TH1I("CentralPixelY","CentralPixelY", 64, 0, 63 )
while True:
    chain.GetEntry(entry)
    if chain.LoadTree(entry)<0:
        break
    if entry%100==0:
        print "Processed %i events," % entry, " time taken: %.2f sec"%(time.time()-timestart)
        timestart=time.time()
        pass
    entry += 1
    tree.LoadBranches(chain)
    
    #get diamond postions   
    xpos=tree.x.GetValue()
    ypos=tree.y.GetValue()
    pixail=tree.ail.GetValue()

    if not xpos in vxpos: vxpos.append(xpos)
    if not ypos in vypos: vypos.append(ypos)

    hXpos.Fill(xpos)
    hYpos.Fill(ypos)
    gDpos.SetPoint(gDpos.GetN(),ypos,-xpos)
    #convert from Diamond postion in mm to MALTA readout position in um
    nx=(ypos-yoff)*1000
    ny=(xpos-xoff)*1000
    ny=(36.4*64)-ny
    if xmin>nx:xmin=nx
    if xmax<nx:xmax=nx
    if ymin>ny:ymin=ny
    if ymax<ny:ymax=ny
    if run == 6035:
      xmin=356
      xmax=456
      ymin=90
      ymax=190
    maxy=hHitM.GetYaxis().GetXmax() 
    miny=hHitM.GetYaxis().GetXmin()
    maxx=hHitM.GetXaxis().GetXmax()
    minx=hHitM.GetXaxis().GetXmin()   

    #loop to create a tgraph for each pixel
    maxHits = 0 # maximum number of hits in l1A, assumed to be beam position
    # position of pixel with max hits
    xMax = goodPixxy[0][0]
    yMax = goodPixxy[0][1]
    hCentralPixelX.Fill(xMax)
    hCentralPixelY.Fill(yMax)
    #location of pixels surrounding the pixel with the most hits
    xMin = max(int(xMax) - 2,0)
    xMax = min(int(xMax)+3, 15)
    yMin = max(int(yMax) -2,0)
    yMax = min(int(yMax) +3,63)

    for pixx in xrange(16):
        for pixy in xrange(64):
            #n=gPixels[pixx][pixy].GetN()
            hits=float(tree.hits.GetValue(pix2bin(pixx,pixy)))
	    hits = float(hits/pixail)*.042  #normalization to ail and scaling for number of hits
            gPixels[pixx][pixy].Fill(nx,ny,hits)
	    if hits>maxHits:
              if [pixx,pixy] in masked: continue 
	      maxHits = hits
            pass
        pass
    #print "for level 1 trigger ",entry,"and max pixel (",xMax,",",yMax,"), the number of hits is ",int(maxHits)
    hits = 0.
    #loop to get number of hits at each point
    for pixx in xrange(xMin,xMax, 1 ):
        for pixy in xrange(yMin,yMax ,1):
            hitsTemp=float(tree.hits.GetValue(pix2bin(pixx,pixy))) #hits in the pixel in l1id
	    hitsTemp = float(hitsTemp/pixail)*.042; #factor to (approximately) account for ail normalization
            if hitsTemp<=maxHits*0.07:continue #hits = mean 
            if nx<minx or nx>maxx:
                print("X out of range: %.3f [%.3f,%.3f]"%(nx,minx,maxx))
                pass
            if ny<miny or ny>maxy:
                print("Y out of range: %.3f [%.3f,%.3f]"%(ny,miny,maxy))
                pass
            hits+=hitsTemp
            effs+=hits
            effn+=1
            pass
        pass
    pass
    if hmin>hits:hmin=hits
    if hmax<hits:hmax=hits
    hHitM.Fill(nx,ny,hits)
    gHitM.SetPoint(entry-2,nx,ny,hits)
pass

if args.verbose:
    for x in vxpos: print "%0.3f => %6.3f" % (x,((x-xoff)*1000))
    for y in vypos: print "%0.3f => %6.3f" % (y,((y-yoff)*1000))
    
fw=ROOT.TFile.Open(args.output,"RECREATE")
dn=os.path.dirname(args.output)
if dn=="": dn="."
if not os.path.exists(dn):
    os.makedirs(dn)
#######################################
###### INTERPOLATION OF HIT MAPS ######
########################################
#interpolation of hitMap

print "Interpolating hit map..."
for x in range(mxbin):
  for y in range(mybin):
    neighbors = [hHitM.GetBinContent(x-1,y), hHitM.GetBinContent(x+1,y), hHitM.GetBinContent(x,y-1), hHitM.GetBinContent(x,y+1)]
    intVal = 0
    nNeighbors=0.00000000000000001
    for i in range(4):
      if neighbors[i] > 1:
        nNeighbors+=1
        intVal+=neighbors[i] 
    intVal = intVal/nNeighbors 
    if nNeighbors >= 2 and hHitM.GetBinContent(x,y) == 0:
      hHitM.SetBinContent(x,y,intVal)
    pass
  pass
pass

#do interpolation for relevant pixels in gPixels

print "Interpolating pixel maps..."
projMiny = round((refy - 36.0)/bres)
projMaxy = round((refy +108.0)/bres)
projMinx = round((refx - 36.0)/bres)
projMaxx = round((refx + 108.0)/bres)

for p in range(nPix):
  for x in range(int(projMinx),int(projMaxx)):
    for y in range(int(projMiny),int(projMaxy)):
      xP = pixxy[p][0]
      yP = pixxy[p][1]
      neighbors = [gPixels[xP][yP].GetBinContent(x-1,y), gPixels[xP][yP].GetBinContent(x+1,y), gPixels[xP][yP].GetBinContent(x,y-1), gPixels[xP][yP].GetBinContent(x,y+1)]
      intVal = 0
      nNeighbors=0.00000000000000001
      for i in range(4):
        if neighbors[i] >0:
          nNeighbors+=1
          intVal+=neighbors[i] 
      intVal = intVal/nNeighbors 
      if nNeighbors >= 2 and gPixels[xP][yP].GetBinContent(x,y) <= 1e-9:
         gPixels[xP][yP].SetBinContent(x,y,intVal)

#interpolation for gPixels - WARNING takes like an hour on 2um scans for the now... 
#may want to put in separate piece of code
#commenting out, will just use projections that are actually interesting later
'''
for pixx in xrange(16):
  for pixy in xrange(64):
    for x in range(0,int(mxbin)):
      for y in range(0,int(mybin)):
        neighbors = [gPixels[pixx][pixy].GetBinContent(x-1,y), gPixels[pixx][pixy].GetBinContent(x+1,y), gPixels[pixx][pixy].GetBinContent(x,y-1), gPixels[pixx][pixy].GetBinContent(x,y+1)]
        intVal = 0
        nNeighbors=0.00000000000000001
        for i in range(4):
          if neighbors[i] >= 1:
            nNeighbors+=1
            intVal+=neighbors[i] 
        intVal = intVal/nNeighbors 
        if nNeighbors >= 2 and gPixels[pixx][pixy].GetBinContent(x,y) == 0:
           gPixels[pixx][pixy].SetBinContent(x,y,intVal)
           print "Interpolating for bin: ",x,",",y," The interpolated value is: ",intVal 
'''
#######################################################
### projections and fits ##############################
#######################################################
print "creating projections and fits"
projN = {}
projNy = {}
cProjN = ROOT.TCanvas("projN", "projN", 1000, 800)
cProjNy = ROOT.TCanvas("projNy", "projNy", 1000, 800)
projFit = {}
projFitY = {}
projH = ROOT.TH1D()
pname = ''
print "the projection has the y-limits:",projMiny,projMaxy
limits = {}
limitsY = {}
widthY = {}
width = {}
xCenters = []
yCenters = []
nperc = 5

for i in range(nPix):
  #x=projections
  cProjN.cd()
  pName = "cProj"+str(i)
  projH.SetName(pname)
  projH = gPixels[pixxy[i][0]][pixxy[i][1]].ProjectionX(pname,int(projMiny), int(projMaxy))
  projN[i] = projH
  fitName = pName+"fit"
  xCenter = projH.GetMean()
  projFit[i] = ROOT.TF1(fitName,"[0] * (TMath::Erf((x-[1])/[2]*sqrt(2)) + TMath::Erf(([3]-x)/[4]*sqrt(2)) ) +[5] ", xCenter -40, xCenter+ 40 ) 
  projFit[i].SetParameter(0,projH.GetMaximum()/2)
  projFit[i].SetParameter(2,5)
  projFit[i].SetParLimits(2,1,15)
  projFit[i].SetParameter(4,5)
  projFit[i].SetParLimits(4,1,15)
  projFit[i].SetParameter(1,xCenter-20)
  projFit[i].SetParLimits(1,xCenter-40, xCenter-10 )
  projFit[i].SetParameter(3,xCenter+20)
  projFit[i].SetParLimits(3,xCenter+10, xCenter+40 )
  projFit[i].SetParLimits(5, 0, projH.GetMaximum()/4 )
  projFit[i].SetParameter(5, projH.GetMaximum()/10.0)
  projH.SetTitle("X-Projection of pixel [%d, %d]" %(pixxy[i][0], pixxy[i][0]))
  projH.GetXaxis().SetTitle( "MALTA X (um)")
  projH.Draw()
  projH.GetXaxis().SetRangeUser(xCenter-40, xCenter+40)
  projH.Fit(fitName, "W")
  limits[i] = cSharingNperc(projH, nperc,projFit[i].GetParameter(5))
  width[i] = limits[i][1]-limits[i][0]
  csLineLo = ROOT.TLine(limits[i][0], 0, limits[i][0], projH.GetMaximum())
  csLineLo.SetLineColor(3)
  csLineLo.Draw("same")
  csLineHi = ROOT.TLine(limits[i][1], 0, limits[i][1], projH.GetMaximum())
  csLineHi.SetLineColor(3)
  csLineHi.Draw("same")
  xCenters.append( (projFit[i].GetParameter(3) +  projFit[i].GetParameter(1) )/2 )
  cProjN.SaveAs("%s/PixelFit_%d.pdf" % (dn, i))
  cProjN.SaveAs("%s/PixelFit_%d.C" % (dn, i))
  #y-projections
  cProjNy.cd()
  pName = "cProjY"+str(i)
  projH.SetName(pname)
  projH = gPixels[pixxy[i][0]][pixxy[i][1]].ProjectionY(pname,int(projMinx), int(projMaxx))
  projNy[i] = projH
  fitName = pName+"fit"
  yCenter = projH.GetMean()
  projFitY[i] = ROOT.TF1(fitName,"[0] * (TMath::Erf((x-[1])/[2]*sqrt(2)) + TMath::Erf(([3]-x)/[4]*sqrt(2)) ) +[5] ", -30, yCenter+ 30 ) 
  projFitY[i].SetParameter(0, projH.GetMaximum()/2)
  projFitY[i].SetParameter(2,5)
  projFitY[i].SetParLimits(2,1,15)
  projFitY[i].SetParameter(4,5)
  projFitY[i].SetParLimits(4,1,15)
  projFitY[i].SetParameter(1,yCenter-20)
  projFitY[i].SetParLimits(1,yCenter-40, yCenter-10 )
  projFitY[i].SetParameter(3,yCenter+20)
  projFitY[i].SetParLimits(3,yCenter+10, yCenter+40 )
  projFitY[i].SetParLimits(5, 0, projH.GetMaximum()/4 )
  projFitY[i].SetParameter(5, projH.GetMaximum()/10.0)
  projH.Fit(fitName, "W")
  limitsY[i] = cSharingNperc(projH, nperc, projFitY[i].GetParameter(5))
  widthY[i] = limitsY[i][1]-limitsY[i][0]
  projH.SetTitle("Y-Projection of pixel [%d, %d]" %(pixxy[i][0], pixxy[i][0]))
  projH.GetXaxis().SetTitle( "MALTA Y (um)")
  if i == 0: 
    projH.Draw("")
  else: 
    projH.Draw("same")
  projH.GetXaxis().SetRangeUser(yCenter-40, yCenter+40)
  csLineLoY = ROOT.TLine(limitsY[i][0], 0, limitsY[i][0], projH.GetMaximum()  )
  csLineLoY.SetLineColor(3)
  csLineLoY.Draw("same")
  csLineHiY = ROOT.TLine(limitsY[i][1], 0, limitsY[i][1], projH.GetMaximum()  )
  csLineHiY.SetLineColor(3)
  csLineHiY.Draw("same")
  yCenters.append( (projFitY[i].GetParameter(3) +  projFitY[i].GetParameter(1) )/2 )
  cProjNy.SaveAs("%s/PixelFitY_%d.pdf" % (dn, i))
  cProjNy.SaveAs("%s/PixelFitY_%d.C" % (dn, i))

cProjN.Update()
cProjN.Write()
cProjNy.Update()
cProjNy.Write()
  
refx = np.mean( xCenters )  - 36.4
refy = np.mean( yCenters )  - 36.4

if nFullPix == 1:
  refx = np.mean( xCenters[0] )  - 18.2
  refy = np.mean( yCenters[0] )  - 18.2
elif nFullPix == 2:
  refx = np.mean( xCenters[2] )  - 18.2
  refy = np.mean( yCenters[2] )  - 18.2 - 36.4  
elif nFullPix == 3:
  refx = np.mean( xCenters[3] )  - 18.2 - 36.4
  refy = np.mean( yCenters[3] )  - 18.2 - 36.4 
else: 
  refx = np.mean( xCenters )  - 36.4
  refy = np.mean( yCenters )  - 36.4

print "the reference point is now: ",refx,refy
#########################################
#in-pixel calculations
##########################################
#Normalization to pixel centers
hW = 5 #half the width of the central square for efficiency calculations, in microns
xy = [[18.2-hW,18.2-hW,18.2+hW,18.2+hW],[54.6 - hW,18.2-hW,54.6 + hW,18.2+hW],[18.2-hW,54.6 - hW,18.2+hW,54.6 + hW],[54.6 - hW,54.6 - hW,54.6 + hW,54.6 + hW]] # corners of 6x6 squares for relative efficiency calculations
centerAvg=[0,0,0,0] #average of number of hits in center area of pixel
pixAvg=[0,0,0,0] #average of number of hits in center area of pixel
squares =[0,0,0,0] #average of square of number of hits in center area of pixel. for variance calculation
stdDev  = [0,0,0,0]
stdDevMean = [0,0,0,0]
  #place central region relative to reference point

for i in range(nPix):
  xy[i][0] += refx
  xy[i][1] += refy
  xy[i][2] += refx
  xy[i][3] += refy
  for j in range(nPix):
    xy[i][j] = xy[i][j]/bres


for pix in range(nPix):
  xlo = int( np.floor(xy[pix][0]) )
  xhi = int( np.ceil(xy[pix][2] ) )
  ylo = int( np.floor(xy[pix][1] ))
  yhi = int( np.ceil(xy[pix][3] ) )
  centerAvg[pix]= 1e-9
  squares[pix]= 1e-9
  count=1e-9
  xP = pixxy[pix][0]
  yP = pixxy[pix][1]
  for x in range (xlo,xhi):
    for y in range (ylo, yhi):
      scale = 1.0
      if x == xlo:
        scale = scale * ( xlo + 1 - xy[pix][0] ) 
      if x == xhi - 1:
        scale = scale * ( xy[pix][2] - (xhi -1) )
      if y == ylo:
        scale = scale * (ylo + 1 - xy[pix][1] )
      if y == yhi - 1:
        scale = scale * (xy[pix][3] - (yhi - 1) )
      if gPixels[xP][yP].GetBinContent(x,y) > 0:
        centerAvg[pix] += gPixels[xP][yP].GetBinContent(x,y)*scale
	squares[pix] += (gPixels[xP][yP].GetBinContent(x,y)*scale) * gPixels[xP][yP].GetBinContent(x,y)
        count += 1.0*scale
      if args.clusters:
        centerAvg[pix] += hHitM.GetBinContent(x,y)*scale
	squares[pix] += ( hHitM.GetBinContent(x,y) *  hHitM.GetBinContent(x,y)) *scale
    pass
  pass 
    	  
  squares[pix] = squares[pix]/count 
  centerAvg[pix]= centerAvg[pix]/(count)
  stdDev[pix] = np.sqrt( squares[pix] - centerAvg[pix] * centerAvg[pix] )
  stdDevMean[pix] = np.sqrt( squares[pix] - centerAvg[pix] * centerAvg[pix] ) /np.sqrt(count-1)
  print "the standard deviation and of the mean are", stdDev,"and ",stdDevMean
  stdDevMean[pix] = stdDevMean[pix]/centerAvg[pix] #normalization of the standard deviation
  stdDev[pix] = stdDev[pix]/centerAvg[pix] #normalization of the standard deviation

  print "the average for pixel",xP,yP," is ",centerAvg[pix]
pass

#normalize gpix, hitmap by average of average pixel relative efficiencies
  #hcenterAvg= hcenterAvg/hCount #normalization from entire hit map
normalization = np.mean(centerAvg)
for pixx in xrange(16):
  for pixy in xrange(64):
    gPixels[pixx][pixy].Scale(1.0/normalization)
if args.normalization:
  hHitM.Scale(1.0/normalization)

for i in range(nPix):
  gPixels[pixxy[i][0]][pixxy[i][1]].Scale(normalization/centerAvg[i])
pass

#Relative efficiency calculations
xy = [[0,0,36.4,36.4],[36.4,0,72.8,36.4],[0,36.4,36.4,72.8],[36.4,36.4,72.8,72.8]] #  # array of corrdinates of corners
stdTotal = [0,0,0,0]
stdEff = [0,0,0,0]

for i in range(nPix):
  xy[i][0] += refx
  xy[i][1] += refy
  xy[i][2] += refx
  xy[i][3] += refy
  for j in range(nPix):
    xy[i][j] = xy[i][j]/bres

for pix in range(nPix):
  xlo = int( np.floor(xy[pix][0]) )
  xhi = int( np.ceil(xy[pix][2] ) )
  ylo = int( np.floor(xy[pix][1] ))
  yhi = int( np.ceil(xy[pix][3] ) )
  pixAvg[pix]=0
  squares[pix]=0
  count=0
  xP = pixxy[pix][0]
  yP = pixxy[pix][1]
  for x in range (xlo,xhi):
    for y in range (ylo, yhi):
      if gPixels[xP][yP].GetBinContent(x,y) > 0:
	scale = 1.0
	if x == xlo:
	  scale = scale * ( xlo + 1 - xy[pix][0] ) 
	if x == xhi - 1:
	  scale = scale * ( xy[pix][2] - (xhi -1) )
	if y == ylo:
	  scale = scale * (ylo + 1 - xy[pix][1] )
	if y == yhi - 1:
	  scale = scale * (xy[pix][3] - (yhi - 1) )
        pixAvg[pix] += min(gPixels[xP][yP].GetBinContent(x,y),1)*scale
	squares[pix] += min(gPixels[xP][yP].GetBinContent(x,y),1)*scale ** 2
        count += 1.0*scale
        if args.clusters:
          pixAvg[pix] += hHitM.GetBinContent(x,y)*scale
	  squares[pix] += ( hHitM.GetBinContent(x,y) *  hHitM.GetBinContent(x,y))
    pass
  pass
  
    
  pixAvg[pix] = pixAvg[pix]/count
  squares[pix] = squares[pix]/count
  stdTotal[pix] = np.sqrt(squares[pix]  - pixAvg[pix]*pixAvg[pix])
  #stdEff[pix]   = np.sqrt( (stdTotal[pix]) ** 2 +  stdDev[pix]  ** 2 ) 
  stdEff[pix]  = stdDev[pix]
  print "the average relative efficiency for pixel",xP,yP," is ",pixAvg[pix]," with an uncertainty of ",stdEff[pix] 
pass


#plot positions of pixels which were hit most in a given l1a trigger
cPos=ROOT.TCanvas("cPos","cPos",800,600)
cPos.Divide(2,1)
cPos.cd(1).Divide(1,2)
cPos.cd(1).cd(1)
hXpos.Draw()
hXpos.Write()
cPos.cd(1).cd(2)
hYpos.Draw()
hYpos.Write()
cPos.cd(2)
gDpos.Draw()
gDpos.Write()
cPos.Update()
cPos.Write()

fixedX =  (gHitM.GetXmax() + gHitM.GetXmin() )/2
fixedY =   (gHitM.GetYmax() + gHitM.GetYmin() )/2
centralPixel = [int(fixedX/36.4), int(fixedY/36.4)]
yBin = int(fixedY/bres)#int((fixedY- gHitM.GetYmin())/bres )
print "yBin: ",yBin

cCenter = ROOT.TCanvas("center","centralPixel")
cCenter.Divide(2,1)
cCenter.cd(1)
hCentralPixelX.Draw("") 
cCenter.cd(2)
hCentralPixelY.Draw("")
fitX  = ROOT.TF1("fitX","gaus",hCentralPixelX.GetMinimum(),hCentralPixelX.GetMaximum());
fitY  = ROOT.TF1("fitY","gaus",hCentralPixelY.GetMinimum(),hCentralPixelY.GetMaximum());
hCentralPixelX.Fit("fitX") 
hCentralPixelY.Fit("fitY") 
centralPixel = [round(fitX.GetParameter(1)), round(fitY.GetParameter(1))]

print "Central pixel: ",centralPixel
cHitM=ROOT.TCanvas("cHitM","Hit Map",800,600)
cHitM.cd()
hHitM.GetXaxis().SetRangeUser(xmin,xmax)
hHitM.GetYaxis().SetRangeUser(ymin,ymax+20)
if args.normalization:
  hHitM.GetZaxis().SetRangeUser(0,2.5)
  if args.clusters:
      hHitM.GetZaxis().SetRangeUser(0,1.5)
else:  
  hHitM.GetZaxis().SetRangeUser(hHitM.GetMinimum(),hHitM.GetMaximum())

hHitM.Draw("COLZ")
pix0=ROOT.TBox(xy[0][0]*bres, xy[0][1]*bres, xy[0][2]*bres, xy[0][3]*bres)
pix0.SetFillStyle(0)
pix0.SetLineColor(0)
pix0.SetLineWidth(3)
pix0.Draw("same")
pix1=ROOT.TBox(xy[1][0]*bres, xy[1][1]*bres, xy[1][2]*bres, xy[1][3]*bres)
pix1.SetFillStyle(0)
pix1.SetLineColor(0)
pix1.SetLineWidth(3)
pix1.Draw("same")
pix2=ROOT.TBox(xy[2][0]*bres, xy[2][1]*bres, xy[2][2]*bres, xy[2][3]*bres)
pix2.SetFillStyle(0)
pix2.SetLineColor(0)
pix2.SetLineWidth(3)
pix2.Draw("same")
pix3=ROOT.TBox(xy[3][0]*bres, xy[3][1]*bres, xy[3][2]*bres, xy[3][3]*bres)
pix3.SetFillStyle(0)
pix3.SetLineColor(0)
pix3.SetLineWidth(3)
pix3.Draw("same")
#projLine = ROOT.TLine(minx, fixedY, maxx, fixedY)
#projLine.SetLineColor(1)
#projLine.Draw("same")

for i in range(nPix):
  if [pixxy[i][0],pixxy[i][1]] == goodPixxy[0]:
    goodI = i

cPixM = ROOT.TCanvas("cPixM", "Pixel Hit Map" )
cPixM.cd()
hPixM = gPixels[goodPixxy[0][0]][goodPixxy[0][1]]
hPixM.GetXaxis().SetRangeUser(xmin,xmax)
hPixM.GetYaxis().SetRangeUser(ymin,ymax+20)
hPixM.GetZaxis().SetRangeUser(0,1.1)
hPixM.Draw("colz")
pix=ROOT.TBox(xy[goodI][0]*bres, xy[goodI][1]*bres, xy[goodI][2]*bres, xy[goodI][3]*bres)
pix.SetFillStyle(0)
pix.SetLineColor(0)
pix.SetLineWidth(3)
pix.Draw("same")
mid=ROOT.TBox(xy[goodI][0]*bres+ 18.2 - hW, xy[goodI][1]*bres+ 18.2 - hW, xy[goodI][2]*bres-18.2 +hW, xy[goodI][3]*bres - 18.2 +hW)
mid.SetFillStyle(0)
mid.SetLineColor(0)
mid.SetLineWidth(3)
mid.Draw("same")

if not args.normalization:
  hHitM.SetMaximum(hHitM.GetMaximum())
  hHitM.SetMinimum(hmin-1)
hHitM.Write()
cHitM.Update()
#draw 1-d projections of individual pixels
#can switch to arrays of projections at some point
'''
cProj=ROOT.TCanvas("Projections","Projection of Pixels",800,600)
proj2 = gPixels[centralPixel[0]][centralPixel[1]].ProjectionX("test",yBin, yBin)#.Draw()
proj2.SetLineColor(3)
proj2.SetMarkerColor(3)
proj2.Draw("hist")
proj1 = gPixels[max(centralPixel[0]-1,0)][centralPixel[1]].ProjectionX("p1",yBin, yBin)
proj0 = gPixels[max(centralPixel[0]-2,0)][centralPixel[1]].ProjectionX("p0",yBin, yBin)
proj3 = gPixels[min(centralPixel[0]+1,15)][centralPixel[1]].ProjectionX("p3",yBin, yBin)
proj4 = gPixels[min(centralPixel[0]+2,15)][centralPixel[1]].ProjectionX("p4",yBin, yBin)
print "pixels for projection:"
print max(centralPixel[0]-1,0),centralPixel[1]
print max(centralPixel[0]-2,0),centralPixel[1]
print min(centralPixel[0]+1,15),centralPixel[1]
print min(centralPixel[0]+2,15),centralPixel[1]

#cProj0 = ROOT.TCanvas("c0")
proj0.SetLineColor(1)
proj0.SetMarkerColor(1)
proj0.Draw("hist&&same")
#cProj1 = ROOT.TCanvas("c1")
proj1.SetLineColor(2)
proj1.SetMarkerColor(2)
proj1.Draw("hist&&same")
#cProj3 = ROOT.TCanvas("c3")
proj3.SetLineColor(4)
proj3.SetMarkerColor(4)
proj3.Draw("hist&&same")
#cProj4 = ROOT.TCanvas("c4")
proj4.SetLineColor(5)
proj4.SetMarkerColor(5)
proj4.Draw("hist&&same")

cProj.SaveAs("%s/PixelProjections.pdf" % dn) #save plot with projections
#combined projections for the explicitly listed pixels
'''

hEff=hHitM.Clone("Eff")
hEff.Write()

for pixx in xrange(16):
    for pixy in xrange(64):
        gPixels[pixx][pixy].Write()
        pass
    pass
############################################################################
print ("Output DQ file: DQ_%06i.txt"%(run))
dq=open("%s/DQ_%06i.txt"%(dn,run),"w+")
dq.write("MiniMALTA Diamond Test Beam analysis\n")
if args.clusters:
  f = ROOT.TFile(fr)
  hCluster = f.Get("hCluster")
  clusterSize = hCluster.GetMean()
  dq.write("Mean cluster size = %f\n" % (clusterSize))
#dq.write("File written time(now)\n")
dq.write("Pixel \t Efficiency \t Uncertainty \t Uncertainty of Mean \t %d perc X width (um) \t %d perc Y width (um) \t N hits\n" % (nperc, nperc))
#averages
eff = []
sigma = [] 
X = []
Y = []
sigmaMean = []

for i in range(nPix):
  if [pixxy[i][0],pixxy[i][1]] in goodPixxy:  
    xOverlap = (width[i]-36.4)/2
    yOverlap = (widthY[i]-36.4)/2
    dq.write("%d, %d \t %f \t %f \t %f \t %f \t %f \t %f \n" %(pixxy[i][0],pixxy[i][1],pixAvg[i],stdDev[i],stdDevMean[i], xOverlap,yOverlap, centerAvg[i]))
    eff.append(pixAvg[i])
    sigma.append(stdDev[i]*stdDev[i])
    X.append(xOverlap)
    Y.append(yOverlap)
    sigmaMean.append(stdDevMean[i]*stdDevMean[i])
eff = np.mean(eff)
sigma = np.sqrt(np.mean(sigma))
sigmaMean = np.sqrt(np.mean(sigmaMean))
centerAvg = np.mean(centerAvg)
X = np.mean(X)
Y = np.mean(Y)
dq.write("Averages: \t %f \t %f \t %f \t %f \t %f \t %f \n" %( eff, sigma, sigmaMean, X, Y , centerAvg) )
dq.close()

cHitM.cd()
pt = ROOT.TPaveText(xmin+10, ymax+3, xmax-10, ymax+30)
pt.SetTextFont(22)
for i in range(len(runDict)):
  if runDict[i]['run'] == run:
    sector = runDict[i]['sector']
    sensor = runDict[i]['sensor']
    voltage = runDict[i]['voltage']
pt.AddText( "%s %.1f V %s sector" %(sensor,voltage, sector) )
pt.AddText("Efficiency = %.1f  +/- %.1f (%.1f) N = %.0f " %(eff*100, sigmaMean*100, sigma*100, centerAvg))
pt.Draw()
mid.Draw("same")
cHitM.Update()
cHitM.Write()
cHitM.SaveAs("%s/HitMap%i.pdf" % (dn, run))
cHitM.SaveAs("%s/HitMap%i.C" % (dn, run))

cPixM.cd()
pt.Draw()
cPixM.Update()
cPixM.Write()
cPixM.SaveAs("%s/PixMap%i.pdf" % (dn, run))
cPixM.SaveAs("%s/PixMap%i.C" % (dn, run))

if not args.batch: raw_input("Press any key to continue.")
fw.Write()
fw.Close()
print("Have a nice day")
