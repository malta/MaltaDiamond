#!/usr/bin/env python
##############################
# Parse a google spreadsheet
#
# Carlos.Solans@cern.ch
# April 2019
##############################

import os
import sys
import json
import urllib2

class GoogleSpreadsheet:
    def __init__(self):
        self.sheet=[]
        self.cols=[]
        self.verbose=False
        pass

    def SetVerbose(self, enable):
        self.verbose=enable
        pass
    
    def Open(self, url):
        resp = urllib2.urlopen(url)
        #lines = response.read().split("\n") # "\r\n" if needed
        first = True
        for line in resp.readlines():
            if self.verbose: print "Parse: %s" % line
            line=line.replace("\r","")
            if first:
                first=False
                for col in line.split(","):
                    self.cols.append(col)
                    pass
                continue
            cols=line.split(",")
            self.sheet.append(cols)                
            pass
        pass

    def Open2(self,url,page):
        key=url
        if "/" in key:
            for k in url.split("/"):
                if len(k)>12: key=k
                pass
            pass
        newurl="https://spreadsheets.google.com/feeds/list/KEY/PAGE/public/full?alt=json"
        newurl=newurl.replace("KEY",key).replace("PAGE",str(page))
        if self.verbose: print("Open url: %s"%newurl)
        resp = urllib2.urlopen(newurl)
        data = json.loads(resp.read())
        iline=0
        for entry in data["feed"]["entry"]:
            row=[]
            for c in entry:
                if not "gsx$" in c: continue
                if "gsx$_" in c: continue
                name=c.replace("gsx$","").encode("utf-8")
                value=entry[c]["$t"].encode("utf-8")
                row.append(value)
                if not name in self.cols:
                    self.cols.append(name)
                    if self.verbose: print("Append name to cols: %s"%name)
                    pass
                pass
            if self.verbose: print("Append row to sheet: %r"%row)
            self.sheet.append(row)
            pass
        pass

    def GetCol(self,name):
        col=0
        for i in xrange(len(self.cols)):
            if self.verbose:
                print("Find: %s in %s" % (name.lower(), self.cols[i].lower()))
                pass
            #if name.lower() in self.cols[i].lower():
            found=True
            for n in name.split():
                if not n.lower() in self.cols[i].lower():
                    found=False
                    pass
            if found:
                return i
            pass
        return 0

    def GetNumRows(self):
        return len(self.sheet)
    
    def GetCell(self,row,col):
        return self.sheet[row][col]
    
    pass

if __name__=="__main__":
    "https://docs.google.com/spreadsheets/d/1k7Gm0MHYZHw7TdtcPYdwS4nMSBo1Sp0JOLqSplnGK6E/export?id=1k7Gm0MHYZHw7TdtcPYdwS4nMSBo1Sp0JOLqSplnGK6E&gid=1719069121&format=csv"
    import argparse
    parser=argparse.ArgumentParser()
    parser.add_argument("-u","--url",help="url to google sheet",required=True)
    parser.add_argument("-p","--page",help="page to load",required=True)
    parser.add_argument("-v","--verbose",help="enable verbose mode",action='store_true')
    args=parser.parse_args()
    gs=GoogleSpreadsheet()
    gs.SetVerbose(args.verbose)
    gs.Open2(args.url, args.page)
    
    
