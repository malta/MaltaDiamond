#!/usr/bin/env python
###########################################
# Tuple class to simplify ROOT Tree access
#
# Carlos.Solans@cern.ch
# Based on the code of someone else
# April 2019
###########################################
import os
import sys
import ROOT

class Tuple(dict):
    def __init__(self):
        self.treenumber=-1
        pass
    def LoadBranches(self,t):
        if self.treenumber == t.GetTreeNumber(): return
        self.treenumber = t.GetTreeNumber()
        for i in range(t.GetListOfLeaves().GetEntries()):
            print "Load", t.GetListOfLeaves().At(i).GetName()
            self.__setattr__(t.GetListOfLeaves().At(i).GetName(),
                             t.GetListOfLeaves().At(i))
            pass
        pass
    pass

